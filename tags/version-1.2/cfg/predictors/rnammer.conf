[General]
name = "RNAmmer"
exe_path = "rnammer"
path = "rnammer.py"
speed = ""
version = "1.2"
type = "known_specialized"
description: "<b>RNAmmer</b> predicts ribosomal RNA genes (5s/8s, 16s/18s, and
	      23s/28s)."

[Options]
simple = "tsu,ssu,lsu"
long = ""
basic_options = ""

[Options_default_params]
default_simple = "tsu,ssu,lsu"

[Options_desc]
tsu = "Search for 5/8S rRNA"
ssu = "Search for 16/18S rRNA"
lsu = "Search for 23/28S rRNA"



[Help]
text: "<div class="help_soft_title">Approach</div>
      RNAmmer  predicts  ribosomal  RNA  genes  in  full genome sequences by 
      utilising two levels of Hidden Markov Models: An initial spotter model 
      searches both strands. The spotter model is constructed from highly 
      conserved loci within a structural alignment of known rRNA sequences.
      Once the spotter model detects an approximate position of a gene, 
      flanking regions are extracted and parsed to the full model which matches
      the entire gene. By enabling a two-level approach it is avoided to run a 
      full model through an entire genome sequence allowing faster 
      predictions.<br /> <br />

      <div class="help_soft_title">Parameters</div>
      <b>Search for 5/8S rRNA</b>: Set RNAmmer to search for 5/8S rRNA.<br />
      <b>Search for 16/18S rRNA</b>: Set RNAmmer to search for 16/18S rRNA.<br />
      <b>Search for 23/28S rRNA</b>: Set RNAmmer to search for 23/28S rRNA.<br />
	  If there is no options checked, RNAmmer is settled to search for all of them.
	  
      <div class="help_soft_title">Reference</div>
      RNammer: consistent annotation of rRNA genes in genomic sequences. <br />
      Lagesen K, Hallin PF, Rødland E, Stærfeldt HH, Rognes T Ussery DW. Nucleic 
      Acids Res. 2007 Apr 22.<br />
      <a href="http://www.cbs.dtu.dk/services/RNAmmer/">RNAmmer web site</a>
      "