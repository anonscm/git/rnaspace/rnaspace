#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import re
import subprocess
import tempfile
import logging

from rnaspace.core.data_manager import data_manager
from rnaspace.core.email_sender import email_sender
from rnaspace.core.trace.event import error_event

class cgview(object):
    
    def run(self, user_id, project_id, cgview_file, ext="png"):
        dm = data_manager()
        cgview_exe = dm.get_cgview_exe()
        tmp_dir = dm.config.get("storage","tmp_dir")
        tmp_output_name = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        if ext == "png":
            cgview_cmd = "java -jar %s -i %s -o %s -f png -r T -A 12 -U 9"%(cgview_exe,
                                                                 cgview_file,
                                                                 tmp_output_name)
        else:
            cgview_cmd = "java -jar %s -i %s -o %s -f svg -r T -A 12 -U 9"%(cgview_exe,
                                                                 cgview_file,
                                                                 tmp_output_name)
        cgview_cmd += " 1>/dev/null 2>/dev/null"
        retcode = subprocess.call(cgview_cmd, shell=True)
        
        if retcode != 0:
            message = "Problem running CGview: " + cgview_cmd
            email_sender().send_admin_failed_email(user_id, project_id, \
                                                   "explore", "cgview",
                                                   cgview_cmd) 
            mail = dm.get_user_email(user_id, project_id)
            ev = error_event(user_id, project_id, mail, 
                             message, dm.get_project_size(user_id,
                                                          project_id))
            logging.getLogger("rnaspace").error(ev.get_display())

        return tmp_output_name
