#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




from Queue import Queue, Empty
from threading import Thread
import threading
import thread

class worker(Thread):
    """Thread executing tasks from a given tasks queue"""
    
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.stop_flag = False
        self.start()
        
    def run(self):
        
        while not self.stop_flag:
            try:
                pred_class = self.tasks.get()
                try:
                    pred_class.run()
                except Exception, e:
                    print e
                
                self.tasks.task_done()
            except Empty:
                pass
            
class threads_manager:
    """Pool of threads consuming tasks from a queue"""
    
    def __init__(self, num_threads):
        self.tasks = Queue()
        self.threads = []
        self.num_threads = num_threads

        for _ in range(num_threads):
            try:
                self.threads.append(worker(self.tasks))
            except thread.error:
                pass

    def add_task(self, pred_class):
        """Add a task to the queue"""
        self.tasks.put(pred_class)

    def wait_completion(self):
        """Wait for completion of all the tasks in the queue"""
        self.tasks.join()

