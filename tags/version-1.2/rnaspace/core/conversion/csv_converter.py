#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from rnaspace.core.putative_rna import putative_rna

class csv_converter (object):
    """ Class csv_converter: converts objects to csv or read csv to objects
        This object has to be used as following:
            my_var = csv_converter()
            my_var.read(path/to.csv.csv)
            obj = my_var.get_object()
    """   

    def write(self, obj, output):
        """ writes a table of object into an gff file
            prnas(type:[object])    the object table to write down
            output(type:string)     path to the folder to store the gff file
        """
        if len(obj) > 0:
            if type(obj[0]) == putative_rna or isinstance(obj[0], putative_rna):
                self.__write_putative_rnas(obj, output)
            else: raise TypeError( str(type(obj)) + " is not supported by our rnaml converter.")

    def __write_putative_rnas(self, prnas, output):
        """ writes a table of putative_rna into a csv file
            prna(type:[putative_rna])  the putative_rna table to write down
            output(type:string)      path to the folder to store the csv file
        """
        csv_file = open(output, 'a')
        for prna in prnas:
            if prna.family == "unknown":
                family = "ncRNA"
            else:
                family = prna.family              
            csv_file.write(str(prna.user_id) + ";" + 
			   str(prna.genomic_sequence_id) + ";" +
			   family + ";" + 
			   str(prna.start_position) + ";" + 
			   str(prna.stop_position) + ";" + 
			   str(prna.size) + ";" + 
			   str(prna.strand) + ";" + 
			   str(prna.species) + ";" + 
			   str(prna.domain) + ";" + 
			   str(prna.replicon) + ";" + 
			   str(prna.program_name) + ";" + 
			   str(prna.score) + ";" + 
			   str(len(prna.alignment)) + ";" +
			   str(prna.run) + "\n")
        csv_file.close()

        
