#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses
from rnaspace.ui.cli.window import window

class selectBox(window):
    """Window containing items that can be selected.

    The selected item is hilighted by given him the curses.A_REVERSE color.
    """

    ## FIXME: need to handle scrolling + cut lines when too long
    
    def __init__(self, stdscr, y, x, h, w, tabs, border=False, paddingX=1,
                 paddingY=1):
        """
        y, x: positions of the window
        h, w: height and width
        tabs: a list of items
        """

        window.__init__(self, stdscr, y, x, h, w, border, paddingX, paddingY)
        # the selectable items
        self.tabs = tabs
        # the current selected item
        self.selected_tab = 0
        # number of spaces between two items
        # self.space = 10
        # colors
        self.hilight_color = curses.A_REVERSE
        self.normal_color = curses.A_NORMAL
        
    def update(self):
        """ Update the window's content.

        The update is done in the virtual buffer of curses.
        A call to curses.doupdate() is needed to see the changes.
        """

        self.win.clear()
        if self.border:
            self.win.border()
            
        for (i, tab) in enumerate(self.tabs):
            # set the color according to the state of the item
            color = self.normal_color
            if self.selected_tab == i:
                color = self.hilight_color

            if self.border:
                self.win.addstr(i + 1 + self.paddingY, 1 + self.paddingX, tab,
                                color)
            else:
                self.win.addstr(i+ self.paddingY, self.paddingX, tab,
                                color)
            
        self.win.noutrefresh()


    def next(self):
        """Select the next item in the list if the current one is not
        the last one.

        Return the selected item.
        """
        
        if self.selected_tab < len(self.tabs) - 1:
            self.selected_tab += 1
        else:
            self.selected_tab = 0
            
        self.update()
        
        return self.tabs[self.selected_tab]

    def prev(self):
        """Select the previous item in the list if the current one is not
        the first one.

        Return the selected item.
        """

        if self.selected_tab > 0:            
            self.selected_tab -= 1
        else:
            self.selected_tab = len(self.tabs) - 1
            
        self.update()

        return self.tabs[self.selected_tab]


    def get_selection(self):
        return self.tabs[self.selected_tab]

    def set_selection(self, section):
        for (i, tab) in enumerate(self.tabs):
            if section == tab:
                self.selected_tab = i
                self.update()
                break
        
