#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses

class window:
    """ Generic window object."""

    
    def __init__(self, stdscr, y, x, h, w, border=False, paddingX=1,
                 paddingY=1, content=""):
        """
        y, x: positions of the window
        h, w: height and width
        paddingX, paddingY: number of spaces added before the content of the
        window.

        If border is True, the window border will be displayed
        """
        
        self.stdscr = stdscr
        # the curses.window object
        self.win = curses.newwin(h, w, y, x)
        # string that is displayed in the window
        self.content = content
        
        self.paddingX = paddingX
        self.paddingY = paddingY
        self.border = border

        height, width = self.win.getmaxyx()

        self.max_width = width - 2 * self.paddingX - 2
        self.max_height = height - 2 * self.paddingY
        if self.border:
            self.max_width -= 2
            self.max_height -= 2

    
    def set_content(self, new_content):
        self.content = new_content
        self.update()

    def update(self):
        """ Update the window's content.

        The update is done in the virtual buffer of curses.
        A call to curses.doupdate() is needed to see the changes.
        """
        
        self.win.clear()

        if self.border:
            self.win.border()
            self.win.addstr(1 + self.paddingY, 1 + self.paddingX, self.content)
        else:
            self.win.addstr(self.paddingY, self.paddingX, self.content)
        self.win.noutrefresh()

    def overwrite(self, dest):
        self.win.overwrite(dest.win)

    def get_selection(self):
        return None
