#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Keyboard Navigation:

[TAB] or [RIGHT]            : next item in the left panel
[SHIFT]+[TAB] or [LEFT]     : previous item in the left panel
[UP_KEY] or [PAGE_UP]       : scroll up in the right panel
[DOWN_KEY] or [PAGE_DOWN]   : scroll down in the right panel
[HOME_KEY]                  : move at the top of the page in the right panel
[END_KEY]                   : move at the bottom of the page in the right panel
[BACKSPACE] or [ESC]        : previous page (used in predictors section)

[q]                         : exit
"""


import curses

from rnaspace.ui.cli.window import window
from rnaspace.ui.cli.inputwindow import inputWindow
from rnaspace.ui.cli.plaintextwindow import plainTextWindow
from rnaspace.ui.cli.splittedwindow import splittedWindow
from rnaspace.ui.cli.titlewindow import titleWindow
from rnaspace.ui.cli.parameterswindow import parametersWindow
from rnaspace.ui.cli.selectbox import selectBox

class helpWindow:
    """The main window of the help interface.

    It contains the other subwindows:
      - the navigation window
      - the content window
      - the status window
      - the input window
    """

    def __init__(self, stdscr, usage, softs, organisms, output_formats, dbs):
        
        self.stdscr = stdscr
        self.h, self.w = self.stdscr.getmaxyx()

        # deal with very small terminal
        if self.w < 80 or self.h < 10:
            self.h, self.w = (10, 80)

        self.nav_width = 20
        self.softs = softs
        self.usage = usage
        
        if organisms is None:
            self.organisms = []
        else:
            self.organisms = organisms
        if output_formats is None:
            self.formats = []
        else:
            self.formats = output_formats
        if dbs is None:
            self.dbs = []
        else:
            self.dbs = dbs
        
        # the name of the predictor the content windows is displaying the infos
        self.predictors_page = None
        self.option_page = None
        
        self.software_dict(softs)
        self.build_windows()
        self.content = self.content_windows["Shortcuts"]
        
    def build_windows(self):
        tabs = ["Shortcuts", "Usage", "Gene Finder", "Organisms",
                "Export Formats", "Databases"]

        if len(self.organisms) == 0:
            organisms_str = "No organisms found."
        else:
            organisms_str = "\n".join(o for o in self.organisms)
        if len(self.formats) == 0:
            export_str = "No export formats found."
        else:
            export_str = "\n".join(e for e in self.formats)
        if len(self.dbs) == 0:
            dbs_str = "No databases found."
        else:
            dbs_str = "\n".join(d for d in self.dbs)

        self.nav = selectBox(self.stdscr, 0, 0, self.h-3, self.nav_width, tabs,
                             border=True)
        
        self.status = splittedWindow(self.stdscr, self.h-3, 0, 3, self.w,
                                     "Shortcuts", "", paddingX=2, paddingY=0,
                                     border=True)
        
        self.input = inputWindow(self.stdscr, 1, 1, 1, 1)

        self.content_windows = {
            "Shortcuts": plainTextWindow(self.stdscr, 0, self.nav_width,
                                         self.h-3, self.w - self.nav_width,
                                         content=__doc__, border=True),
            "Usage": plainTextWindow(self.stdscr, 0, self.nav_width,
                                    self.h-3, self.w - self.nav_width,
                                    content=self.usage, border=True),
            "Gene Finder": titleWindow(self.stdscr, 0, self.nav_width,
                                      self.h-3, self.w - self.nav_width,
                                      content=self.dsofts,
                                      border=True),
            "Organisms": plainTextWindow(self.stdscr, 0, self.nav_width,
                                         self.h-3, self.w - self.nav_width,
                                         content=organisms_str,
                                         border=True),
            "Export Formats": plainTextWindow(self.stdscr, 0, self.nav_width,
                                              self.h-3, self.w - self.nav_width,
                                              content=export_str,
                                              border=True),
            "Databases": plainTextWindow(self.stdscr, 0, self.nav_width,
                                         self.h-3, self.w - self.nav_width,
                                         content=dbs_str,
                                         border=True)
            }

        # pre-build the windows that contains the help of each predictor
        self.predictors_help = {}
        for soft_type in self.softs:
            for soft in self.softs[soft_type]:
                h = self.software_help(soft.name)
                self.predictors_help[soft.name] =\
                        parametersWindow(self.stdscr, 0, self.nav_width,
                                         self.h-3, self.w - self.nav_width,
                                         content=h, border=True)

        # pre-build the windows that contains the help for each option of each
        # predictor
        self.predictors_options = {}
        for soft_type in self.softs:
            for soft in self.softs[soft_type]:
                self.predictors_options[soft.name] = {}
                for opt in soft.simple_options + soft.long_options:
                    if opt == '':
                        continue                    
                    h = self.software_option_help(soft.name, opt)
                    self.predictors_options[soft.name][opt] =\
                        plainTextWindow(self.stdscr, 0, self.nav_width,
                                         self.h-3, self.w - self.nav_width,
                                         content=h, border=True)


                
    def update(self):
        """Update the content of all the subwindows."""
        
        self.nav.update()
        self.content.update()
        self.status.update()

    def resize(self):
        """Call when the terminal is resized.

        Update the size of all the subwindows according to the new
        terminal size.
        """
        
        self.h, self.w = self.stdscr.getmaxyx()
        
        # do not resize windows if the terminal is too small
        if self.w < 80 or self.h < 10:
            return

        # emorize the current section that is displayed
        section = self.nav.get_selection()

        # we re build everything... this is the only way i fnd to resize
        # windows without crashing curses. It must have something better.
        self.build_windows()
        self.nav.set_selection(section)

        # if we are on the help page of a predictor, self.predictors_page is
        # not None
        if section == "Gene Finder":
            if self.predictors_page is not None:
                if self.option_page is not None:
                    self.content = self.predictors_options[self.predictors_page][self.option_page]
                else:
                    self.content = self.predictors_help[self.predictors_page]
            else:
                self.content = self.content_windows[section]
        else:
            self.content = self.content_windows[section]
        self.update()
        
    def run(self):
        """Enter in the main loop and wait for events."""
        
        self.update()
        curses.doupdate()

        while True:
            # wait for user input
            key = self.input.getch()

            # navigation through the navigation menu
            if (key == curses.KEY_LEFT or key == curses.KEY_BTAB or
                key == curses.KEY_RIGHT or key == 9):
                if key == curses.KEY_LEFT or key == curses.KEY_BTAB:
                    section = self.nav.prev()
                else:
                    section = self.nav.next()
                if section == "Gene Finder":
                    if self.predictors_page is not None:
                        if self.option_page is not None:
                            self.content = self.predictors_options[self.predictors_page][self.option_page]
                        else:
                            self.content= self.predictors_help[self.predictors_page]
                    else:
                        self.content = self.content_windows[section]
                else:
                    self.content = self.content_windows[section]
                self.content.update()

            # scroll the main content
            elif key == curses.KEY_DOWN:
                self.content.scroll_down()
            elif key == curses.KEY_UP:
                self.content.scroll_up()
            # big scroll
            elif key == curses.KEY_PPAGE:
                self.content.scroll_up(self.content.win.getmaxyx()[0]-5)
            elif key == ord(" ") or key == curses.KEY_NPAGE:
                self.content.scroll_down(self.content.win.getmaxyx()[0]-5)
            elif key == curses.KEY_HOME:
                self.content.scroll_top()
            elif key == curses.KEY_END:
                self.content.scroll_bottom()

            # if enter, look if the hilighted line is selectable (ie the line
            # is the name of a predictor)
            elif key == curses.KEY_ENTER or key == 10:
                selection = self.content.get_selection()
                if selection is not None:
                    if self.predictors_page is None:
                        self.content = self.predictors_help[selection]
                        self.content.update()
                        self.predictors_page = selection
                    else:
                        self.content = self.predictors_options[self.predictors_page][selection]
                        self.content.update()
                        self.option_page = selection

            # BACKSPACE return to the predictor page when we come from
            # a help page
            elif key == curses.KEY_BACKSPACE or key == 27:                
                if (self.nav.get_selection() == "Gene Finder" and
                    self.predictors_page is not None):
                    if self.option_page is not None:
                        self.option_page = None
                        self.content= self.predictors_help[self.predictors_page]
                    else:
                        self.predictors_page = None
                        self.content = self.content_windows["Gene Finder"]
                    self.content.update()

                
            # exit the help
            elif key == ord("q"):
                break

            # resize the terminal
            elif key == curses.KEY_RESIZE:
                self.resize()


            # update status window 
            section = self.nav.get_selection()
            lst = section
            rst = ""
            if section == "Gene Finder":
                rst = "Press [Enter] for details."
                if self.predictors_page is not None:
                    lst += " > " + self.predictors_page
                    if self.option_page is not None:
                        lst += " > " + self.option_page
                        rst = "[BACKSPACE]: return to %s" % self.predictors_page

            self.status.set_left_content(lst)
            self.status.set_right_content(rst)
            self.status.update()
            
            curses.doupdate()


    def software_dict(self, softs):
        self.dsofts = {}
        self.softs = softs
        
        inf = softs["inference"]
        agg = softs["aggregation"]
        cons = softs["conservation"]
        known = softs["known"]
        abinitio = softs["abinitio"]

        self.dsofts["Homology"] = [s.name for s in known]
        self.dsofts["Ab initio"] = [s.name for s in abinitio]
        self.dsofts["Comparative Analysis"] = {}
        self.dsofts["Comparative Analysis"]["Sequence alignment"] = []
        self.dsofts["Comparative Analysis"]["Sequence aggregation"] = []
        self.dsofts["Comparative Analysis"]["Structure inference"] = []

        for s in cons:
            self.dsofts["Comparative Analysis"]["Sequence alignment"].append(s.name)
        for s in agg:
            self.dsofts["Comparative Analysis"]["Sequence aggregation"].append(s.name)
        for s in inf:
            self.dsofts["Comparative Analysis"]["Structure inference"].append(s.name)

    def software_help(self, softname):
        softs = self.softs
        ok = False
        for soft_type in softs:
            for soft in softs[soft_type]:
                if soft.name == softname:                    
                    asked_soft = soft
                    ok = True
                    break
            if ok:
                break

        if not ok:
            return None
        
        options = {softname:[]}            
        for opt in asked_soft.simple_options + asked_soft.long_options:
            if opt != '':
                h = asked_soft.options_desc[opt].replace('<br />', ' ')
                if h[-1] == ':':
                    h = h[:-1]
                options[softname].append((opt, h))
                
        return options


    def software_option_help(self, softname, opt):
        softs = self.softs
        ok = False
        for soft_type in softs:
            for soft in softs[soft_type]:
                if soft.name == softname:                    
                    asked_soft = soft
                    ok = True
                    break
            if ok:
                break

        if not ok:
            return None
        
        content = ""
        for o in asked_soft.long_options + asked_soft.simple_options:
            if o == opt:
                h = soft.options_desc[opt].replace('<br />', '\n')
                if h[-1] == ':':
                    h = h[:-1]
                if opt in asked_soft.long_options:
                    default = asked_soft.options_default[opt]
                    possible = asked_soft.options_params[opt]
                    if isinstance(possible, list):
                        if len(possible) == 1 and possible[0] == "":
                            possible_str = "<string>"
                        else:
                            possible_str = "\n".join(p for p in possible)
                    else:
                        if possible == "":
                            possible_str = "<string>"
                        else:
                            possible_str = possible
                else:
                    default = opt in asked_soft.default_simple
                    possible_str = "True\nFalse"
                content = "%s:\n\n%s\n\n* Default value: %s\n\n* Accepted values:\n\n%s"
                content = content % (opt, h, str(default), str(possible_str))
                return content
                
        return content


