#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses

from rnaspace.ui.cli.titlewindow import titleWindow

class parametersWindow(titleWindow):

    def __init__(self, stdscr, y, x, h, w, border=False, paddingX=1,
                 paddingY=1, content=""):
        titleWindow.__init__(self, stdscr, y, x, h, w, border, paddingX,
                             paddingY, content)

        self.max_opt_length = 0
        for softname in content:
            for (opt, h) in content[softname]:
                if len(opt) > self.max_opt_length:
                    self.max_opt_length = len(opt)
                
        self.format_content()
        self.beginY = 0
        self.selected_line = 0


    def get_title(self, line, indent_level, is_selectable=True):
        indented_line = "* " + line
        return (indented_line, curses.A_BOLD, indent_level, is_selectable)

    def get_subtitle(self, line, color, indent_level, is_selectable=True):
        indented_line = indent_levell * 2 * " " + "- " + line
        return (indented_line, curses.A_NORMAL, indent_level, is_selectable)
    
    def get_line(self, line, indent_level, is_selectable=True):
        indented_line = line[0] + " " * (self.max_opt_length - len(line[0]))
        indented_line += "   " + line[1]
        indented_line = indented_line[:self.max_width-1]
        return (indented_line, curses.A_NORMAL, indent_level, is_selectable)

    def get_selection(self):
        if self.formatted_content[self.selected_line][3]:
            selection = self.formatted_content[self.selected_line][0]
            # get only the option's name
            opt = selection.split()[0]
            # remove the indentation spaces
            return opt.strip()

        return None
