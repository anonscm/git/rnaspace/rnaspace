#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses

from rnaspace.ui.cli.window import window

class plainTextWindow(window):
    """ Generic window object with scroll facilities.

    The window is also able to resize its content according its width.
    """

    
    def __init__(self, stdscr, y, x, h, w, border=False, paddingX=1,
                 paddingY=1, content=""):
        """
        y, x: positions of the window
        h, w: height and width
        paddingX, paddingY: number of spaces added before the content of the
        window.

        If border is True, the window border will be displayed
        """
        window.__init__(self, stdscr, y, x, h, w, border, paddingX, paddingY,
                        content)
        # the content splitted into lines
        self.formatted_content = []
        
        # the first line of formatted_content to displayed 
        self.beginY = 0
        self.format_content()

    def scroll_down(self, step=1):
        if self.beginY < len(self.formatted_content) - step:
            self.beginY += step
        else:
            self.beginY = len(self.formatted_content) - 1
            
        self.update()
        
    def scroll_up(self, step=1):
        if self.beginY > step-1:
            self.beginY -= step
        else:
            self.beginY = 0
            
        self.update()

    def scroll_top(self):
        self.beginY = 0
        self.update()

    def scroll_bottom(self):
        self.beginY = len(self.formatted_content) - 1
        self.update()
        
    def set_content(self, newcontent):
        self.content = newcontent
        self.format_content()
        self.update()
            
    def split_line(self, line, width):
        """Split a line into several lines with a maximum width of 'width'.

        Return a list of lines.
        """
        
        if len(line) > width:
            lines = []
            lines.append(line[:width])
            lines.extend(self.split_line(line[width:], width))
            return lines
        
        return [line]

    def format_content(self):
        """
        Split the content of the window in several line according to the
        width of the window. The splitted content is stored in
        self.formatted_content.
        """
        
        height, width = self.win.getmaxyx()

        self.max_width = width - 2 * self.paddingX
        self.max_height = height - 2 * self.paddingY
        if self.border:
            self.max_width -= 2
            self.max_height -= 2
            
        self.formatted_content = []
        splitted = self.content.splitlines()

        for line in splitted:
            if len(line) > self.max_width:
                self.formatted_content.extend(self.split_line(line,
                                                              self.max_width))
            else:
                self.formatted_content.append(line)


    def update(self):
        """ Update the window's content.

        The update is done in the virtual buffer of curses.
        A call to curses.doupdate() is needed to see the changes.
        """
        
        self.win.clear()

        # get wich lines of content we have to display.
        endY = self.beginY + min(self.max_height,
                                 len(self.formatted_content[self.beginY:]))

        if self.border:
            self.win.border()
            for (i, line) in enumerate(self.formatted_content[self.beginY:endY]):
                self.win.addstr(1+self.paddingY+i, 1+self.paddingX, line)
        else:
            for (i, line) in enumerate(self.formatted_content[self.beginY:endY]):
                self.win.addstr(self.paddingY+i, self.paddingX, line)
        self.win.noutrefresh()

