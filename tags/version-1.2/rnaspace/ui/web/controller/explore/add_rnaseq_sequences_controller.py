#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import cherrypy

from rnaspace.ui.web.utils.common import common
from rnaspace.ui.web.utils.common import AUTH_ERROR
from rnaspace.ui.web.model.explore.add_rnaseq_sequences_model import add_rnaseq_sequences_model
from rnaspace.ui.web.controller.error_controller import error_controller
from rnaspace.core.exceptions import disk_error


DISK_ERROR =\
"""
<br />
Sorry, no more space available. <br />
You can not modify predictions.
<br /><br />
Return to <a href="%s">Explore page</a>.
"""

class add_rnaseq_sequences_controller(object):
    
    def __init__(self):
        self.model = add_rnaseq_sequences_model()


    @cherrypy.expose
    def index(self, **params):
        mount_point = self.model.get_mount_point()
        view = common.get_template('explore/add_rnaseq_sequences_view.tmpl')

        if not params.has_key("authkey"):
            return "You're not authorized to access this page."
        
        (user_id,
         project_id) = self.model.get_ids_from_authkey(params["authkey"])
            
        if user_id is None or project_id is None:
            return self.error(AUTH_ERROR)

        if self.model.get_action(params) == "form":
            view.sequences = self.model.get_project_sequences(user_id,
                                                              project_id)
            view.authkey = params["authkey"]
            view.mount_point = mount_point

            return str(view)
        
        elif self.model.get_action(params) == "add":

            (error, message) = self.model.add_predictions(user_id, project_id,
                                                          params)
            if error:
                return message
            else:
                return ""

    @cherrypy.expose
    def error(self, msg, authkey=None):
        error_page = error_controller()
        return error_page.get_page(msg, authkey)
