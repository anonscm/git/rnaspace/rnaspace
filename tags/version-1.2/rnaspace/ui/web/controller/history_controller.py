#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import cherrypy

from rnaspace.ui.web.utils.common import common
from rnaspace.ui.web.model.history_model import history_model
from rnaspace.ui.web.utils.common import AUTH_ERROR
from rnaspace.ui.web.controller.error_controller import error_controller
from rnaspace.ui.web.controller.infobar_controller import infobar_controller

class history_controller(object):
    """ Class history_controller: the controller of the history page
    """
    
    def __init__(self):
        self.model = history_model()
        self.view = common.get_template('history_view.tmpl')
        self.infobar = infobar_controller()
        
    @cherrypy.expose
    def index(self, **params):
        mount_point = self.model.get_mount_point()
        self.view.authentification_platform = self.model.is_an_authentification_platform()

        if params.has_key("authkey"):
            (user_id,
             project_id) = self.model.get_ids_from_authkey(params["authkey"])

            if user_id is None or project_id is None:
                return self.error(AUTH_ERROR)
            
            if self.view.authentification_platform:
                self.view.infobar = self.infobar.get_infobar(user_id, project_id)

            events = self.model.get_project_trace(user_id, project_id)
            
            self.view.user_id = user_id
            self.view.project_id = project_id
            self.view.authkey = params["authkey"]
            self.view.mount_point = mount_point
            self.view.events = events
            return str(self.view)
        else:
            return self.error(AUTH_ERROR)
    
    @cherrypy.expose
    def error(self, msg, authkey=None):
        error_page = error_controller()
        return error_page.get_page(msg, authkey)
