#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from rnaspace.core.data_manager import data_manager

class history_model:

    def __init__(self):
        self.data_m = data_manager()

    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_m.get_ids_from_authkey(id)

    def get_mount_point(self):
        return self.data_m.get_mount_point()

    def get_project_trace(self, user_id, project_id):
        event_type = ["ADD_SEQ", "PREDICT_COMBINE", "USER_COMBINE", "ADD_ALIGNMENT",
                      "PREDICT", "REMOVE_RNA", "EDIT_RNA", "ADD_RNA"]
        ptrace = self.data_m.get_project_trace(user_id, project_id)
        events = ptrace.events
        run_events = {}
        user_events = []
        to_return = []
        
        for event in events:
            if event.type in event_type:
                if event.run_id is not None:
                    run_events.setdefault(event.run_id, {"date":event.date_str,
                                                         "events":[],
                                                         "run":event.run_id})
                    run_events[event.run_id]["events"].append(event)
                    run_events[event.run_id]["date"] = min(run_events[event.run_id]["date"],
                                                           event.date_str)
                else:
                    user_events.append({"date":event.date_str, "events":[event],
                                        "run":None})

        to_return = user_events
        for run_id in run_events:
            to_return.append(run_events[run_id])

        to_return.sort(cmp=lambda x,y: cmp(y["date"], x["date"]))
        
        return to_return

    def is_an_authentification_platform(self):
        return self.data_m.is_an_authentification_platform()
