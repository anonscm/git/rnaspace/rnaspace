#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re
import StringIO
import time

from rnaspace.core.id_tools import id_tools
import rnaspace.core.common as common
from rnaspace.core.data_manager import data_manager
from rnaspace.core.putative_rna import putative_rna
from rnaspace.core.trace.event import add_rna_event
from rnaspace.core.trace.event import disk_error_event
from rnaspace.core.exceptions import disk_error

class add_rnaseq_sequences_model(object):
    
    def __init__(self):
        self.data_m = data_manager()

    def add_predictions(self, user_id, project_id, params):

        predictions = None
        
        if(not (params.has_key("gff") or params.has_key("gfffile")) or
           not params.has_key("sequence") ):
            return (True, "Sorry an error occured")

        if (params.has_key("gfffile") and params.has_key("gff") and
            params["gfffile"].filename != "" and params["gff"] != ""):
            return (True, "Paste your GFF OR upload a file")
        
        if params.has_key("gfffile") and params["gfffile"].filename != "":
            predictions = self.read_gff(params['gfffile'].file)
            
        elif params.has_key("gff") and params["gff"] != "":
            predictions = self.read_gff(StringIO.StringIO(params['gff']))
                        
        if predictions is None:
            return (True, "GFF format required not respected.")

        (error, message) = self.save_predictions(user_id, project_id,
                                                 predictions,
                                                 params["sequence"])        
        return (error, message)

    def __get_complement(self, base):
        """ 
        Return the complement base
        base(string)     the base to complement
        """
        cbase_table = {
            'A': 'T',
            'a': 't',
            'T': 'A',
            't': 'a',
            'C': 'G',
            'c': 'g',
            'G': 'C',
            'g': 'c',
            'U': 'A',
            'u': 'a',
            'N': 'N',
            'n': 'n'}
        return cbase_table[base]
 
    def __get_reverse_complement(self, seq):
        """ Return the reverse complement sequence
        seq(string)     the sequence to reverse and complement
        """
        rc_seq = ""
        for base in reversed(seq):
            rc_seq += self.__get_complement(base)
        return rc_seq

    def save_predictions(self, user_id, project_id, predictions,
                         sequence_id):
        
        ids_gen = id_tools()
        prnas = []

        seq = self.data_m.get_sequence( user_id, sequence_id, project_id)
        seq_length = len(seq.data)
        user_ids = self.get_user_id_already_used(user_id, project_id)
        
        for i in predictions:
            prnaid = ids_gen.get_new_putativerna_id(user_id, project_id,
                                                    sequence_id)            
            
            # test the positions
            if predictions[i]["start_position"] > seq_length:
                return (True, "Positions are not correct: start position > sequence length")
            if predictions[i]["stop_position"] > seq_length:
                return (True, "Positions are not correct: end position > sequence length")         
            if predictions[i]["start_position"] <= 0:
                return (True, "Positions are not correct: start position <= 0")            
            if predictions[i]["stop_position"] <= 0:
                return (True, "Positions are not correct: end position <= 0")
            if predictions[i]["start_position"] > predictions[i]["stop_position"]:
                return (True, "Positions are not correct: start position > end position")

            # test the family
            if not re.search(common.family_regexp, predictions[i]["feature"]):
                return (True, "Invalid family name (%s)" % predictions[i]["feature"])

            # test the software
            if not re.search(common.soft_regexp, predictions[i]['source']):
                return (True, "Invalid source name (%s)" % predictions[i]['source'])

            # test the strand
            if not re.search(common.strand_regexp, predictions[i]['strand']):
                return (True, "Invalid strand (%s)" % predictions[i]['strand'])
            
            # test if the user_id already exists
            if predictions[i]["user_id"] in user_ids:
                return (True, "User id already used (%s)" % predictions[i]["user_id"])
            if not re.search(common.user_id_regexp, predictions[i]["user_id"]):
                return (True, "User id invalid (%s)" % predictions[i]["user_id"])
            
            user_ids.append(predictions[i]["user_id"])
                        
            sequence = seq.data[predictions[i]['start_position']-1:predictions[i]['stop_position']]
            if predictions[i]["strand"] == "-":
                sequence = self.__get_reverse_complement(sequence)

            prna = putative_rna(id=prnaid,
                                genomic_sequence_id=sequence_id,
                                start_position=predictions[i]['start_position'],
                                stop_position=predictions[i]['stop_position'],
                                sequence=sequence,
                                run="user",
                                user_id=predictions[i]["user_id"],
                                family=predictions[i]['feature'],
                                domain=seq.domain,
                                species=seq.species,
                                replicon=seq.replicon,
                                strain=seq.strain,
                                score=predictions[i]['score'],
                                program_name=predictions[i]['source'],
                                strand=predictions[i]['strand'],
                                day=str(time.localtime()[2]),
                                month=str(time.localtime()[1]),
                                year=str(time.localtime()[0]),
                                alignment=[])
            prnas.append(prna)

        try:
            self.data_m.add_putative_rnas(user_id, project_id, prnas)
        except disk_error, e:
            mail = self.data_m.get_user_email(user_id, project_id)
            project_size = self.data_m.get_project_used_space(user_id,
                                                              project_id)
            ev = disk_error_event(user_id, project_id, mail, e.message,
                                  project_size)
            
            self.data_m.update_project_trace(user_id, project_id, [ev])
            raise
        else:
            for prna in prnas:
                email = self.data_m.get_user_email(user_id,project_id)
                e = add_rna_event(user_id, project_id,
                                  email, prna.user_id,
                                  seq_name=prna.genomic_sequence_id,
                                  family=prna.family,
                                  start=prna.start_position, 
                                  stop=prna.stop_position, strand=prna.strand,
                                  secondary_structures=prna.structure,
                                  nb_alignment=0)
                self.data_m.update_project_trace(user_id, project_id, [e])
            
        return (False, "")

    def read_gff(self, gff_file):
        predictions = {}
        i = 0
        error = False
        for line in gff_file:
            if line.startswith('#') or line == '':
                continue
            
            splitted = line.split();
            
            if len(splitted) < 8:
                error = True
                break
            else:
                try:
                    predictions[i] = {"user_id": splitted[0],
                                      "source": splitted[1],
                                      "feature": splitted[2],
                                      "start_position": int(splitted[3]),
                                      "stop_position": int(splitted[4]),
                                      "score": float(splitted[5]),
                                      "strand": splitted[6]}
                except ValueError:
                    error = True
                    break

                i += 1

        if error:
            return None

        return predictions

    def get_user_id_already_used(self, user_id, project_id):
        rnas = self.data_m.get_putative_rnas(user_id, project_id)
        user_ids = []
        for rna in rnas:
            user_ids.append(rna.user_id)
        return user_ids
 
        
    def get_project_sequences(self, user_id, project_id):
        """
        Return                the sequence ids the project
        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        """
        sequences = []        
        try:
            ids = self.data_m.get_sequences_id(user_id, project_id)   
            for seqid in sorted(ids):
                sequences.append(self.data_m.get_sequence(user_id, seqid,
                                                          project_id))
        except:
            pass
        return sequences

    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_m.get_ids_from_authkey(id)

    def get_authkey(self, user_id, project_id):
        """
        user_id(sting)      the user_id
        project_id(string)  the project_id
        return the id
        """
        return self.data_m.get_authkey(user_id, project_id)

    def get_mount_point(self):
        return self.data_m.get_mount_point()

    def get_action(self, params):
        if params.has_key("rnaseq-action"):
            return params["rnaseq-action"]
        else:
            return ""
