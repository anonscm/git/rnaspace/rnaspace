#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import tempfile
from rnaspace.core.exploration.filter import filter
from rnaspace.core.exploration.filter import selection_criteria
from rnaspace.core.data_manager import data_manager

class jbrowse_model(object):

    def __init__(self):
        self.data_manager = data_manager()

    def get_ids_from_authkey(self, id):
        return self.data_manager.get_ids_from_authkey(id)

    def user_has_data(self, user_id, project_id):
        """ Return True if the user has data, false else 
            user_id(type:string)   the user id
        """
        return self.data_manager.user_has_project(user_id, project_id)

    def get_project_expiration_days(self):
        return self.data_manager.get_project_expiration_days()

    def file_path(self, user_id, project_id, name):
        user_dir = self.data_manager.get_project_directory(user_id, project_id) + "/jbrowse"
        path = os.path.join(user_dir, name)
        return path
        
    def get_filter_from_params(self, params):
        f = filter()
        
        if params.has_key("nb_criteria"):
            for i in range(int(params["nb_criteria"])):
                cname = "criteria" + str(i)
                oname = "operators" + str(i)
                vname = "value" + str(i)
                c = selection_criteria(params[cname])
                c.operator = params[oname]
                c.value = (params[vname])
                f.add_criteria(c) 
        return f
        
    def prepare_jbrowse_tracks (self, params):
        prnas = []
        putative_rnas = {}
        
        filter = self.get_filter_from_params(params)
        (user_id,project_id) = self.get_ids_from_authkey(params["authkey"])
        prnas = filter.run(self.data_manager.get_putative_rnas(user_id,project_id))
        
        for prna in prnas:
            if putative_rnas.has_key(prna.genomic_sequence_id):
                if putative_rnas[prna.genomic_sequence_id].has_key(prna.program_name):
                    putative_rnas[prna.genomic_sequence_id][prna.program_name].append(prna)
                else :
                    putative_rnas[prna.genomic_sequence_id][prna.program_name] = [prna]
            else :
                putative_rnas[prna.genomic_sequence_id] = {}
                putative_rnas[prna.genomic_sequence_id][prna.program_name] = [prna]
        
        self.data_manager.prepare_jbrowse_tracks(user_id, project_id, putative_rnas)
