#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import os

from rnaspace.core.data_manager import data_manager

class download_model(object):
    """
    Files serve by the download url can not come from another place than
    tmp_dir.
    """

    def __init__(self):
        self.data_manager = data_manager()

    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_manager.get_ids_from_authkey(id)
        
    def file_path(self, name):
        tmp_dir = self.data_manager.config.get("storage", "tmp_dir")
        path = os.path.join(tmp_dir, name)
        return path
        
    def check_path(self, path):
        tmp_dir = self.data_manager.config.get("storage", "tmp_dir")
        # protect the reading of other than files in tmp_dir
        if os.path.dirname(path) != tmp_dir:
            return False

        return True

