/*
 * RNAspace: non-coding RNA annotation platform
 * Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function init_history(mount_point, user_id, project_id, authkey){


  $("a.history_info_section").click(function(){
    $(this).next(".info-content").toggle(200);
    return false;
  });

  // when we click on a toogletext element, display added sequences
  $("a.info_section").click( function () {
    $(this).next(".addedseq").slideToggle("normal");
    return false;
  });

  $("#back").click(function(){
		     var url = mount_point + "explore/index?authkey=" + authkey;
		     document.location = url;
		     return false;
		   });

}
