#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import json
import operator
import shutil

import rnaspace
from data_handler import data_handler
from user_handler import user_handler
from rnaspace.dao.sequence_handler import sequence_handler
from rnaspace.core.putative_rna import putative_rna
import rnaspace.core.common as common
from rnaspace.core.exceptions import disk_error, project_space_error, user_space_error

class jbrowse_handler (data_handler):
    """
    Class jbrowse_handler: this data handler control all the data dealing with jbrowse
    """

    def __init__(self):
        data_handler.__init__(self)
        self.user_handler = user_handler()
        # Create the symbolic link between jbrowse and user datas : created from the rnaspace_on_web script 
        if not os.path.islink("./rnaspace/ui/web/ressource/jbrowse/data"):
            os.symlink(self.config.get_storage_directory(), "./rnaspace/ui/web/ressource/jbrowse/data")
    
    def add_sequence (self, user_id, project_id, seq):
        
        size = 0
        max_user_size = common.get_nb_octet(self.config.get("storage", "user_size_limitation"))
        max_project_size = common.get_nb_octet(self.config.get("storage", "project_size_limitation"))
        user_space = self.user_handler.get_user_used_space(user_id)
        project_space = self.user_handler.get_project_used_space(user_id, project_id)
                                                                 
        jbrowse_dir = self.config.get_jbrowse_directory(user_id, project_id)
        refseq_path = os.path.join(jbrowse_dir, 'refSeqs.js')
        file_content = ""
        
        
        if os.path.isfile(refseq_path) :
                                          
            refseq_file = open(refseq_path,'r')
            for line in refseq_file.readlines():
                if not line.startswith("]"): # end of file
                    file_content += line
            file_content = file_content[:-1]
            refseq_file = open(refseq_path,'w')
            refseq_file.write(file_content+",\n")
            refseq_file.write(json.dumps({
                                     'length': len(seq.data),
                                     'name': seq.id,
                                     'seqDir' : "../../ressource/jbrowse/data/"+user_id+"/"+project_id+"/jbrowse/seq/" + seq.id,
                                     'seqChunkSize' : 20000,
                                     'end' : len(seq.data),
                                     'start' : 0
                                     }, sort_keys=True, indent=4))
            refseq_file.write("\n")
            refseq_file.write("]\n")
            refseq_file.close()
            
            size += self.get_disk_size(refseq_path)
            
            if size + user_space > max_user_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise user_space_error(user_id, project_id,
                                       disk_error.jbrowse_message)
                
            if size + project_space > max_project_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise project_space_error(user_id, project_id,
                                          disk_error.jbrowse_message)
        else :
            refseq_file = open(refseq_path,'w')
            refseq_file.write("refSeqs = \n[\n")
            refseq_file.write(json.dumps({
                                     'length': len(seq.data),
                                     'name': seq.id,
                                     'seqDir' : "../../ressource/jbrowse/data/"+user_id+"/"+project_id+"/jbrowse/seq/" + seq.id,
                                     'seqChunkSize' : 20000,
                                     'end' : len(seq.data),
                                     'start' : 0
                                     }, sort_keys=True, indent=4))
            refseq_file.write("\n")
            refseq_file.write("]\n")
            refseq_file.close()
            
            size += self.get_disk_size(refseq_path)
            
            
            if size + user_space > max_user_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise user_space_error(user_id, project_id,
                                       disk_error.jbrowse_message)
                
            if size + project_space > max_project_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise project_space_error(user_id, project_id,
                                          disk_error.jbrowse_message)
            
        if not os.path.isdir(os.path.join(jbrowse_dir, 'seq')) :
            os.mkdir(os.path.join(jbrowse_dir, 'seq'))
        
        seq_dir = os.path.join(os.path.join(jbrowse_dir, 'seq'), seq.id)
        if not os.path.isdir(seq_dir):
            os.mkdir(seq_dir)
        
        seqData = [seq.data[i:i+20000] for i in range(0, len(seq.data), 20000)]
        for i, piece in enumerate(seqData) :
            trunk = open(os.path.join(seq_dir, str(i)+".txt"),"w")
            trunk.write(piece)
            trunk.close()
            
            size += self.get_disk_size(seq_dir+"/"+str(i)+".txt")
            
            if size + user_space > max_user_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise user_space_error(user_id, project_id,
                                       disk_error.jbrowse_message)
                
            if size + project_space > max_project_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise project_space_error(user_id, project_id,
                                          disk_error.jbrowse_message)
        self.user_handler.update_project_used_space(user_id, project_id, size)
            
    def add_putative_rnas (self, user_id, project_id, prnas):
        self.prepare_tracks(user_id, project_id, prnas)
    
    def prepare_tracks (self, user_id, project_id, prnas):
        max_length_family_name = 8
        
        seq_handler = sequence_handler()
        size = 0
        max_user_size = common.get_nb_octet(self.config.get("storage", "user_size_limitation"))
        max_project_size = common.get_nb_octet(self.config.get("storage", "project_size_limitation"))
        user_space = self.user_handler.get_user_used_space(user_id)
        project_space = self.user_handler.get_project_used_space(user_id, project_id)
        

        jbrowse_dir = os.path.join(str(self.config.get_project_directory(user_id, project_id)),"jbrowse")
        if not os.path.isdir(jbrowse_dir) :
            os.mkdir(jbrowse_dir)
            for seq_id in prnas.keys() :
                sequence = seq_handler.get_sequence(user_id, seq_id, project_id)
                self.add_sequence(user_id, project_id, sequence)
                
        jbrowse_dir = self.config.get_jbrowse_directory(user_id, project_id)
        
        trackinfo_path = os.path.join(jbrowse_dir, 'trackInfo.js')
        trackinfo_file = open(trackinfo_path,'w')
        trackinfo_file.write("trackInfo = \n[\n")
        trackinfo_file.write(json.dumps({
                                    'args': { 'chunkSize' : 20000 },
                                    'url': "../../ressource/jbrowse/data/"+user_id+"/"+project_id+"/jbrowse/seq/{refseq}/",
                                    'type' : "SequenceTrack",
                                    'label' : "DNA",
                                    'key' : "DNA"
                                    }, sort_keys=True, indent=4))     
        trackinfo_file.write(",\n")
        
        if not prnas.keys():
            trackinfo_file.write("]\n")  
            

        refseq_path = os.path.join(jbrowse_dir, 'refSeqs.js')
        refseq_file = open(refseq_path,'w')
        
        for i,seq_id in enumerate(prnas.keys()) :
                        
            if not os.path.isdir(os.path.join(jbrowse_dir, 'tracks')) :
                os.mkdir(os.path.join(jbrowse_dir, 'tracks'))
            
            tracks_dir = os.path.join(os.path.join(jbrowse_dir, 'tracks'), seq_id)
            if not os.path.isdir(tracks_dir):
                os.mkdir(tracks_dir)
                
            sequence = seq_handler.get_sequence(user_id, seq_id, project_id)
            if i == 0:
                refseq_file.write("refSeqs = \n")
                refseq_file.write("[\n")
            refseq_file.write(json.dumps({
                                     'length': len(sequence.data),
                                     'name': seq_id,
                                     'seqDir' : "../../ressource/jbrowse/data/"+user_id+"/"+project_id+"/jbrowse/seq/" + seq_id,
                                     'seqChunkSize' : 20000,
                                     'end' : len(sequence.data),
                                     'start' : 0
                                     }, sort_keys=True, indent=4))
            refseq_file.write("\n")
            if i != len(prnas.keys())-1:
                refseq_file.write(",")
            if i == len(prnas.keys())-1:
                refseq_file.write("]\n")
            
            if (i ==0):
                trackinfo_file.write(json.dumps({
                                         'url' : "../../ressource/jbrowse/data/"+user_id+"/"+project_id+"/jbrowse/tracks/{refseq}/ncRNA/trackData.json",
                                         'label' : "ncRNA",
                                         'type' : "FeatureTrack",
                                         'key' : "ncRNA"
                                         }, sort_keys=True, indent=4))

                trackinfo_file.write("]\n")     

            
            feature_dir = os.path.join(tracks_dir, "ncRNA")
            if not os.path.isdir(feature_dir):
                os.mkdir(feature_dir)
                
            trackdata = open(os.path.join(feature_dir, "trackData.json"),"w")
            jsonline = "{\"histogramMeta\":[{\"basesPerBin\":\"200000\",\"arrayParams\":{\"length\":1,\"chunkSize\":10000}}],\"headers\":[\"start\",\"end\",\"strand\",\"id\",\"name\"],"
            jsonline += "\"featureCount\":"
            
            len_feat = 0
            for predictor_name in prnas[seq_id].keys() :
                len_feat += len(prnas[seq_id][predictor_name])
                
            jsonline += str(len_feat)+",\"featureNCList\":["
            
            prnatab = []
            for predictor_name in prnas[seq_id].keys() :
                for prna in prnas[seq_id][predictor_name]:
                    tmpprnatab = []
                    tmpprnatab.append(prna.start_position)
                    tmpprnatab.append(prna.stop_position)
                    tmpprnatab.append(prna.strand)
                    if len(prna.family) > max_length_family_name :
                        tmpprnatab.append(prna.user_id+"/"+prna.family[0:max_length_family_name]+".")
                    else :
                        tmpprnatab.append(prna.user_id+"/"+prna.family[0:max_length_family_name])
                    tmpprnatab.append(prna.user_id)
                    prnatab.append(tmpprnatab)
                
                prnatab = sorted(prnatab, key=operator.itemgetter(0))
                            
            for tmpprnatab in prnatab:
                jsonline += "["
                jsonline += str(tmpprnatab[0]) + "," + str(tmpprnatab[1]) + ","
                if tmpprnatab[2] == "+":
                    jsonline += "1,"
                elif tmpprnatab[2] == "-":
                    jsonline += "-1,"
                else:
                    jsonline += "0,"
                jsonline += "\"" + tmpprnatab[4] + "\",\"" + tmpprnatab[3] + "\"],"
                            
            jsonline = jsonline[:-1]   
            jsonline += "],"
            jsonline += "\"lazyIndex\":2,"
            jsonline += "\"key\":\"ncRNA\","
            jsonline += "\"className\":\"feature5\","
            jsonline += "\"urlTemplate\":\"" + self.config.get("global", "mount_point") + "explore/rnavisualisation/index?authkey="+user_id+"-"+project_id+"&mode=display&rna_id={id}\","
            jsonline += "\"histStats\":[{\"bases\":\"100\",\"max\":1,\"mean\":1}],"
            jsonline += "\"label\":\"ncRNA\","
            jsonline += "\"type\":\"FeatureTrack\","
            jsonline += "\"sublistIndex\":5}"
            trackdata.write(jsonline)
            trackdata.close()
        trackinfo_file.close()
        refseq_file.close()
        
        size += self.get_disk_size(trackinfo_path)
            
        if size + user_space > max_user_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise user_space_error(user_id, project_id,
                                       disk_error.jbrowse_message)
                
        if size + project_space > max_project_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise project_space_error(user_id, project_id,
                                          disk_error.jbrowse_message)
    
