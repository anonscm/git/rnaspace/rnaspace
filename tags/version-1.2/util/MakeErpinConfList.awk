# Script to extract the RNA family names (list to put in erpin.conf)
# from the file epn.dat included in the ERPIN distribution
# 
# use: awk -F/ -f MakeErpinFamilies.awk epn.dat > tmp_conf_list
# 
# Then the file tmp_conf_list is manually edited to:
# - replace [BACEUKARCVIR] (and all combinations) by [B/E/A/V] (and appropriate values)
# - suppress multiple space in the list
#
# Finally, the list is copied-pasted at the rigth location in erpin.conf

INIT {buf = ""}
{
buf=buf $5 " [" $3 "],"
}
END {print buf}
