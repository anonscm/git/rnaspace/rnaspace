#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import unittest

from rnaspace_testcase import rnaspace_testcase
from rnaspace.core.exploration.filter import selection_criteria, filter
from rnaspace.ui.web.model.explore.explore_model import explore_model


# test with E. coli, a subset of predictors,
# a filter on predictions that are compared in Fasta format with reference predictions
class test_ecoli (rnaspace_testcase):

    def test(self):

        self.testcase_name = 'ecoli'
        self.sequences_id = ['NC_000913', 'NC_000913bis']
        predictor_list = [ [ 'darn', {'d':'Lysine [B/A/E]'}, {} ],
                           [ 'erpin', {'training_set': 'Bacterial Rnase P class A [B]'}, {} ],
                           [ 'comparative_analysis',
                             {'conservation_soft': 'blast', 'inference_soft': 'caRNAc',
                              'conservation_soft_opts': None, 'inference_soft_opts': None,
                              'aggregation_soft': 'CG-seq', 'aggregation_soft_opts': None},
                             {'species': ['Buchnera_aphidicola [2 species]']}],
                           [ 'atypicalgc', {'F':'True','T':'all','W':101,'L':200}, {} ] ]

        testfilter = filter()
        testfilter.add_criteria(selection_criteria("size","<=","400"))

        self.init_seq(self.sequences_id)
        self.rnas = []        
        for (predictor_name, opts, p) in predictor_list:
            result = self.launch_predictor(predictor_name, opts, p)
        self.rnas = testfilter.run(result)
        self.convert_compare_result('FASTA')


def suite_ecoli():
    test_ecoli_suite = unittest.TestSuite()
    test_ecoli_suite.addTest(test_ecoli('test'))
    return test_ecoli_suite


if __name__ == '__main__':
    unittest.main()


