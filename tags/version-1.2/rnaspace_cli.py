#!/usr/bin/env python

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""

"""

import os
import sys
import ConfigParser
from optparse import OptionParser
import curses
import shutil

import rnaspace
from rnaspace.ui.cli.helpwindow import helpWindow
from rnaspace.ui.web.model.manage.manage_model import manage_model
from rnaspace.core.prediction.software_manager import software_manager
from rnaspace.core.data_manager import data_manager
from rnaspace.core.id_tools import id_tools
from rnaspace.core.sequence import sequence
import rnaspace.core.logger as logger
from rnaspace.core.trace.event import add_seq_event
from rnaspace.core.trace.event import disk_error_event
import rnaspace.core.common as common_core

USAGE = """Usage:
  python rnaspace_cli.py [Options] RUN_CONFIGURATION_FILE

"""

DESCRIPTION = """This script is the command line interface of the RNAspace
platform. It takes a ini file as parameter. This file describes the
properties of the gene finder to execute. 
An help system has been built to help you to write the configuration file.
See below for details.
"""

OPTIONS = """
Options:
  -r RNASPACE_CONF, --rnaspace=RNASPACE_CONF
  \t RNAspace configuration file
  -p PREDICTORS_CONF_DIR, --predictors_conf_dir=PREDICTORS_CONF_DIR
  \t Directory that contains the predictors conf files

Help Options:
  -h interactive
  \t enter in the interactive help
  -h full
  \t display all the help pages. It can be huge, so it is better to save it in
  \t a file: python rnaspace_cli.py -h full > help_rnaspace.txt
  -h gene-finder
  \t display the list of the available gene finder
  -h organisms
  \t display the list of the available organisms
  -h output-formats
  \t display the list of available export formats
  -h GENE_FINDER_NAME
  \t display the help of GENE_FINDER_NAME
"""


COMPARATIVE_ACTIVATED = True
KONWN_ACTIVATED = True
ABINITIO_ACTIVATED = True

# user id used for the command line interface
USER_ID = "cli"

# colors used to print in the terminal
COLORS_ACTIVATED = True
COLORS = {"FAIL": '\033[91m',
          "OKBLUE": '\033[94m',
          "OKGREEN": '\033[92m',
          "WARNING": '\033[93m'}

# output formats
FORMATS = ["rnaml", "fasta", "gff", "apollo_gff", "csv"]

class MyConfigParser(ConfigParser.ConfigParser):
    # default ConfigParser return a lowercase version of the options,
    # we do not want that.
    def optionxform(self, option):
        return str(option)


class RunConfiguration(object):
    """ In charge of reading the configuration file and of launching the run.

    Before launching the run, a call to set_up must be done.    
    """

    def __init__(self, user_id, sm, dm, mm):
        """
        - user_id: the id of the user
        - sm: instance of the software_manager
        - dm: instance of the data_manager
        - mm: instance of manage_model
        """
        
        self.user_id = user_id
        self.data_manager = dm
        self.software_manager = sm
        self.manage_model = mm
        
        self.project_id = None
        self.run_id = None
        
        # list of emails
        self.emails = ""

        # list of  sequences ids
        self.sequences = []

        # dictionary that contains the gene finder and their options
        # self.gene_finder[softname] = {type: softtype, opts:{},
        #                               params:{}}
        #
        # - opts is a dictionary where keys are softname options and values
        #   are their corresponding values.
        # - params is dictionary where keys are the names of the parameters
        #   and values are their corresponding values.
        #
        # A parameter is an special case of the options: it is not an option
        # of the gene finder but some gene finder requires a database or a
        # list of organisms to be launched.
        self.gene_finder = {}

        # By default, combine disable
        self.combine = False

        # By default, use Rfam 9.1
        self.default_database = "Rfam_9.1"
        
        # By default, no export
        self.export = False
        self.export_output = None
        self.export_format = "gff"
        self.setup = False

    def run(self):
        """Launch the run."""

        if not self.setup:
            return
        
        self.run_id = self.data_manager.new_run(self.user_id, self.project_id)

        # we add the jobs to the software_manager
        for softname in self.gene_finder:
            softype = self.gene_finder[softname]['type']
            if softype == "known" and not KONWN_ACTIVATED:
                print_term("## Homology search not activated, skipping %s." %softname, COLORS["WARNING"])
                continue
            elif softype == "comparative" and not COMPARATIVE_ACTIVATED:
                inf = self.gene_finder[softname]['opts']['inference_soft']
                cons = self.gene_finder[softname]['opts']['conservation_soft']
                agg = self.gene_finder[softname]['opts']['aggregation_soft']
                pipe = "%s > %s > %s" % (cons, agg, inf)
                print_term("## Comparative analysis  not activated, skipping %s."%pipe, COLORS["WARNING"])
                continue
            elif softype == "abinitio" and not ABINITIO_ACTIVATED:
                print_term("## Abinitio not activated, skipping %s." %softname, COLORS["WARNING"])
                continue
            
            self.software_manager.add_job(self.user_id, self.project_id,
                                          self.run_id, softname,
                                          self.gene_finder[softname]['type'],
                                          self.gene_finder[softname]['opts'],
                                          self.gene_finder[softname]['params'])
        # and we launch the jobs
        self.software_manager.launch(self.user_id, self.project_id,
                                     self.run_id, self.combine)

        # if the user wants to export the results
        if self.export:
            mail = self.data_manager.get_user_email(self.user_id,
                                                    self.project_id)
            rnas_to_export= self.data_manager.get_putative_rnas(self.user_id,
                                                                self.project_id)
            rnas_id_to_export = []
            for r in rnas_to_export:
                rnas_id_to_export.append(r.user_id)

            path = self.data_manager.create_export_file(self.user_id,
                                                        self.project_id,
                                                        rnas_to_export,
                                                        self.export_format)
            shutil.copy(path, self.export_output)
            
    def set_up(self, conf_file):
        """ Read the configuration file and set the run attributes.

        Return a tuple (error, message, warnings) where:
            - error: True if a blocking error occured
            - message: the error message
            - warnings: a list of warning message
            
        A warning is an error that is not blocking for the run (an option
        that does not exist for example).
        """

        # the list of warning messages
        warnings = []

        # read the conf and validate it. If not valid return.
        conf = self.__read_conf(conf_file)
        (valid, mess) = self.__validate_conf(conf)
        if not valid:
            return (True, mess, warnings)

        # crete a new project
        self.project_id = self.data_manager.new_project(self.user_id)
        
        self.emails = conf["Main"]["email"]
        if conf["Main"].has_key("combine"):
            if conf["Main"]["combine"].lower().strip() == 'true':
                self.combine = True                
        if conf["Main"].has_key("export_output"):
            self.export_output = conf["Main"]["export_output"].strip()
            self.export = True
        if conf["Main"].has_key("export_format"):
            self.export_format = conf["Main"]["export_format"].strip().lower()

        self.data_manager.save_user_email(self.user_id, self.project_id,
                                          self.emails)

        # add the sequences to the project
        inputs = conf["Main"]["inputs"].split(',')
        for input_sec in inputs:
            path = conf[input_sec]["path"]
            name = conf[input_sec]["name"]
            domain = conf[input_sec]["domain"]
            species = "unknown"
            strain = "unknown"
            replicon = "unknown"
            if conf[input_sec].has_key("species"):
                species = conf[input_sec]["species"]
            if conf[input_sec].has_key("strain"):
                strain = conf[input_sec]["strain"]
            if conf[input_sec].has_key("replicon"):
                replicon = conf[input_sec]["replicon"]
            (error, mess) = self.__add_sequences(path, name, domain, species,
                                                 strain, replicon)
            if error:
                return (True, mess, warnings)

        self.sequences = self.data_manager.get_sequences_id(self.user_id,
                                                            self.project_id)

        # set up the gene finder.
        # The comparative part is done differently that the other part beause
        # it contains different gene finder (inference, conservation and
        # aggregation.
        softs = conf["Main"]["run"]
        for soft in softs.split(','):
            if soft != "Comparative":
                self.gene_finder.setdefault(soft, {})
                soft_type = self.software_manager.get_software_type(soft)
                if soft_type.startswith('known_'):
                    soft_type = "known"

                (error, error_msg, warning,
                 opts, params) = self.__set_up_gene_finder(soft, soft_type,
                                                           conf)
                if error:
                    return (error, error_msg, warnings)
                if len(warning) > 0:
                    warnings.extend(warning)
                    
                self.gene_finder[soft]['type'] = soft_type
                self.gene_finder[soft]['opts'] = opts
                self.gene_finder[soft]['params'] = params
                
            # comparative part
            else:
                comp = conf['Comparative']
                organisms = comp["organisms"].split(',')
                
                inf = self.software_manager.get_software_by_type("inference")
                agg = self.software_manager.get_software_by_type("aggregation")
                cons = self.software_manager.get_software_by_type("conservation")
                params = {}
                params['species'] = organisms

                # if the user has not specified the software he wants to use,
                # we take the first one of each type.
                opts = {}
                opts['conservation_soft'] = cons[0].name
                opts['aggregation_soft'] = agg[0].name
                opts['inference_soft'] = inf[0].name
                opts['conservation_soft_opts'] = None
                opts['aggregation_soft_opts'] = None
                opts['inference_soft_opts'] = None

                if comp.has_key('conservation_soft'):
                    opts['conservation_soft'] = comp['conservation_soft']
                if comp.has_key('aggregation_soft'):
                    opts['aggregation_soft'] = comp['aggregation_soft']
                if comp.has_key('inference_soft'):
                    opts['inference_soft'] = comp['inference_soft']


                (error, error_msg, warning, cons_opts, 
                 p) = self.__set_up_gene_finder(opts['conservation_soft'],
                                                "conservation", conf)
                if error:
                    return (error, error_msg, warnings)
                if len(warning) > 0:
                    warnings.extend(warning)
                opts['conservation_soft_opts'] = cons_opts

                (error, error_msg, warning, agg_opts,
                 p) = self.__set_up_gene_finder(opts['aggregation_soft'],
                                                "aggregation", conf)
                if error:
                    return (error, error_msg, warnings)
                if len(warning) > 0:
                    warnings.extend(warning)                
                opts['aggregation_soft_opts'] = agg_opts

                (error, error_msg, warning, inf_opts,
                 p) = self.__set_up_gene_finder(opts['inference_soft'],
                                                "inference", conf)
                if error:
                    return (error, error_msg, warnings)
                if len(warning) > 0:
                    warnings.extend(warning)                
                opts['inference_soft_opts'] = inf_opts
                
                self.gene_finder["comparative"] = {}
                self.gene_finder["comparative"]['type'] = "comparative"
                self.gene_finder["comparative"]['opts'] = opts
                self.gene_finder["comparative"]['params'] = params
                
        self.setup = True
        return (False, mess, warnings)

    def __set_up_gene_finder(self, soft_name, soft_type, conf):
        """Return the options and paramater dictionary of a gene finder.

        This function return a tuple (error, message, warning, opts, params):
            - error: True if a blocking error occured
            - message: the error message
            - warnings: a list of warning messages
            - opts: the options of the gene finder
            - the parameters of the gene finder
        """

        
        bad_opt = "## Unknown option '%s' for '%s'. Ignoring it."

        # get the software instance.
        soft_inst = self.software_manager.get_software(soft_name, soft_type)
        params = {}
        warnings = []

        # if the user has specified the options of the gene finder
        if conf.has_key(soft_name):
            # we first set the default options and then modify them with the
            # user ones
            opts = soft_inst.get_default_opts()
            for opt in conf[soft_name]:
                if opt != 'database':
                    # if opt is a simple option (ie a switch), then
                    # we delete it from the options if it is set to False.
                    if opt in soft_inst.simple_options:
                        if conf[soft_name][opt].lower() == 'true':
                            opts[opt] = 'on'
                        elif opt in opts:
                            del opts[opt]
                    elif opt in soft_inst.long_options:
                        opts[opt] = conf[soft_name][opt]
                    # the option does not exist
                    else:
                        warnings.append( bad_opt % (opt, soft_name))
                else:
                    params['db'] = conf[soft_name][opt] + '.fasta'
        else:
            opts = None

        # if the gene finder needs a database and no one is set, take the
        # default one.
        if soft_inst.database == "1" and not params.has_key('db'):
            params['db'] = self.default_database + '.fasta'

        if opts is not None:
            # we check the values of the options.
            errors = soft_inst.check_parameters(opts)
            if len(errors) > 0:
                error_msg = ""
                for item in errors:
                    error_msg += "%s: %s\n" % (item, errors[item])
                return (True, error_msg, [], None, None)
                    
        # error, error_msg, warnings, opts, params
        return (False, "", warnings, opts, params)

    def __add_sequences(self, path, name, domain, species, strain, replicon):
        """Add the sequences contained in the 'path' file to the project.

        return a tuple (error, message) where:
           - error is set to True if an error occured
           - message is the error details
        """

        too_many = "Too many sequences into '%s' !\n" % path
        too_many += 'Maximum allowed is %i per fasta file.'
        too_many = too_many % self.data_manager.get_nb_sequences_limitation()
        nbseqmax = self.data_manager.get_nb_sequences_limitation()

        # if the name provided by the user is already used, generate another one
        existing_ids = self.data_manager.get_sequences_id(self.user_id,
                                                          self.project_id)
        if name in existing_ids:
            id_t = id_tools()
            seqid = id_t.get_new_sequence_id(self.user_id, self.project_id)
        else:
            seqid = name

        # read the fasta file and create the 'sequence' objects.
        # if the file contains several sequences, then  a suffix is added to the
        # name.
        fseq = open(path, 'r')
        seq_data = ''
        i = 1
        seqs = []
        for line in fseq:
            if line.startswith('>'):
                if len(seqs) >= nbseqmax - 1:
                    return (True, too_many)
                else:
                    if seq_data != '':
                        tmp_id = seqid + "." + str(i)
                        seq = sequence(tmp_id, seq_data, domain, species,
                                       replicon, strain)
                        seqs.append(seq)
                        i = i + 1
                    seq_data = line.replace('\r', '')
            else:
                temp = line.upper()
                temp = temp.replace('\r', '')
                seq_data += temp
        if i != 1:
            tmp_id = seqid + "." + str(i)
        else:
            tmp_id = seqid
        seq = sequence(tmp_id, seq_data, domain, species, replicon, strain)
        seqs.append(seq)
        fseq.close()

        # the file is parsed, we can now really add the sequences to the project
        events = []
        for seq in seqs:

            (error, e) = self.__add_sequence(seq)
            if not error:
                events.append(e)
            else:
                project_size = self.data_manager.get_project_size(self.user_id, self.project_id)
                ev = disk_error_event(self.user_id, self.project_id, "-",
                                      e,  project_size)
                self.data_manager.update_project_trace(self.user_id,
                                                       self.project_id,
                                                       [ev])
                return (True, e)
        
        self.data_manager.update_project_trace(self.user_id, self.project_id,
                                               events)

        return (False, "")
        
    def __add_sequence(self, seq):
        """Add a sequence to the project.

        The function first checks if some space is available.

        return a tuple (error, message) where:
           - error is set to True if an error occured
           - message is the error details if error is True, or a
             add_seq_event event if error is False.
        """
        
        max_length = self.data_manager.get_sequence_size_limitation()
        current_size = self.data_manager.get_user_sequences_used_space(self.user_id, self.project_id)
        if len(seq.data) + current_size < max_length:
            self.data_manager.add_sequence(self.user_id, self.project_id, seq)

            e = add_seq_event(self.user_id, self.project_id, "-", seq.id,
                              len(seq.data),
                              self.data_manager.get_sequence_header(self.user_id, self.project_id, seq.id), 0)
            return (False, e)

        else:
            space_left = max_length - current_size
            if space_left < 0:
                space_left = 0
            space_left = common_core.get_octet_string_representation(space_left)
            space_error = 'Sequence length is way too long! You have %s'
            space_error = space_error % space_left
            return (True, space_error)



    def __read_conf(self, conf_file):
        """ Load the configuration file into a dictionary. """
        
        conf = {}
        
        cp = MyConfigParser()
        cp.read(conf_file)
        
        for sec in cp.sections():
            for opt in cp.options(sec):
                value = cp.get(sec, opt)
                conf.setdefault(sec, {})
                if value.startswith('"') and value.endswith('"'):
                    value = value[1:-1]
                conf[sec][opt] = value

        return conf


    def __validate_conf(self, conf):
        """ Check if the configuration is ok.

        Return a tuple (bool, mess). bool is False if the conf is not valid.
        If so, mess is set with the error message.
        """

        # the error messages
        missing_opt = "## Missing '%s' option in the [%s] section."
        missing_sec = "## [%s] section does not exist and is mandatory."
        missing_val = "## Please fill the '%s' option of the [%s] section."
        file_error = "## Can not find the file %s for the %s input."
        format_error = "## Unknown format %s (available formats: " + (
            ', '.join(f for f in FORMATS)) + ")."
        soft_error = "## The software %s does not exists"
        organisms_error = "## Can not find %s ."
        fasta_error = "## The file %s is not in fasta format"
        db_error = "## Unknown database '%s' for gene finder '%s'"
        
        if not conf.has_key("Main"):
            return (False, missing_sec % "Main")

        # check the mandatory options of the main section
        mainsec = conf["Main"]
        if not mainsec.has_key("inputs"):
            return (False, missing_opt % ("inputs", "Main"))
        if not mainsec.has_key("run"):
            return (False, missing_opt % ("run", "Main"))
        if not mainsec.has_key("email"):
            return (False, missing_opt % ("email", "Main"))

        inputs = mainsec["inputs"]
        inputs = inputs.split(',')
        for input_sec in inputs:
            # check if the inputs have their section 
            if input_sec not in conf:
                return (False, missing_sec % input_sec)

            else:
                # check if the mandatory options are here
                if not conf[input_sec].has_key("path"):
                    return (False, missing_opt % ("path", input_sec))
                if not conf[input_sec].has_key("name"):
                    return (False, missing_opt % ("name", input_sec))
                if not conf[input_sec].has_key("domain"):
                    return (False, missing_opt % ("domain", input_sec))

                # does the sequence file exists
                if not os.path.isfile(conf[input_sec]["path"]):
                    return (False, file_error % (conf[input_sec]["path"],
                                                 input_sec))
                # if the sequence is not in fasta format
                ffasta = open(conf[input_sec]["path"], "r")
                if not self.manage_model.valid_fasta_sequence(ffasta):
                    ffasta.close()
                    return (False, fasta_error % (conf[input_sec]["path"]))
                ffasta.close()

        # check if the output format is available
        if mainsec.has_key("export_format"):
            if mainsec["export_format"].lower() not in FORMATS:
                return (False, format_error % mainsec["export_format"])

        # check if the software exists
        softs = mainsec["run"].split(',')
        dbs = self.data_manager.get_db_names()
        if dbs is None:
            available_dbs = []
        else:
            available_dbs = ['.'.join(name.split(".")[:-1]) for name in dbs]
        if len(softs) < 1 or (len(softs) == 1 and softs[0] == ''):
            return (False, missing_val % ("run", "Main"))
        for soft in softs:                
            if(soft != "Comparative" and
               self.software_manager.get_software_type(soft) == ''):
                return (False, soft_error % soft)
            # check database if provided
            if conf.has_key(soft) and conf[soft].has_key("database"):
                if conf[soft]["database"] not in available_dbs:
                    return (False, db_error % (conf[soft]["database"], soft))

        if "Comparative" in softs and not conf.has_key("Comparative"):
            return (False, missing_sec % "Comparative")

        # check the comparative section if it exists
        if COMPARATIVE_ACTIVATED and conf.has_key("Comparative"):
            comp = conf["Comparative"]

            # check the organisms
            if not comp.has_key("organisms"):
                return (False, missing_opt % ("organisms", "Comparative"))

            organisms = get_organisms(self.data_manager)
            for org in comp["organisms"].split(','):
                if org not in organisms:
                    return (False, organisms_error % org)

            # check the Comparative software
            if comp.has_key("conservation_soft"):
                if self.software_manager.get_software(comp["conservation_soft"],
                                                      "conservation") == '':
                    return (False, soft_error % comp["conservation_soft"])
            if comp.has_key("aggregation_soft"):
                if self.software_manager.get_software(comp["aggregation_soft"],
                                                      "aggregation") == '':
                    return (False, soft_error % comp["aggregation_soft"])
            if comp.has_key("inference_soft"):
                if self.software_manager.get_software(comp["inference_soft"],
                                                      "inference") == '':
                    return (False, soft_error % comp["inference_soft"])

        return (True, None)


        
def print_term(message, color=None):
    """ Print 'message' with the color 'color' if not None. """
    
    endc = '\033[0m'
    
    if color is None or not COLORS_ACTIVATED:
        print message
    else:
        print color + message + endc


def get_opt_parser():
    """ Build the options parser. """
    
    parser = OptionParser(add_help_option=False)
    parser.add_option("-r", "--rnaspace",
                      action="store",
                      type="string",
                      dest="rnaspace",
                      default="cfg/rnaspace.cfg",
                      metavar="RNASPACE_CONF",
                      help="RNAspace configuration file")
    parser.add_option("-p", "--predictors_conf_dir",
                      action="store",
                      type="string",
                      dest="predictors_conf_dir",
                      default="cfg/predictors",
                      metavar="PREDICTORS_CONF_DIR",
                      help="Directory that contains the predictors conf files")

    return parser

def get_databases(data_manager):
    dbs = data_manager.get_db_names()
    if dbs is None:
        return  []
    available_dbs = ['.'.join(name.split(".")[:-1]) for name in dbs]
    return available_dbs

def get_databases_str(data_manager):
    dbs = get_databases(data_manager)
    if len(dbs) == 0:
        return "No databases found."
    return "\n".join(db for db in dbs)

def get_organisms(data_manager):
    """Return the list of available organisms."""
    domain = ["bacteria"]
    organisms = data_manager.get_species_names(domain)
    if organisms is None:
        return []
    return organisms

def get_organisms_str(data_manager):
    """Return the list of available organisms as a string."""
    organisms = get_organisms(data_manager)
    if len(organisms) == 0:
        return "No organisms found."
    return '\n'.join(o for o in organisms)

def get_output_formats_str():
    """Return the available output formats as a string."""
    return '\n'.join(f for f in FORMATS)

def get_gene_finders_str(software_manager):
    """Return the available gene finder as a string."""
    
    softs = software_manager.software
    softs_str = ""
    inf = softs["inference"]
    agg = softs["aggregation"]
    cons = softs["conservation"]
    known = softs["known"]
    abinitio = softs["abinitio"]
    softs_str += "## Homology\n"
    for soft in known:
        softs_str += '\t' + soft.name + "\n"
    softs_str += "\n## Comparative Analysis\n"
    softs_str += "\t# Sequence alignement\n"
    for soft in cons:
        softs_str += '\t\t' + soft.name + "\n"
    softs_str += "\t# Sequence aaggregation\n"
    for soft in agg:
        softs_str += '\t\t' + soft.name + "\n"
    softs_str += "\t# Structure inference\n"
    for soft in inf:
        softs_str += '\t\t' + soft.name + "\n"
    softs_str += "\n## Ab initio\n"
    for soft in abinitio:
        softs_str += '\t' + soft.name + "\n"

    return softs_str

def get_gene_finder_help_str(asked_soft):
    """Return the help of 'asked_soft' as a string."""

    opt_str = """
    %s:

    %s

    * Default value: %s
    * Accepted value: %s

    
    """
    
    h_str = asked_soft.name  +"\n\n"
    for opt in asked_soft.simple_options + asked_soft.long_options:
        if opt != '':
            h = asked_soft.options_desc[opt].replace('<br />', '\n')
            if h[-1] == ':':
                h = h[:-1]
            if opt in asked_soft.long_options:
                default = asked_soft.options_default[opt]
                possible = asked_soft.options_params[opt]
                if isinstance(possible, list):
                    if len(possible) == 1 and possible[0] == "":
                        possible_str = "<string>"
                    else:
                        possible_str = " | ".join(p for p in possible)
                else:
                    if possible == "":
                        possible_str = "<string>"
                    else:
                        possible_str = possible
            else:
                default = opt in asked_soft.default_simple
                possible_str = "True | False"
                
            h_str += opt_str % (opt, h, str(default), str(possible_str))
    h_str += "___________________________________________________________\n\n"
    return h_str


def get_full_help(data_manager, software_manager):
    """ Return the full help as a string.
    
    Build a string containing the help of all the gene finder, the list
    of the available organisms and output formats. Can be huge.
    """
    
    softs = software_manager.software
    inf = softs["inference"]
    agg = softs["aggregation"]
    cons = softs["conservation"]
    known = softs["known"]
    abinitio = softs["abinitio"]

    h_str = """
%s

* DATABASES
===========
%s

* EXPORT FORMATS
=================
%s
    
* GENE FINDER
===============

- Homology
-----------
%s

Comparative Analysis
--------------------

Sequence conservation
%s

Sequence aggregation
%s

Structure inference
%s

- Ab initio
--------------------
%s

* ORGANISMS
==============
%s


"""

    known_str = "\n"
    for soft in known:
        known_str += get_gene_finder_help_str(soft)

    cons_str = "\n"
    for soft in cons:
        cons_str += get_gene_finder_help_str(soft)

    agg_str = "\n"
    for soft in agg:
        agg_str += get_gene_finder_help_str(soft)

    inf_str = "\n"
    for soft in inf:
        inf_str += get_gene_finder_help_str(soft)

    ab_str = "\n"
    for soft in abinitio:
        ab_str += get_gene_finder_help_str(soft)


    org = get_organisms_str(data_manager)
    output_formats = get_output_formats_str()
    help_str = USAGE + DESCRIPTION + OPTIONS
    dbs = get_databases_str(data_manager)
    return h_str % (help_str, dbs, output_formats, known_str, cons_str, agg_str,
                    inf_str, ab_str, org)
    

def display_help(item, parser, software_manager, data_manager):
    """ Display the help section definied by 'item'.

    If 'item' is None, then display, the general help.
    """

    # curses function
    def curses_help(stdscr):
        # try disabling the cursor
        try:
            curses.curs_set(0)
        except:
            pass
            
        organisms = get_organisms(data_manager)
        softs = software_manager.software
        usage = USAGE + DESCRIPTION + OPTIONS
        formats = FORMATS
        dbs = get_databases(data_manager)
        main = helpWindow(stdscr, usage, softs, organisms, formats, dbs)
        main.run()


    if item is None:
        print USAGE
        print DESCRIPTION
        print OPTIONS

    elif item.lower() == "interactive":
        curses.wrapper(curses_help)

    elif item.lower() == "full":
        print get_full_help(data_manager, software_manager)
        
    elif item.lower() == "organisms":
        print get_organisms_str(data_manager)
        
    elif item.lower() == 'gene-finder':
        # print the gene finder by type
        print get_gene_finders_str(software_manager)
        
    elif item.lower() == "output-formats":
        print get_output_formats_str()
        
    # the user asked for a gene finder help
    else:
        softs = software_manager.software
        ok = False
        for soft_type in softs:
            for soft in softs[soft_type]:
                if soft.name == item:
                    
                    asked_soft = soft
                    ok = True
                    break
            if ok:
                break

        if ok:
            print "## option\tdescription\tdefault value\n"
            print get_gene_finder_help_str(asked_soft)
        else:
            print_term("## Sorry, can not find %s in help." % item,
                       COLORS['FAIL'])


def comparative_activated(dm, sm):
    cons = sm.get_software_by_type("conservation")
    agg = sm.get_software_by_type("aggregation")
    inf = sm.get_software_by_type("inference")
    domain = ["bacteria"]
    species = dm.get_species_names(domain)

    if len(cons) == 0 or len(agg) == 0 or len(inf) == 0 or species is None:
        message = ""
        if len(cons) == 0:
            message += "No conservation soft found, "
        if len(inf) == 0:
            message += "No inference soft found, "
        if len(agg) == 0:
            message += "No aggregation soft found, "
        if species is None:
            message += "No genomes found, "
        message += "comparative analysis disable"
        return False
    else:
        return True

def known_activated(sm):
    known = sm.get_software_by_type("known")
    if len(known) == 0:
        message = "No known soft found, known RNAs part disable"
        return False
    else:
        return True

def abinitio_activated(sm):
    abinitio = sm.get_software_by_type("abinitio")
    if len(abinitio) == 0:
        message = "No known soft found, known RNAs part disable"
        return False
    else:
        return True

        
def main():

    # parse the command line to extract the help part if present.
    opts_help = False
    opts_items = None
    opts_help_index = 0
    for (i, a) in enumerate(sys.argv):
        if a == '-h' or a == '--help':
            opts_help = True
            opts_help_index = i
            break
        else:
            opts_help = False

    # remove the help option and value from the command line, so we can
    # pass the new command line to optparse.
    # We need to do that because optparse can not deal with options that
    # take optionnal value.
    argv = sys.argv[1:]
    if opts_help:
        if(len(argv) > opts_help_index and
           not argv[opts_help_index].startswith('-')):
            opts_items = argv[opts_help_index]
            del argv[opts_help_index]
        del argv[opts_help_index-1]

    parser = get_opt_parser()
    (opts, args) = parser.parse_args(args=argv)
            
    predictors_conf_dir = opts.predictors_conf_dir        
    conf_rnaspace = opts.rnaspace

    if not os.path.isfile(conf_rnaspace):
        print_term('## Can not find "%s"\n' % conf_rnaspace, COLORS['FAIL'])
        print USAGE
        print DESCRIPTION
        print OPTIONS
        return
    if not os.path.isdir(predictors_conf_dir):
        print_term('## "%s" is not a directory\n' % predictors_conf_dir,
                   COLORS['FAIL'])    
        print USAGE
        print DESCRIPTION
        print OPTIONS
        return


    rnaspace.update_conf(conf_rnaspace, predictors_conf_dir)
    logger.init()
    sm = software_manager()
    dm = data_manager()
    mm = manage_model()

    global COMPARATIVE_ACTIVATED
    global KONWN_ACTIVATED
    global ABINITIO_ACTIVATED

    COMPARATIVE_ACTIVATED = comparative_activated(dm, sm)
    KONWN_ACTIVATED = known_activated(sm)
    ABINITIO_ACTIVATED = abinitio_activated(sm)
    
    if opts_help:
        display_help(opts_items, parser, sm, dm)        
        return
    
    if not opts_help and len(args) < 1:
        print_term('## Missing run configuration file\n', COLORS['FAIL'])
        print USAGE
        print DESCRIPTION
        print OPTIONS
        return
    
    conf_file = args[0]

    if not os.path.isfile(conf_file):
        print_term("## %s does not exist" % conf_file, COLORS['FAIL'])
        return

    run_conf = RunConfiguration(USER_ID, sm, dm, mm)
    (error, mess, warnings) = run_conf.set_up(conf_file)
    if error:
        print_term(mess, COLORS['FAIL'])
        return
    if len(warnings) > 0:
        for warning in warnings:
            print_term(warning, COLORS['WARNING'])

    run_conf.run()
    
if __name__ == '__main__':
    main()
