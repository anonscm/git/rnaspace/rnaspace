import os
import tempfile

from rnaspace.core.data_manager import data_manager
from rnaspace.core.conversion.cgview_converter import cgview_converter
from rnaspace.core.exploration.cgview import cgview

class cgview_model(object):

    def __init__(self):
        self.data_manager = data_manager()

    def get_ids_from_authkey(self, id):
        return self.data_manager.get_ids_from_authkey(id)

    def get_action(self, params):
        if params.has_key("action"):
            return params["action"]
        else:
            return ""

    def user_has_data(self, user_id, project_id):
        """ Return True if the user has data, false else 
            user_id(type:string)   the user id
        """
        return self.data_manager.user_has_project(user_id, project_id)

    def get_project_expiration_days(self):
        return self.data_manager.get_project_expiration_days()

    def create_cgview_image(self, user_id, project_id, params):
        rnas_id_to_export = []
        for rna in range(int(params["nb_putative_rna"])):
            rnas_id_to_export.append(params["putative_rna"+str(rna)])
        
        rnas_to_export = []
        for rna_id in rnas_id_to_export:
            rna = self.data_manager.get_putative_rna(user_id, rna_id, project_id)
            rnas_to_export.append(rna)
        try:
            converter = cgview_converter()
            tmp_dir = self.data_manager.config.get("storage","tmp_dir")
            tmp_filename = os.path.basename(tempfile.NamedTemporaryFile().name)
            output =  os.path.join(tmp_dir, tmp_filename + '.tab')
            seq_length = len(self.data_manager.get_sequence(user_id, rnas_to_export[0].genomic_sequence_id, project_id))
            converter.write(rnas_to_export, seq_length, output)            
        except:
            raise IOError, 'Error when attempting to create CGView file!'

        cgview_generator = cgview()
        image_path = cgview_generator.run(user_id, project_id, output)        
        return os.path.basename(image_path)

    def create_cgview_svg(self, user_id, project_id, params):
        rnas_id_to_export = []
        for rna in range(int(params["nb_putative_rna"])):
            rnas_id_to_export.append(params["putative_rna"+str(rna)])
        
        rnas_to_export = []
        for rna_id in rnas_id_to_export:
            rna = self.data_manager.get_putative_rna(user_id, rna_id, project_id)
            rnas_to_export.append(rna)
        try:
            converter = cgview_converter()
            tmp_dir = self.data_manager.config.get("storage","tmp_dir")
            tmp_filename = os.path.basename(tempfile.NamedTemporaryFile().name)
            output =  os.path.join(tmp_dir, tmp_filename + '.tab')
            seq_length = len(self.data_manager.get_sequence(user_id, rnas_to_export[0].genomic_sequence_id, project_id))
            converter.write(rnas_to_export, seq_length, output)            
        except:
            raise IOError, 'Error when attempting to create CGView file!'

        cgview_generator = cgview()
        image_path = cgview_generator.run(user_id, project_id, output, "svg")

        tmp_filename = os.path.basename(tempfile.NamedTemporaryFile().name)
        newoutput =  os.path.join(tmp_dir, tmp_filename + '.svg')
        foutput = open(newoutput, "w")

        svgold = open(self.tab_file_content(image_path), 'r')
        svg = ""
        script = False
        for line in svgold:
            if line.find('<script') > 0:
                script = True
            elif line.find('</script>') > 0:
                script = False
            else:
                if not script:
                    foutput.write(line)
        foutput.close()
        svgold.close()
        return os.path.basename(newoutput)

    
    def create_cgview_file(self, user_id, project_id, params):
        rnas_id_to_export = []
        for rna in range(int(params["nb_putative_rna"])):
            rnas_id_to_export.append(params["putative_rna"+str(rna)])
        
        rnas_to_export = []
        for rna_id in rnas_id_to_export:
            rna = self.data_manager.get_putative_rna(user_id, rna_id, project_id)
            rnas_to_export.append(rna)
        try:
            converter = cgview_converter()
            tmp_dir = self.data_manager.config.get("storage","tmp_dir")
            tmp_filename = os.path.basename(tempfile.NamedTemporaryFile().name)
            output =  os.path.join(tmp_dir, tmp_filename + '.tab')
            web_path = tmp_filename + '.tab'
            seq_length = len(self.data_manager.get_sequence(user_id, rnas_to_export[0].genomic_sequence_id, project_id))
            converter.write(rnas_to_export, seq_length, output)
            return web_path
        except:
            raise IOError, 'Error when attempting to create CGView file!'

    def tab_file_content(self, name):
        tmp_dir = self.data_manager.config.get("storage", "tmp_dir")
        path = os.path.join(tmp_dir, name)
        return path

