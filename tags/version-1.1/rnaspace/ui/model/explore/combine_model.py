#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import threading
import pickle
import os

from rnaspace.core.prediction.threads_manager import threads_manager
from rnaspace.core.data_manager import data_manager
from rnaspace.core.trace.event import disk_error_event
from rnaspace.core.trace.event import explore_combine_event
from rnaspace.core.exceptions import disk_error


class combine_model(object):
    
    threads = None
    
    def __init__(self):        
        self.data_manager = data_manager()
        if combine_model.threads is None:
            combine_model.threads = threads_manager()

    def user_has_data(self, user_id):
        """ Return True if the user has data, false else 
            user_id(type:string)   the user id
        """
        return self.data_manager.user_has_data(user_id)
    
    def get_putative_rnas(self, user_id, project_id, params):
        """ Return a dictionary of putative_rna indexed by their sys_id
            user_id(type:string)      the user id
            project_id(type:string)   project id the user is working on
            params(type:{})         the dictionary of all the parameters
        """
        result_table = []
        if params.has_key("nb_putative_rnas"):
            rnas = self.data_manager.get_putative_rnas(user_id, project_id)
            for i in range(int(params["nb_putative_rnas"])):
                param_name = "putative_rna" + str(i)
                for rna in rnas:    
                    if rna.sys_id == params[param_name]:
                        result_table.append(rna)
        return result_table
                
    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_manager.get_ids_from_authkey(id)

    def get_authkey(self, user_id, project_id):
        """
        user_id(sting)      the user_id
        project_id(string)  the project_id
        return the id
        """
        return self.data_manager.get_authkey(user_id, project_id)

    def get_action(self, params):
        """ Return the action
            params(type:{})     the dictionary of parameters
        """
        if params.has_key("action"):
            return params["action"]
        else:
            return None 

    def get_mount_point(self):
        return self.data_manager.get_mount_point()

    def combine(self, user_id, project_id, run_id, prnas, full_url,
                not_merged_path):
        
        email = self.data_manager.get_user_email(user_id, project_id)
        try:
            res = self.data_manager.combine_putative_rnas(user_id, project_id,
                                                          prnas, "explore")
            new_prnas = res["merged"]
            t = res["time"]
            not_merged_warning = res["not-merged-warning"]
            
            fnotmerged = open(not_merged_path, "w")
            pickle.dump(not_merged_warning, fnotmerged)
            fnotmerged.close()
            
            not_merged_warning_list = []
            for grp in not_merged_warning:
                not_merged_warning_list.extend(grp)

            not_merged = res["not-merged"]
            not_merged.extend(not_merged_warning_list)
            
            # only save the ids in the trace
            prnas_ids = [rna.sys_id for rna in prnas]
            new_prnas_ids = [rna.sys_id for rna in new_prnas]
            project_size = self.data_manager.get_project_size(user_id,
                                                              project_id)       
            ev = explore_combine_event(user_id, project_id, email,
                                       prnas_ids, new_prnas_ids, not_merged,
                                       "basic_combine", t,  project_size)
            self.data_manager.update_project_trace(user_id, project_id, [ev])
        except disk_error, e:
            ev = disk_error_event(user_id, project_id, email, e.__str__(),
                                  "explore_combine", run_id)
            self.data_manager.update_project_trace(user_id, project_id, [ev])
            self.email.send_user_failed_email(user_id, project_id, run_id,
                                              [("combine", e.__str__())],
                                              email, full_url)
        

    def combine_putative_rnas(self, user_id, project_id, params):
        run_id = "explore_combine"
        explore_url = self.get_mount_point() + "explore?authkey=" +\
                      self.get_authkey(user_id, project_id)
        
        prnas = self.get_putative_rnas(user_id, project_id, params)
        
        tmp_dir = self.data_manager.config.get("storage","tmp_dir")
        not_merged_path = os.path.join(tmp_dir, user_id + project_id + run_id)
        
        t = threading.Thread(target=self.combine,
                             args=(user_id, project_id, run_id, prnas,
                                   explore_url, not_merged_path))
        t.start()
        combine_model.threads.add_thread(user_id, project_id, run_id, t)

        return os.path.basename(not_merged_path)

    def get_not_combined_prnas(self, user_id, project_id, params):
        run_id = "explore_combine"
        rnas_id = []
        
        tmp_dir = self.data_manager.config.get("storage","tmp_dir")
        not_merged_path = os.path.join(tmp_dir, user_id + project_id + run_id)
        fnotmerged = open(not_merged_path, "r")
        groups = pickle.load(fnotmerged)
        fnotmerged.close()

        if not len(groups) > 0:
            return []

        for rnas in groups:
            rnas_id_group = []
            for rna in rnas:
                rnas_id_group.append(rna.user_id)
            rnas_id.append(rnas_id_group)
            
        return rnas_id
    
    def combine_finished(self, user_id, project_id):
        run_id = "explore_combine"
        return combine_model.threads.run_threads_finished(user_id, project_id,
                                                          run_id)

        
