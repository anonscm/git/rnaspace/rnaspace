#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import cherrypy
import json

from rnaspace.ui.utils.common import common
from rnaspace.ui.utils.common import AUTH_ERROR
from rnaspace.ui.controller.error_controller import error_controller
from rnaspace.ui.model.explore.combine_model import combine_model
from rnaspace.core.exceptions import disk_error

DISK_ERROR =\
"""
<br />
Sorry, no more space available. <br />
You can not modify predictions.
<br /><br />
Return to <a href="%s">Explore page</a>.
"""


class combine_controller(object):

    def __init__(self):
        self.model = combine_model()

    @cherrypy.expose
    def index(self, **params):

        view = common.get_template('explore/combine_wait.tmpl')
        
        if not params.has_key("authkey"):
            return "You're not authorized to access this page."

        (user_id,
         project_id) = self.model.get_ids_from_authkey(params["authkey"])
        
        if user_id is None or project_id is None:
            return self.error(AUTH_ERROR)

        if not self.model.user_has_data(user_id):
            return "You're data have been deleted. For server maintenance, " +\
                   "data are cleared after a while."


        if self.model.get_action(params) == "combine":
            not_merged_path = self.model.combine_putative_rnas(user_id,
                                                               project_id,
                                                               params)
            view.mount_point = self.model.get_mount_point()
            view.type = "wait"
            view.groups = []
            return json.dumps({"type":"wait", "view":str(view)})
            
        elif self.model.get_action(params) == "wait":
            finished = self.model.combine_finished(user_id, project_id)
            if finished:
                not_merged = self.model.get_not_combined_prnas(user_id,
                                                               project_id,
                                                               params)
                if len(not_merged) > 0:
                    view.mount_point = self.model.get_mount_point()
                    view.type = "results"
                    view.groups = not_merged
                    return json.dumps({"type":"warning", "view":str(view)})
                else:
                    return json.dumps({"type":"done", "view":""})
            else:
                view.mount_point = self.model.get_mount_point()
                view.type = "wait"
                view.groups = []
                return json.dumps({"type":"wait", "view":str(view)})

    @cherrypy.expose
    def error(self, msg, authkey=None):
        error_page = error_controller()
        return error_page.get_page(msg, authkey)
