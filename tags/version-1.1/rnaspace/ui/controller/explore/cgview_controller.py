#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import cherrypy

from rnaspace.ui.utils.common import common
from rnaspace.ui.utils.common import AUTH_ERROR
from rnaspace.ui.controller.error_controller import error_controller
from rnaspace.ui.model.explore.cgview_model import cgview_model

class cgview_controller(object):

    def __init__(self):
        self.model = cgview_model()

    @cherrypy.expose
    def index(self, **params):

        if not params.has_key("authkey"):
            return self.__error("You're not authorized to access this page")

        (user_id, project_id) = (None, None)        
        (user_id,project_id) = self.model.get_ids_from_authkey(params["authkey"])
        if user_id is None or project_id is None:
            return self.__error(AUTH_ERROR)
            
        if not self.model.user_has_data(user_id, project_id):
            return self.__error("No data found for authkey = " +
                                params["authkey"] +
                                ", your datas may have been deleted (datas " +
                                "are deleted every " +
                                self.model.get_project_expiration_days() +
                                " days)")
        try:
            action = self.model.get_action(params)
            if action == "image":
                return self.model.create_cgview_image(user_id, project_id,
                                                      params)
            elif action == "svg":
                return self.model.create_cgview_svg(user_id, project_id, params)
            else:
                return self.model.create_cgview_file(user_id, project_id, params)
        except IOError, error:
            return error

    def __error(self, msg):   
        """ Build an error page defined by a message error
            msg(type:string)   the message to display 
        """  
        error_page = error_controller()
        return error_page.get_page(msg)
