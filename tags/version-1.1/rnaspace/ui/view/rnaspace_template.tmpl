##
## RNAspace: non-coding RNA annotation platform
## Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>RNAspace</title>
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-15" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Language" content="fr" />
    <link href="${mount_point}ressource/img/favicon.ico" rel="SHORTCUT ICON" />
    <link href="${mount_point}ressource/css/rnaspace.css" rel="stylesheet"
	  type="text/css" />
    <script type="text/javascript" 
	    src="${mount_point}ressource/js/svg.js" 
	    data-path="${mount_point}ressource/js" >
    </script>

    <script type="text/javascript" 
	    src="${mount_point}ressource/js/jquery-1.3.1.min.js" >
    </script>
    
	$import_ressources
    
    <script type="text/javascript">
     <!--

     #raw
      $(document).ready(function(){
     #end raw
      var mount_point = "${mount_point}";      
     #raw
	var help_link = {"Home": "#help1", "1.Manage": "#help3", "1.Load data": "#help3", "2.Predict": "#help4", "3.Explore": "#help5"}
	$("#help").attr("href", mount_point + "help" + help_link[$(".active").html()])      
      
	document.title = "RNAspace - " + $(".active").html();
     
	if (!$("#home_btn").hasClass("active")) {
	  var cookieStr = "ck_check=checkingnow";
	  document.cookie = cookieStr;
	  if (document.cookie.indexOf(cookieStr) <= -1){
	    window.location.replace(mount_point + "cookies_error");
	  }
	}
     
	$("#manage_btn").attr("href", mount_point + "manage");
	$("#predict_btn").attr("href", mount_point + "predict");
	$("#explore_btn").attr("href", mount_point + "explore");
	  
	// when we click on a toogletext element, display added sequences
        $("a.info_section").click( function () {
          $(this).next(".addedseq").slideToggle("normal");
          return false;
        }); 
	  
     #end raw
     $jquery
     #raw
       });
     #end raw
     $javascript
     -->
    </script>
    
    #def home_id: inactive
    #def manage_id: inactive
    #def predict_id: inactive
    #def explore_id: inactive
    #def header_title: none
    #def info_boxes: inactive

  </head>
  <body>
    <div id="container">       
    
      <div id="logo"><a href="/"></a></div>    
    
      <div id="header">
    	<p> $header_title </p>
      </div>   
		  		  
      <div id="menu">
	<ul>
	  <li>
	    <a href="${mount_point}" id="home_btn" 
	       class="$home_id">Home</a>
	  </li>
	  #if $authentification_platform
	  <li>
	    <a href="${mount_point}" id="manage_btn" 
	       class="$manage_id">1.Manage</a>
	  </li>
	  #else
	  <li>
	    <a href="${mount_point}" id="manage_btn" 
	       class="$manage_id">1.Load data</a>
	  </li>
          #end if
	  <li>
	    <a href="${mount_point}" id="predict_btn" 
	       class="$predict_id">2.Predict</a>
	  </li>
	  <li>
	    <a href="${mount_point}" id="explore_btn" 
	       class="$explore_id">3.Explore</a>
	  </li>
	</ul>
      </div>

      <div id="content">
	<div id="help_o">
	  <a id="help" href="" target="blank">
	    <img src="${mount_point}ressource/img/icon/help_o.png"
		 class="border0" alt="help" />
	  </a>
	</div>

	<noscript>
	  <div id="crb_header">
	    <div id="crb_title"> J a v a s c r i p t &nbsp; W a r n i n g </div>
	    <div id="crb_content">
	      If you see this message, your web browser doesn't support 
	      JavaScript or JavaScript is disabled. Please enable JavaScript
	      in your browser settings so RNAspace can be functional. 
	    </div>
	  </div>
	</noscript>

	#if $info_boxes != "inactive"
	<div class="infobar">
	  $infobar
	</div>
	
	<div class="page">
	#end if

	$page_content

	#if $info_boxes != "inactive"
	</div>
	#end if

      </div>

      <div id="big_footer">
	<div id="footer">
	  <p>Comments and remarks: 
	    <a href="mailto:contact@rnaspace.org">contact@rnaspace.org</a>.
	  </p>
	</div>
      </div>

    </div>
  </body>
</html>
