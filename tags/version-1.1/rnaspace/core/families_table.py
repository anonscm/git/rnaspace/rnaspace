families = {

    "16s_rRNA": [
        # RNAmmer
        "16s_rRNA",
        # ERPIN
        "16S_rRNA_helix_18", "16S_rRNA_helix_18"
    ],

    "23s_rRNA": [
        # RNAmmer
        "23s_rRNA",
        # ERPIN
        "23S/28S_rRNA_sarcin_loop", "23S/28S_rRNA_sarcin_loop"
    ],

    "5s_rRNA": [
        # RNAmmer
        "5s_rRNA",
        # ERPIN
        "Archae_5S_rRNA", "Bacterial_5S_rRNA", "Eukaryotic_5S_rRNA",
        "5S_ribosomal_RNA","Archae_5S_rRNA", "Bacterial_5S_rRNA",
        "Eukaryotic_5S_rRNA", "5S_ribosomal",
        # BLAST / YASS
        "5S_rRNA"
    ],

    "5_8S_rRNA": [
        # ERPIN
        "5.8S_ribosomal",
        # BLAST / YASS
        "5_8S_rRNA"
    ],

    "Alpha_RBS": [
        # BLAST / YASS
        "Alpha_RBS",
        # ERPIN
        "Alpha_operon_ribosome_binding_site"
    ],

    "DnaX": [
        # BLAST / YASS
        "DnaX",
        # ERPIN
        "DnaX_ribosomal_frameshifting_element"
    ],

    "FMN": [
        # darn / BLAST / YASS
        "FMN",
        # ERPIN
        "FMN_riboswitch_(RFN_element)"
    ],

    "istR": [
        # BLAST / YASS
        "istR",
        # ERPIN
        "istR_Hfq_binding"
    ],

    "Leu_leader": [
        # BLAST / YASS
        "Leu_leader",
        # ERPIN
        "Leucine_operon_leader"
    ],

    "Lysine": [
        # darn / BLAST / YASS
        "Lysine",
        # ERPIN
        "Lysine_riboswitch"
    ],

    "Mg_sensor": [
        # BLAST / YASS
        "Mg_sensor",
        # ERPIN
        "Magnesium_Sensor"
    ],
    
    "RNaseP": [
        # darn
        "RNaseP",
        # ERPIN
        "Bacterial_Rnase_P_class_A", "Bacterial_Rnase_P_class_B",
        "Bacterial_Rnase_P_class_A", "Bacterial_Rnase_P_class_B",
        # BLAST / YASS
        "RnaseP_nuc", "RnaseP_bact_a", "RnaseP_bact_b",  "RnaseP_arch",
        "RNaseP_nuc", "RNaseP_bact_a", "RNaseP_bact_b",  "RNaseP_arch"        
    ],

    "me5": [
        "me5", "RNase_E_5'_UTR_element"
    ],

    "mini-ykkC": [
        # BLAST / YASS
        "mini-ykkC",
        # ERPIN
        "mini-ykkC_RNA_motif"
    ],
    
    "S15": [
        # BLAST / YASS
        "S15",
        # ERPIN
        "Ribosomal_S15_leader"
    ],
    
    "snoRNA-HACA": [
        # ERPIN
        "Archae H/ACA sno RNA by Fabrice Leclerc",
        # BLAST / YASS
        "HACA_sno_Snake"
    ],
    
    "snoRNA-CD": [
        # ERPIN
        "Pyrococcus_C/D_box_small_nucleolar", 
        "Small_nucleolar_RNA_sR1", "Small_nucleolar_RNA_sR2","Small_nucleolar_RNA_sR4",
        "Small_nucleolar_RNA_sR5", "Small_nucleolar_RNA_sR7","Small_nucleolar_RNA_sR8",
        "Small_nucleolar_RNA_sR9", "Small_nucleolar_RNA_sR10","Small_nucleolar_RNA_sR11",
        "Small_nucleolar_RNA_sR12", "Small_nucleolar_RNA_sR13","Small_nucleolar_RNA_sR14",
        "Small_nucleolar_RNA_sR15", "Small_nucleolar_RNA_sR16","Small_nucleolar_RNA_sR17",
        "Small_nucleolar_RNA_sR18", "Small_nucleolar_RNA_sR19","Small_nucleolar_RNA_sR20",
        "Small_nucleolar_RNA_sR21", "Small_nucleolar_RNA_sR22","Small_nucleolar_RNA_sR23",
        "Small_nucleolar_RNA_sR24", "Small_nucleolar_RNA_sR28",
        "Small_nucleolar_RNA_sR30","Small_nucleolar_RNA_sR32",
        "Small_nucleolar_RNA_sR33", "Small_nucleolar_RNA_sR34","Small_nucleolar_RNA_sR36",
        "Small_nucleolar_RNA_sR38", "Small_nucleolar_RNA_sR39","Small_nucleolar_RNA_sR41",
        "Small_nucleolar_RNA_sR42", "Small_nucleolar_RNA_sR44","Small_nucleolar_RNA_sR45",
        "Small_nucleolar_RNA_sR46", "Small_nucleolar_RNA_sR47","Small_nucleolar_RNA_sR48",
        "Small_nucleolar_RNA_sR49", "Small_nucleolar_RNA_sR51","Small_nucleolar_RNA_sR52",
        "Small_nucleolar_RNA_sR53", "Small_nucleolar_RNA_sR55","Small_nucleolar_RNA_sR58",
        "Small_nucleolar_RNA_sR60",
        # Darn
        "snoRNA-CDbox"
    ],

    "SraA": [
        # BLAST / YASS
        "SraA",
        # ERPIN
        "sraA_Hfq_binding"
    ],

    "SraB": [
        # BLAST / YASS
        "SraB",
        # ERPIN
        "sraB_Hfq_binding"
    ],

    "SraD": [
        # BLAST / YASS
        "SraD",
        # ERPIN
        "sraD_Hfq_binding"
    ],

    "SraG": [
        # BLAST / YASS
        "SraG",
        # ERPIN
        "sraG_Hfq_binding"
    ],

    "SraH": [
        # BLAST / YASS
        "SraH",
        # ERPIN
        "sraH_Hfq_binding"
    ],

    "SraL": [
        # BLAST / YASS
        "SraL",
        # ERPIN
        "sraL_Hfq_binding"
    ],

    "SRP": [
        # BLAST / YASS
        "SRP_bact",
        # ERPIN
        "SRP_RNA_Domain_IV"
    ],
    
    "tRNA": [
        # tRNAscan
        "tRNA-Phe", "tRNA-Val", "tRNA-Leu", "tRNA-Ile", "tRNA-Cys",
        "tRNA-Trp", "tRNA-Arg", "tRNA-Ser", "tRNA-Ser", "tRNA-Ala",
        "tRNA-Pro", "tRNA-Thr", "tRNA-Tyr", "tRNA-Asp", "tRNA-His",
        "tRNA-Asn", "tRNA-Met", "tRNA-Gly", "tRNA-Sup", "tRNA-Glu",
        "tRNA-Gln", "tRNA-Lys", "tRNA-Ind", "tRNA-Pseudo", "tRNA-SeC(p)",
        # ERPIN
        "Type_I_tRNA", "Type_II_tRNA",
        # darn
        "tRNA_Bacteria", "tRNA_Eukaryote", "tRNA_Archaea",
        # BLAST / YASS
        "tRNA"
    ],

    "Thr_leader": [
        # BLAST / YASS
        "Thr_leader",
        # ERPIN
        "Threonine_operon_leader"    
    ],

    "Trp_leader": [
        # BLAST / YASS
        "Trp_leader",
        # ERPIN
        "Tryptophan_operon_leader"    
    ],

    "yybP-ykoY": [
        # BLAST / YASS
        "yybP-ykoY",
        # ERPIN
        "yybP-ykoY_element"    
    ]

}
