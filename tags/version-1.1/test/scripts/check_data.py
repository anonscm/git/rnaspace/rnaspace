#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

# Ask to load organism test data files too big to be on SVN
msg = ''
if not os.path.exists('../data/NC_000913.fna'):
     msg += 'First It is necessary to get NC_000913.fna '
if not os.path.exists('../data/NC_003076.fna'):
     msg += 'and NC_003076.fna '
if msg != '':
     print msg + 'from the rnaspace wiki because file(s) is(are) too big to be on SVN.'

# create factice test sequence file with link
if os.path.exists('../data/NC_000913.fna') and not os.path.exists('../data/NC_000913bis.fna'):
     os.system( 'ln -s ../data/NC_000913.fna ../data/NC_000913bis.fna')
     os.system( 'ln -s ../data/NC_000913.txt ../data/NC_000913bis.txt')

if os.path.exists('../data/NC_000913.fna') and os.path.exists('../data/NC_003076.fna') and os.path.exists('../data/NC_000913bis.fna'):
     print 'Data test is OK.' 
