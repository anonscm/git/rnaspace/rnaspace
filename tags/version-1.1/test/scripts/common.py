#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os

# Definition of environnment variables (only variables in upper case are used
# in other modules and add of python paths


# RNAspace sources description
RNASPACE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", ".."))
# Add required paths to run tests
sys.path.append(RNASPACE_DIR)

src_dir_name = 'rnaspace'
wrappers_subpath = 'core/prediction/wrappers'
cfg_dir_name = 'cfg'
rnaspace_cfg = os.path.join(RNASPACE_DIR, cfg_dir_name, 'rnaspace.cfg')
PREDICTORS_CFG_DIR = os.path.join(RNASPACE_DIR, cfg_dir_name,'predictors' )
PREDICTOR_CFG_SUFFIX = '.conf'

# RNAspace workspace description
import rnaspace.dao.storage_configuration_reader as scrm
scrm.update_conf(rnaspace_cfg, PREDICTORS_CFG_DIR)
scr = scrm.storage_configuration_reader()
WORKSPACE_DIR = scr.get("storage", "workspace_dir")

SEQ_DIR_NAME = 'sequence'

# RNAspace test sources description
TEST_DIR_NAME = 'test'
DATA_DIR_NAME = 'data'
OUTPUT_DIR_NAME = 'outputs'
OUTPUT_FILE_SUFFIX = '.out'
REFERENCE_DIR_NAME = 'references'
REFERENCE_FILE_SUFFIX = '.ref'

# RNAspace test space description in workspace
TEST_USER_DIR_NAME = 'test_user'
TEST_PROJECT_NAME = 'test_project'
TEST_RUN_NAME = 'run_000001'
SEQ_FASTA_SUFFIX = '.fna'
SEQ_TXT_SUFFIX = '.txt'


sys.path.append(os.path.join(RNASPACE_DIR, src_dir_name, wrappers_subpath))


