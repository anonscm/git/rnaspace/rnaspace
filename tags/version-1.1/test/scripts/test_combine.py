#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import unittest

from rnaspace_testcase import rnaspace_testcase
from rnaspace.ui.model.explore.explore_model import explore_model
from rnaspace.core.sequence_combiner import sequence_combiner
from common import TEST_USER_DIR_NAME, TEST_PROJECT_NAME

# test the RNAs combine with E. coli, a subset of gene finders
class test_combine (rnaspace_testcase):

    def test(self):

        self.testcase_name = 'combine'
        self.sequences_id = ['NC_000913']
        predictor_list = [ [ 'darn', {'d':'tRNA_Bacteria [B]'}, {} ], 
                           [ 'erpin', {'training_set': 'Type I tRNA [B/E/A]'}, {} ],
                           [ 'trnascanse', {'C':'False','D':'False','X':20,'L':116}, {} ],
                           [ 'rnammer', {'tsu':'True','ssu':'False','lsu':'False'}, {} ],
                           [ 'erpin', {'training_set': 'Bacterial 5S rRNA [B]'}, {} ],
                           [ 'comparative_analysis',
                             {'conservation_soft': 'BLAST', 'inference_soft': 'caRNAc',
                              'conservation_soft_opts': None, 'inference_soft_opts': None,
                              'aggregation_soft': 'CG-seq', 'aggregation_soft_opts': None},
                             {'species': ['Yersinia_pseudotuberculosis_YPIII [1 sequences]']}],
                           [ 'atypicalgc', {'F':'True','T':'all','W':101,'L':200}, {} ] ]
        self.init_seq(self.sequences_id)
        self.rnas = []    
 
        # launch gene finders
        result = []    
        for (predictor_name, opts, p) in predictor_list:
            result = self.launch_predictor(predictor_name, opts, p)
        
        # combine predictions
        combiner = sequence_combiner()
        res = combiner.run( TEST_USER_DIR_NAME, TEST_PROJECT_NAME, result, "basic_combine")
        self.rnas = res["merged"]
        self.rnas.extend(res["not-merged"])
        not_merged_warning = res["not-merged-warning"]
        not_merged_warning_list = []
        for grp in not_merged_warning:
            not_merged_warning_list.extend(grp)
        self.rnas.extend(not_merged_warning_list)
        
        # export and compare result
        self.convert_compare_result('GFF')


def suite_combine():
    test_combine_suite = unittest.TestSuite()
    test_combine_suite.addTest(test_combine('test'))
    return test_combine_suite


if __name__ == '__main__':
    unittest.main()


