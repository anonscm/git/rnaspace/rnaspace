#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import unittest

from rnaspace_testcase import rnaspace_testcase
from rnaspace.core.exploration.filter import selection_criteria, filter

# test with A. thaliana ch V (27Mb), a small subset of predictors,
# a filter on predictions that are compared in GFF format with reference predictions
class test_athaliana (rnaspace_testcase):

    def test(self):

        self.testcase_name = 'athaliana'
        self.sequences_id = ['NC_003076']
        predictor_list = [ [ 'blast', {'alignments':'0','S': 'both', 'score': '2; -3; 5; 2', 'e': '0.001', 'W': '11', 'F': 'no'}, {'db': 'Rfam_9.1.fasta'}],
                           [ 'trnascanse', {'C':'False','D':'False','X':20,'L':116}, {} ] ]

        testfilter = filter()
        testfilter.add_criteria(selection_criteria("start_position",">=","1000000"))
        testfilter.add_criteria(selection_criteria("stop_position","<=","10000000"))

        
        self.init_seq(self.sequences_id)
        self.rnas = []        
        for (predictor_name, opts, p) in predictor_list:
            result = self.launch_predictor( predictor_name, opts, p)
            self.rnas += testfilter.run(result)         
        self.convert_compare_result( 'GFF')    
           

def suite_athaliana():
    test_athaliana_suite = unittest.TestSuite()
    test_athaliana_suite.addTest(test_athaliana('test'))
    return test_athaliana_suite
        
if __name__ == '__main__':
    unittest.main()

     
