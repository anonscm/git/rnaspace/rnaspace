[General]
name = "BLAST"
path = "blast.py"
exe_path = "blastall"
speed = ""
version = "2.2.17"
database = "1"
type = "known_homology"
description: "<b>BLAST</b>  finds regions of local similarity between sequences
	     using k-mers."

[Options]
simple = ""
long = "alignments,F,S,W,score,e"
basic_options = ""

[Options_Params]
alignments = "<int>"
F = "yes,no"
S = "both,forward,reverse"
W = "[7-30]"
e = "<float>"
score = "2; -7; 2; 4,2; -7; 0; 4,2; -7; 4; 2,2; -7; 2; 2,2; -7; 4; 4,4; -5; 6; 5,4; -5; 5; 5,4; -5; 4; 5,4; -5; 3; 5,4; -5; 12; 8,2; -5; 2; 4,2; -5; 0; 4,2; -5; 4; 2,2; -5; 2; 2,2; -5; 4; 4,1; -2; 1; 2,1; -2; 0; 2,1; -2; 3; 1,1; -2; 2; 1,1; -2; 1; 1,1; -2; 2; 2,1; -3; 1; 2,1; -3; 0; 2,1; -3; 2; 1,1; -3; 1; 1,1; -3; 2; 2,2; -3; 4; 4,2; -3; 2; 4,2; -3; 0; 4,2; -3; 3; 3,2; -3; 6; 2,2; -3; 5; 2,2; -3; 4; 2,2; -3; 2; 2,2; -3; 6; 4,1; -1; 3; 2,1; -1; 2; 2,1; -1; 1; 2,1; -1; 0; 2,1; -1; 4; 1,1; -1; 3; 1,1; -1; 2; 1,1; -1; 4; 2,5; -4; 10; 6,5; -4; 8; 6,5; -4; 25; 10"


[Options_default_params]
default_simple = ""
alignments = "5"
F = "yes"
S = "both"
W = "11"
e = "0.001"
score = "2; -3; 5; 2"

[Options_view]
alignments = "text area"
F = "radio"
S = "radio"
W = "text area"
e = "text area"
score = "listbox"


[Options_desc]
alignments = "Number of alignments"
F = "Filter low complexity regions in the query sequence"
S = "Query strands "
W = "Word size (between 7 and 30)"
e = "E-value threshold"
score = "Match/mismatch/gap opening/gap extension scores"


[Help]
text: "<div class="help_soft_title">Approach</div>
      
      BLAST searches for high scoring sequence alignments between the
      query sequence and sequences in the database using a heuristic
      approach that approximates the Smith-Waterman algorithm for
      local alignment.  The main idea of BLAST is that there are often
      high-scoring segment pairs (HSP) contained in a statistically
      significant alignment. BLAST first search for these HSPs, then extend
      them to construct valid alignments.
      <br />
      <br />
      RNAspace currently uses BlastN 2.2.18     
      

      <div class="help_soft_title">Parameters</div>

      <b>Database</b>: The ncRNA database to which the query sequence will 
      be compared.
      <br />
      <br />
      <b>Number of alignments</b>: When comparing the request sequence to the 
      reference database, any putative RNA can match many homologous ncRNAs 
      occurring in the same family in the database.  
      Such overlapping alignments are merged into a single prediction. 
      This parameter specifies the maximal number of pairwise 
      alignments that will be saved for each putative ncRNA. Selected
      alignments are sorted according to their E-value.
      Default value is 5.		
      <br />
      <br />
		

      <b>Filter low complexity regions in the query sequence</b>:
       This function masks off segments of the query sequence that have low
       compositional complexity, as determined by the DUST program of
       Tatusov and Lipman. Filtering can eliminate statistically significant
       but biologically uninteresting reports from the BLAST output. 
       Filtering is only applied to the query sequence, not to database 
       sequences.  Default value is yes.
       <br />
       <br /> 
       When this option is selected,  the complementary option 
       "Mask for lookup table" is automatically set to true. It means that 
       no HSPs  are found based upon low-complexity sequence, but the BLAST 
       extensions are performed without masking and so they can be extended 
       through low-complexity sequence.
      <br />
      <br />
      <b>Query strand </b>: The comparison of the query sequence against 
      the database can be performed on the forward strand only, the 
      reverse strand only,  or on both strands. Default value is both.<br />
      <br />
      <br />
      <b>Word size (between 7 and 30)</b>: This is the size of the
      HSPs (or k-mers) used in BLAST heuristics to identify potential
      high scoring alignments. One normally regulates the sensitivity
      and speed of the search by increasing or decreasing the
      word size. The lower is this value, the more sensitive is the
      algorithm. The higher is this value, the faster is the
      algorithm.  
      <br /> 
      <br /> 
      
      <b>Match/mismatch/gap opening/gap extension scores</b>: This is
      simply the scoring system used to assess the quality of a
       pairwise alignment. In BLAST, there are internal dependencies
       between these score parameters. So they are available in a pull
       down menu that shows the allowed reward/penalty pairs and their
       associated gap existence and gap extension penalties.  Default
       values are 2 for a match, -3 for a mismatch, 5 for a gap
       opening, and 2 for a gap extension.
      <br />
      <br />

      <b>E-value (expectation value) threshold</b>: This setting specifies
      the statistical significance threshold for reporting alignments
      against database sequences. For example, when this parameter is
      set to 5, it means that 5 such matches are expected to be found
      merely by chance, according to the stochastic model of Karlin
      and Altschul (1990).  Lower E-value thresholds are more
      stringent, leading to fewer chance matches being reported.
      <br />
      When several alignments are involved in a prediction, RNAspace 
      reports the best E-value, (the lowest E-value).


      <div class="help_soft_title">References</div>

      Altschul, S.F., Gish, W., Miller, W., Myers, E.W. &amp; Lipman,
      D.J. (1990) "Basic local alignment search tool."
      J. Mol. Biol. 215:403-410. <br />
 
      NCBI BLAST: a better web interface Mark Johnson, Irena
      Zaretskaya, Yan Raytselis, Yuri Merezhuk, Scott McGinnis, and
      Thomas L. Madden Nucleic Acids Res. 2008 July 1; 36(Web Server
      issue): W5-W9.

      <br />
      <br />
      <a href="http://blast.ncbi.nlm.nih.gov/Blast.cgi">BLAST NCBI web site</a>
      "
      