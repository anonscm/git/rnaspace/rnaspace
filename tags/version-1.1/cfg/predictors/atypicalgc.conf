[General]
name = "atypicalGC"
exe_path = "/media/Stock/rnaspace_softwares/atypicalGC/atypicalgc.py"
path = "atypicalgc.py"
speed = ""
version = "1.0"
type = "abinitio"
description:  "<b>atypicalGC</b> identifies atypical G and C content regions
    	       in sequence with a GC% out of [40 ; 60].
	      "

[Options]
simple = "F"
long = "T,W,L"
basic_options = ""

[Options_Params]
T = "all,up,down"
W = "[3-501]"
L = "[1-500]"

[Options_default_params]
default_simple = ""
T = "all"
W = "101"
L = "50"

[Options_view]
T = "listbox"
W = "text area"
L = "text area"

[Options_desc]
F = "Force prediction:"
T = "Look for up/down/all atypical GC%:"
W = "Window size:"
L = "Min length:"


[Help]
text: "<div class="help_soft_title">Approach</div>
       First, evaluate if the approach is appropriate 
       (ie GC% mean on all the sequence is out of the interval [40 ; 60]
       If so, on each position of a given sequence, compute G and C percent 
       content on a sliding window centered on the considered position. 
       Only positions with a value more distant than two standard deviation 
       of the mean are considered as atypical.<br /><br />
       Second, only regions (continuous atypical positions) longer than a
       minimum length and containing less than 5% of 'N' nucleotides are kept.<br />

       <div class="help_soft_title">Parameters</div>
       <b>Force prediction</b>: force prediction even if the approach is not 
       appropriate.<br /> default value: 'False'<br /> Allowed value in 'True','False'
       <br /><br />
       <b>Look for up/down/all atypical GC%</b>: only keep regions with GC% 
       up/down/all mean value (with sliding window), default value: 'all'<br />
       Allowed value in 'all', 'up', 'down'<br />
       <br />
       <b>Window size</b>: sliding window size, default value: 101 <br />
       odd integer value in [3 ; 501]<br />
       If non odd integer, the value is reduced by 1<br />
       <br />
       <b>Min length</b>: minimum region length, default value: 50 <br />
       Integer value between [1 ; 500]<br />

       <div class="help_soft_title">Reference</div>
       INRA BIA Toulouse, 2008
      "
