[General]
name = "Darn"
path = "darn.py"
exe_path = "/media/Stock/rnaspace_softwares/darn-1.0/src/darn"
speed = ""
version = "1.0"
type = "known_rna_motif"
description: "<b>Darn</b> provides a wide-range of RNA profiles. The search
	     algorithm is based on constraint networks."

[Options]
simple = ""
long = "d"
basic_options = "d"

[Options_Params]
d = "All [domain],FMN [B/A],Lysine [B/A/E],RNaseP [B/A/E],SAM [B],snoRNA-CDbox [A],tRNA_Archaea [A],tRNA_Bacteria [B],tRNA_Eukaryote [E]"

[Options_default_params]
default_simple = ""
d = "All"

[Options_view]
d = "listbox"

[Options_desc]
d = "Descriptor:"


[Help]
text: "<div class="help_soft_title">Approach</div>
      The problem of finding new occurrences of characterized ncRNAs can be 
      modeled as the problem of finding all locally-optimal solutions of a 
      weighted constraint network using dedicated weighted global constraints, 
      encapsulating pattern-matching algorithms and data structures. 
      This is embodied in Darn, a software tool for ncRNA localization, 
      which, compared to existing pattern-matching based tools, 
      offers additional expressivity (such as enabling RNA---RNA interactions 
      to be described) and improved specificity (through the exploitation of 
      scores and local optimality) without compromises in CPU efficiency.

      <div class="help_soft_title">Parameters</div>
      <b>Descriptor</b>: Descriptor of searched RNA family. Available descriptors are provided in a list.<br />
         The first element of the list is 'All [domain]'. 
         This allows to select all descriptors suitable to the request sequence domain.<br />
         The others lines describes a RNA family name followed by the suitable domain(s) for the family. 
         The notation is 'B' for Bacteria, 'A' for Archaea, 'E' for Eukaryote, separated by '/' and 
         surrounded by squared bracket.
         For example 'RNaseP [B/A/E]' selects a descriptor of RNaseP suitable for all domains.<br />
	 Note that it is not forbidden to try a descriptor not suitable for the request sequence domain 
         but user will have to keep it in mind when analysing results of prediction.<br />


      <div class="help_soft_title">Reference</div>
      DARN! A Weighted Constraint Solver for RNA Motif Localization. <br />
      Matthias Zytnicki, Christine Gaspin, Thomas Schiex. Constraints, Volume 
      13 (2008).
      "
