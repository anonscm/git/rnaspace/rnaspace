[General]
name = "YASS"
path = "yass.py"
exe_path = "/media/Stock/rnaspace_softwares/yass-1.14/src/yass"
speed = ""
version = "1.14"
database = "1"
type = "known_homology"
description: "<b>YASS</b> finds regions of local similarity between sequences
	      using spaced-seeds." 

[Options]
simple = ""
long = "alignments,e,r,p,c,C,go,ge,E"
basic_options = ""

[Options_Params]
alignments = "<int>"
r = "both,forward,reverse"
go = "<float>"
ge = "<float>"
E = "<float>"
p = ""
c = "single,double"
C = ""
e = "yes,no"

[Options_default_params]
default_simple = ""
alignments = "5"
r = "both"
go = "-16"
ge = "-4"
E = "0.001"
p = "###-#@-##@##,###--#-#--#-###"
c = "double"
C = "5,-4,-3,-4"
e = "yes"

[Options_view]
alignments = "text area"
r = "radio"
go = "text area"
ge = "text area"
E = "text area"
p = "text area:25"
c = "radio"
C = "text area"
e = "radio"

[Options_desc]
alignments = "Number of alignments"
r = "Query strands"
go = "Gap cost opening"
ge = "Gap cost extension"
E = "E-value threshold"
p = "Seed pattern(s)<br />("#":match, "@":match or transition, "_":joker, ",":seed separator) "
c = "Seed hit criterion"
C = "Match/transversion/transition/undetermined symbol scores"
e = "Filter low complexity regions in the query sequence "

[Help]
text: "<div class="help_soft_title">Approach</div>

      Like most of the heuristic DNA local alignment software, YASS uses
      seeds to detect potential similarity regions, and then tries to
      extend them to actual alignments. It has the distinctive feature of
      using multiple transition-constrained spaced seeds to ensure a good
      sensitivity/selectivity trade-off.  That enables to search more
      fuzzy conserved sequences, as non-coding RNAs.  <br /> <br />
      Spaced seed is a non-contiguous pattern, that allows for some
      errors.  Transition mutations are purine to purine [A&lt;-&gt;G]
      or pyrimidine to pyrimidine [C&lt;-&gt;T]. 
      <br />
      <br />
      RNAspace currently uses YASS 1.14

      <div class="help_soft_title">Parameters</div>
      <b>Database</b>: The ncRNA database to which the query sequence will 
      be compared. 
      <br />
      <br />
      <b>Number of alignments</b>: When comparing the request sequence to the 
      reference database, any putative RNA can match many homologous ncRNAs 
      occurring in the same family in the database.  Such overlapping 
      alignments are merged into a single prediction. 
      This parameter specifies the maximal number of pairwise 
      alignments that will be saved for each putative ncRNA. Selected
      alignments are sorted according to their E-value.
      Default value is 5.		
      <br />
      <br />

      <b>Filter low complexity regions in the query sequence </b>: This
      function masks off segments of the query sequence that have low
      compositional complexity. Filtering can eliminate statistically
      significant but biologically uninteresting reports from the
      YASS output.  Filtering is only applied to the query sequence,
      not to database sequences.  Default value is yes.
      <br /> 
      <br />


      <b>Query strand </b>: The comparison of the query sequence against
      the database can be performed on the forward strand only, the
      reverse strand only, or on both strands. Default value is both.   
      <br /> 
      <br />

      <b>Seed Pattern(s)</b>: You can specify the seed pattern used in
      the search step.  The "\#" symbol is for matches, that is positions
      without error. The "\@" symbol is for matches or transitions, and
      the "_" is for any possibility; match, transition or transversion.
      The comma "," can be used as a seed separator.  The program speed
      depends on the weight of each pattern (number of # + 1/2 number of
      @): decreasing the weight increases sensitivity, but slows down the
      search.
      <br />
      <br />

      <b>Seed hit criterion</b>: you can specify the number of seeds 
      that have to match the query sequence. Default value is double.  		
      <br />
      <br />
      <b>Match/transversion/transition/undetermined symbol scores</b>: This is
      simply the scoring system used to assess the quality of a pairwise
      alignment. Default values are 5 for a match,-4 for a transversion,
      -3 for a transition and -4 for undetermined symbols.
      <br /> 
      <br />
      <b>Gap costs</b>: This is the affine penalty scores associated to
      gaps in the score of a pairwise alignment.  Default values are -16
      for a gap opening and -4 for a gap extension.  
      <br /> 
      <br />

      <b>E-value (expectation value) threshold</b>: This setting
      specifies the statistical significance threshold for reporting
      alignments against database sequences. For example, when this
      parameter is set to 5, it means that 5 such matches are expected to
      be found merely by chance, according to the stochastic model of
      Karlin and Altschul (1990).  Lower E-value thresholds are more
      stringent, leading to fewer chance matches being reported.
      <br />
      <br />

      <div class="help_soft_title">Reference</div>
      Noe L., Kucherov G.  
      YASS: enhancing the sensitivity of DNA similarity search
      Nucleic Acids Research, 33(2):W540-W543, 2005
      <br />
      <br />
      <a href="http://bionfo.lifl.fr/yass">YASS web site</a>
      "
      