#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Remove temporary files and directories that are :
#   - not accessed since a given number of days (specified by project_expiration
#                                                identifier in rnaspace.cfg file)
#     the directory is not removed)
#   - not in the standard temporary file /tmp
#   - not egal to .svn at the first level
# Now there is a global temporary directory defined in rnaspace.cfg
# and an another where dynamically generated images are stored

# Remove projets that are not accessed since a given number of days 
# Note that if just one file was accessed recently, the project is kept

import sys,os,time


# INITIALISATION
# Read configuration variables in rnaspace.cfg file using test code
sys.path.append("../test/scripts")
from common import WORKSPACE_DIR, scr
STORAGE_SEC = scr.get('storage','project_expiration')*24*3600
DIR_TMP = scr.get('storage','tmp_dir')


# FUNCTIONS DEFINITION
# Remove a tree file
def remove_tree(d):
    try:
        for e in os.listdir(d):
            e=os.path.join(d, e)
            if os.path.isfile(e):
                os.remove(e)
            else:
                remove_tree(e)
        os.rmdir(d)
    except ValueError:
        print "ERROR in function remove_tree the argument is not a directory"

# Test if a tree file was recently accessed
def is_tree_recently_accessed(d):
    try:
        is_accessed = False
        for e in os.listdir(d):
            e=os.path.join(d, e)
            if os.path.isfile(e):
                is_accessed = is_file_recently_accessed(e)
            else:
                is_accessed = is_tree_recently_accessed(e)
        return is_accessed
    except ValueError:
        print "ERROR in function is_tree_recently_accessed the argument is not a directory"

# Test if a file was recently accessed
def is_file_recently_accessed(f):
    if os.path.isdir(f):
        print "WARNING in function is_file_recently_accessed a directory is given instead of a file"
    is_accessed = False
    if (time.time() - os.path.getatime(f) < STORAGE_SEC):
        is_accessed = True
    return is_accessed


# CLEAN TEMPORARY DIRECTORIES
for directory in [DIR_TMP]:
    if directory!="/tmp" and directory!="/tmp/":
        for e in os.listdir(directory):
            if e!= ".svn":
                e = os.path.join(directory,e)
                if os.path.isfile(e):
                    if not is_file_recently_accessed(e):
                        os.remove(e)
                else:
                    if not is_tree_recently_accessed(e):
                        remove_tree(e)
                

# CLEAN WORKSPACE
# Remove projects only when all files are not been accessed recently
# (or when they do not contain files)
for user in os.listdir(WORKSPACE_DIR):
    dir_user = os.path.join(WORKSPACE_DIR,user)
    for project in  os.listdir(dir_user):
        project = os.path.join(dir_user, project)
        if os.path.isdir(project):
            if not is_tree_recently_accessed(project):
                print "Remove project: "+project
                remove_tree(project)
    l = os.listdir(dir_user) 
    if len(l) == 1:
        f = os.path.join(dir_user, l[0])
        if not is_file_recently_accessed(f):
            print "Remove user: "+user
            os.remove(f)
            os.rmdir(dir_user)
        



