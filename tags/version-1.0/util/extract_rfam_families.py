#!/usr/bin/env python

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Read rfam database and generate a file containing:
<family_id>=<family_name>
for each family of rfam.
"""

import sys

def rfam_families(infile):
    fdb = open(infile, "r")
    families = {}
    for line in fdb:
        if line.startswith(">"):
            family = line.split()[1].split(';')[1]
            fid = line.split()[1].split(';')[0]
            families[fid] = family 
    fdb.close()

    families_s = families.keys()
    families_s.sort()
    fres = open("rfam_families.txt", "w")
    for fid in families_s:
        fres.write("%s=%s\n"%(fid, families[fid]))
    fres.close()

    return families

def main(argv):

    if len(argv) != 1:
        print "usage: ./extract_rfam_families.py rfam.fasta"
        exit(1)

    rfam = rfam_families(argv[0])

if __name__ == "__main__":
    main(sys.argv[1:])
