#!/usr/bin/env python




##################################################
## DEPENDENCIES
import sys
import os
import os.path
from os.path import getmtime, exists
import time
import types
import __builtin__
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import DummyTransaction
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers

##################################################
## MODULE CONSTANTS
try:
    True, False
except NameError:
    True, False = (1==1), (1==0)
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '2.0.1'
__CHEETAH_versionTuple__ = (2, 0, 1, 'final', 0)
__CHEETAH_genTime__ = 1269513855.7475619
__CHEETAH_genTimestamp__ = 'Thu Mar 25 11:44:15 2010'
__CHEETAH_src__ = 'rnaspace_template.tmpl'
__CHEETAH_srcLastModified__ = 'Thu Mar 25 11:38:21 2010'
__CHEETAH_docstring__ = 'Autogenerated by CHEETAH: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class rnaspace_template(Template):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        Template.__init__(self, *args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def home_id(self, **KWS):



        ## Generated from #def home_id: inactive at line 77, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''inactive''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def manage_id(self, **KWS):



        ## Generated from #def manage_id: inactive at line 78, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''inactive''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def predict_id(self, **KWS):



        ## Generated from #def predict_id: inactive at line 79, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''inactive''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def explore_id(self, **KWS):



        ## Generated from #def explore_id: inactive at line 80, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''inactive''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def header_title(self, **KWS):



        ## Generated from #def header_title: none at line 81, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''none''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def info_boxes(self, **KWS):



        ## Generated from #def info_boxes: inactive at line 82, col 5.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''inactive''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def respond(self, trans=None):



        ## CHEETAH: main method generated for this template
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        #  RNAspace: non-coding RNA annotation platform
        #  Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
        #  
        #  This program is free software: you can redistribute it and/or modify
        #  it under the terms of the GNU General Public License as published by
        #  the Free Software Foundation, either version 3 of the License, or
        #  (at your option) any later version.
        #  
        #  This program is distributed in the hope that it will be useful,
        #  but WITHOUT ANY WARRANTY; without even the implied warranty of
        #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        #  GNU General Public License for more details.
        #  
        #  You should have received a copy of the GNU General Public License
        #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
        # 
        write('''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
\t  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>RNAspace</title>
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-15" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Language" content="fr" />
    <link href="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 27, col 17
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 27, col 17.
        write('''ressource/img/favicon.ico" rel="SHORTCUT ICON" />
    <link href="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 28, col 17
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 28, col 17.
        write('''ressource/css/rnaspace.css" rel="stylesheet"
\t  type="text/css" />

    <script type="text/javascript" 
\t    src="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 32, col 11
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 32, col 11.
        write('''ressource/js/jquery-1.3.1.min.js" >
    </script>
    
\t''')
        _v = VFFSL(SL,"import_ressources",True) # '$import_ressources' on line 35, col 2
        if _v is not None: write(_filter(_v, rawExpr='$import_ressources')) # from line 35, col 2.
        write('''
    
    <script type="text/javascript">
     <!--

      $(document).ready(function(){
      var mount_point = "''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 43, col 26
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 43, col 26.
        write('''";      
\tvar help_link = {"Home": "#help1", "1.Manage": "#help3", "1.Load data": "#help3", "2.Predict": "#help4", "3.Explore": "#help5"}
\t$("#help").attr("href", mount_point + "help" + help_link[$(".active").html()])      
      
\tdocument.title = "RNAspace - " + $(".active").html();
     
\tif (!$("#home_btn").hasClass("active")) {
\t  var cookieStr = "ck_check=checkingnow";
\t  document.cookie = cookieStr;
\t  if (document.cookie.indexOf(cookieStr) <= -1){
\t    window.location.replace(mount_point + "cookies_error");
\t  }
\t}
     
\t$("#manage_btn").attr("href", mount_point + "manage");
\t$("#predict_btn").attr("href", mount_point + "predict");
\t$("#explore_btn").attr("href", mount_point + "explore");
\t  
\t// when we click on a toogletext element, display added sequences
        $("a.info_section").click( function () {
          $(this).next(".addedseq").slideToggle("normal");
          return false;
        }); 
\t  
     ''')
        _v = VFFSL(SL,"jquery",True) # '$jquery' on line 69, col 6
        if _v is not None: write(_filter(_v, rawExpr='$jquery')) # from line 69, col 6.
        write('''
       });
     ''')
        _v = VFFSL(SL,"javascript",True) # '$javascript' on line 73, col 6
        if _v is not None: write(_filter(_v, rawExpr='$javascript')) # from line 73, col 6.
        write('''
     -->
    </script>
    

  </head>
  <body>
    <div id="container">       
    
      <div id="logo"><a href="/"></a></div>    
    
      <div id="header">
    \t<p> ''')
        _v = VFFSL(SL,"header_title",True) # '$header_title' on line 91, col 10
        if _v is not None: write(_filter(_v, rawExpr='$header_title')) # from line 91, col 10.
        write(''' </p>
      </div>   
\t\t  \t\t  
      <div id="menu">
\t<ul>
\t  <li>
\t    <a href="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 97, col 15
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 97, col 15.
        write('''" id="home_btn" 
\t       class="''')
        _v = VFFSL(SL,"home_id",True) # '$home_id' on line 98, col 16
        if _v is not None: write(_filter(_v, rawExpr='$home_id')) # from line 98, col 16.
        write('''">Home</a>
\t  </li>
''')
        if VFFSL(SL,"authentification_platform",True): # generated from line 100, col 4
            write('''\t  <li>
\t    <a href="''')
            _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 102, col 15
            if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 102, col 15.
            write('''" id="manage_btn" 
\t       class="''')
            _v = VFFSL(SL,"manage_id",True) # '$manage_id' on line 103, col 16
            if _v is not None: write(_filter(_v, rawExpr='$manage_id')) # from line 103, col 16.
            write('''">1.Manage</a>
\t  </li>
''')
        else: # generated from line 105, col 4
            write('''\t  <li>
\t    <a href="''')
            _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 107, col 15
            if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 107, col 15.
            write('''" id="manage_btn" 
\t       class="''')
            _v = VFFSL(SL,"manage_id",True) # '$manage_id' on line 108, col 16
            if _v is not None: write(_filter(_v, rawExpr='$manage_id')) # from line 108, col 16.
            write('''">1.Load data</a>
\t  </li>
''')
        write('''\t  <li>
\t    <a href="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 112, col 15
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 112, col 15.
        write('''" id="predict_btn" 
\t       class="''')
        _v = VFFSL(SL,"predict_id",True) # '$predict_id' on line 113, col 16
        if _v is not None: write(_filter(_v, rawExpr='$predict_id')) # from line 113, col 16.
        write('''">2.Predict</a>
\t  </li>
\t  <li>
\t    <a href="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 116, col 15
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 116, col 15.
        write('''" id="explore_btn" 
\t       class="''')
        _v = VFFSL(SL,"explore_id",True) # '$explore_id' on line 117, col 16
        if _v is not None: write(_filter(_v, rawExpr='$explore_id')) # from line 117, col 16.
        write('''">3.Explore</a>
\t  </li>
\t</ul>
      </div>

      <div id="content">
\t<div id="help_o">
\t  <a id="help" href="" target="blank">
\t    <img src="''')
        _v = VFFSL(SL,"mount_point",True) # '${mount_point}' on line 125, col 16
        if _v is not None: write(_filter(_v, rawExpr='${mount_point}')) # from line 125, col 16.
        write('''ressource/img/icon/help_o.png"
\t\t class="border0" alt="help" />
\t  </a>
\t</div>

\t<noscript>
\t  <div id="crb_header">
\t    <div id="crb_title"> J a v a s c r i p t &nbsp; W a r n i n g </div>
\t    <div id="crb_content">
\t      If you see this message, your web browser doesn\'t support 
\t      JavaScript or JavaScript is disabled. Please enable JavaScript
\t      in your browser settings so RNAspace can be functional. 
\t    </div>
\t  </div>
\t</noscript>

''')
        if VFFSL(SL,"info_boxes",True) != "inactive": # generated from line 141, col 2
            write('''\t<div class="infobar">
\t  ''')
            _v = VFFSL(SL,"infobar",True) # '$infobar' on line 143, col 4
            if _v is not None: write(_filter(_v, rawExpr='$infobar')) # from line 143, col 4.
            write('''
\t</div>
\t
\t<div class="page">
''')
        write('''
\t''')
        _v = VFFSL(SL,"page_content",True) # '$page_content' on line 149, col 2
        if _v is not None: write(_filter(_v, rawExpr='$page_content')) # from line 149, col 2.
        write('''

''')
        if VFFSL(SL,"info_boxes",True) != "inactive": # generated from line 151, col 2
            write('''\t</div>
''')
        write('''
      </div>

      <div id="big_footer">
\t<div id="footer">
\t  <p>Comments and remarks: 
\t    <a href="mailto:contact@rnaspace.org">contact@rnaspace.org</a>.
\t  </p>
\t</div>
      </div>

    </div>
  </body>
</html>
''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_rnaspace_template= 'respond'

## END CLASS DEFINITION

if not hasattr(rnaspace_template, '_initCheetahAttributes'):
    templateAPIClass = getattr(rnaspace_template, '_CHEETAH_templateClass', Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(rnaspace_template)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit http://www.CheetahTemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=rnaspace_template()).run()


