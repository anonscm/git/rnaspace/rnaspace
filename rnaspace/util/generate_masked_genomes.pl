#!/usr/bin/perl -w

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

###############################################################################
###############################################################################
##									     ##
##									     ##
##	Goal : Mask all genomes coding parts for the comparative analysis    ##
##             of the RNAspace platform.                                     ##
##									     ##
##                                                                           ##
###############################################################################
###############################################################################



###############################################################################
###############################################################################
##									     ##
##									     ##
##				MODULES  				     ##
##									     ##
##									     ##
###############################################################################
###############################################################################

use strict;
use warnings;
use Getopt::Long qw(:config noauto_abbrev);
use Pod::Usage;
use File::Spec;
use File::Basename;




###############################################################################
###############################################################################
##									     ##
##									     ##
##			     GLOBAL VARIABLES				     ##
##									     ##
##									     ##
###############################################################################
###############################################################################

#In directory
my $indir = "";

#Out directory
my $outdir = "";

#Directories to except in the processus
my @except;

#Mode of masking : 0 -> only CDS (Default)
#                  1 -> CDS + RNAs (tRNA, rRNA, misc_RNA)
my $mode = 0;

#Help wanted
my $help = 0;

#Verbose mode
my $verbose = 0;

#Booleans that says if Bacteria, Archaea and Eukaryotes must be processed
my $process_bacteria = 1;
my $process_archaea = 1;
my $process_eukaryotes = 1;

#Bacteria variables
my $name_bacteria = "Bacteria";
my $dir_bacteria = "";
my $outdir_bacteria = "";

#Archaea variables
my $name_archaea = "Archaea";
my $dir_archaea = "";
my $outdir_archaea = "";

#Eukaryotes variables
my $name_eukaryotes = "*";
my @dir_eukaryotes;
my $current_dir_eukaryote = "";
my $current_outdir_eukaryote = "";

#Names of subdirs to use inside Bacteria and Archaea : "current/flat"
my $name_current = "current";
my $name_flat = "fasta";

#Suffix of fasta files
my $fasta_suffix = ".fna";

#Suffix of genbank files
my $gbk_suffix = ".gbk";

#Suffix of masked file : none if output is entered, "_masked" if no output
my $mask_suffix = "_masked";


###############################################################################
###############################################################################
##									     ##
##									     ##
##				     MAIN				     ##
##									     ##
##									     ##
###############################################################################
###############################################################################



#Parse options
GetOptions('indir=s' => \$indir,
	   'outdir=s' => \$outdir,
	   'except=s' => \@except,
	   'mode=i' => \$mode,
	   'v' => \$verbose,
	   'verbose' => \$verbose,
	   'help' => \$help,
	   'h' => \$help) or pod2usage(2);

#Help
pod2usage(1) if $help;

#Check parameters
if($indir eq ""){
  print "\nYou must give an input directory\n\n";
  pod2usage(1);
}
else{
  if(! -d $indir){
    print "\nThe input directory \"$indir\" doesn't exist\n\n";
    pod2usage(1);
  }
}
#Case no output entered : the mask fasta will be written under the same
#directory than the original fasta file
if($outdir eq ""){
  $outdir = $indir;
}
else{

  #If an output is set : there won't be any masking suffix ("_masked")
  #Just verify that the outdir provided in not the same than the indir
  my $temp_indir = $indir;
  my $temp_outdir = $outdir;
  $temp_indir =~ s/[\/]$//;
  $temp_outdir =~ s/[\/]$//;

  if($temp_indir ne $temp_outdir){
    $mask_suffix = "";
  }

  if(! -d $outdir){
    mkdir "$outdir" or
      ((print "\nCannot create output directory \"$outdir\"\n\n") &&
       (pod2usage(1)))
  }
}
for(my $i = 0; $i < @except; $i++){
  if($except[$i] eq "Bacteria"){
    $process_bacteria = 0;
  }
  else{
    if($except[$i] eq "Archaea"){
      $process_archaea = 0;
    }
    else{
      if($except[$i] eq "Eukaryotes"){
	$process_eukaryotes = 0;
      }
      else{
	print "\nThe domain to except must be \"Bacteria\", \"Archaea\" or \"Eukaryotes\"\n\n";
	pod2usage(1);
      }
    }
  }
}
if(($mode < 0) || ($mode > 1)){
  print "\nThe masking mode has to be 0 (CDS) or 1 (CDS + RNAs)\n\n";
  pod2usage(1);
}


#Bacteria process
if($process_bacteria){

  $dir_bacteria = File::Spec->catdir($indir, 
				     $name_bacteria,
				     $name_current,
				     $name_flat);

  $outdir_bacteria = File::Spec->catdir($outdir,
					$name_bacteria,
					$name_current,
					$name_flat);
  #Create outdirs
  &create_directory(File::Spec->catdir($outdir,$name_bacteria));

  &create_directory(File::Spec->catdir($outdir,
				       $name_bacteria,
				       $name_current));

  &create_directory($outdir_bacteria);

  if($verbose){
    print "\nPROCESSING BACTERIA\n===================\n";
  }

  #Process bacteria directory
  &process_dir($dir_bacteria,$outdir_bacteria);

}


#Archaea process
if($process_archaea){

  $dir_archaea = File::Spec->catdir($indir,
				    $name_archaea,
				    $name_current,
				    $name_flat);

  $outdir_archaea = File::Spec->catdir($outdir,
				       $name_archaea,
				       $name_current,
				       $name_flat);
  #Create outdirs
  &create_directory(File::Spec->catdir($outdir,
				       $name_archaea));

  &create_directory(File::Spec->catdir($outdir,
				       $name_archaea,
				       $name_current));

  &create_directory($outdir_archaea);


  if($verbose){
    print "\nPROCESSING ARCHAEA\n==================\n";
  }

  #Process archaea directory
  &process_dir($dir_archaea,$outdir_archaea);

}


#Eukaryotes process
if($process_eukaryotes){

  if($verbose){
    print "\nPROCESSING EUKARYOTES\n=====================\n";
  }

  #Eukaryotes directories are within the outdir
  my $search_eukaryotes = File::Spec->catdir($indir,$name_eukaryotes);
  @dir_eukaryotes = glob($search_eukaryotes);

  foreach my $current_dir(@dir_eukaryotes){

    my @dirs = File::Spec->splitdir($current_dir);
    my $species = $dirs[@dirs - 1];

    #Exclude "Archaea" et "Baacteria" directories
    if(($species ne $name_bacteria) &&
       ($species ne $name_archaea)){

      if($verbose){
	print "($species)\n";
      }

      $current_dir_eukaryote = File::Spec->catdir($current_dir,
						  $name_current,
						  $name_flat);

      $current_outdir_eukaryote = File::Spec->catdir($outdir,
						     $species,
						     $name_current,
						     $name_flat);
      #Create outdirs
      &create_directory(File::Spec->catdir($outdir,
					   $species));

      &create_directory(File::Spec->catdir($outdir,
					   $species,
					   $name_current));

      &create_directory($current_outdir_eukaryote);

      #Process directory
      &process_dir($current_dir_eukaryote,$current_outdir_eukaryote);


    }
  }



}





###############################################################################
###############################################################################
##									     ##
##									     ##
##			     FUNCTIONS   				     ##
##									     ##
##									     ##
###############################################################################
###############################################################################


###############################################################################
#									      #
#	Function process_dir						      #
#									      #
#	Goal : process a directory                                            #
#									      #
#	Param :	                                                              #
#               - $dir : directory to process          			      #
#               - $outdir : outdirectory                                      #
#									      #
#	Return :                                                              #
#               - nothing but the processing step has been done               #
#									      #
###############################################################################

sub process_dir{

  #Directory to process
  my $dir = "";

  #Out directory
  my $outdir = "";

  ($dir, $outdir) = @_;


  #Check if the directory exists
  if(! -d $dir){
    print "\nThe directory \"$dir\" doesn't exist : it won't be processed\n\n";
    return;
  }

  #Get all subdirectories
  my $search_subdirs = File::Spec->catdir($dir,
					  "*");
  my @subdirs = glob($search_subdirs);

  foreach my $sub(@subdirs){

    #Get fasta files (.fna suffix)
    my $search_fasta_files = File::Spec->catdir($sub,
						"*".$fasta_suffix);
    my @fasta_files = glob($search_fasta_files);

    if(@fasta_files != 0){

      #Create same subdirectory in outdir : last path element which
      #represents species name or chromosome name
      my @dir_names = File::Spec->splitdir($sub);
      my $nb_path_elements = @dir_names;
      my $current_outdir =
	File::Spec->catdir($outdir,
			   $dir_names[$nb_path_elements - 1]);

      &create_directory($current_outdir);

      if($verbose){
	print "- $dir_names[$nb_path_elements - 1] :\n";
      }

      #Process each fasta file
      foreach my $fasta(@fasta_files){

	(my $name_fasta,
	 my $dir_fasta,
	 my $suffix) = fileparse($fasta,qr/\.[^.]*/);


	#Get genbank associate file
	my $genbank_file = File::Spec->catfile($dir_fasta,
					       $name_fasta.$gbk_suffix);

	if (-e $genbank_file) {

	  if ($verbose) {
	    print "\t- $name_fasta\n";
	  }


	  #Get fasta id and sequence from fasta file
	  (my $id, my $seq) = &read_fasta($fasta);

	  if (($id ne "") && ($seq ne "")) {

	    #Get masking positions from the genbank file
	    my @positions = &get_masking_positions($genbank_file);

	    if (@positions != 0) {

	      #Process each region
	      foreach my $pos (@positions) {
		my $start = -1;
		my $end = -1;
		if ($pos =~ /([0-9]+)\.\.([0-9]+)/) {
		  $start = $1;
		  $end = $2;
		}

		#Build mask region
		my $mask_region = "";
		for (my $i = 0; $i < $end - $start + 1; $i++) {
		  $mask_region .= "N";
		}

		#Replace region by masked one
		my $sub = substr($seq,
				 $start - 1,
				 $end - $start + 1,
				 $mask_region);

	      }

	      #Write new fasta
	      my $name_new_fasta = $name_fasta.$mask_suffix.$suffix;
	      my $outfile = File::Spec->catfile($current_outdir,
						$name_new_fasta);

	      &write_fasta($outfile, $id, $seq);

	    }
	  }
	}
      }
    }
  }
}

###############################################################################
#									      #
#	Function get_masking_positions      				      #
#									      #
#	Goal : Get masking positions from the genabnk provided file according #
#              to the masking mode wanted                                     #
#									      #
#	Param :	                                                              #
#               - $genbank_file : the genabnk_file to read                    #
#									      #
#	Return :                                                              #
#               An array of positions to mask in the format begin..end        #
#									      #
###############################################################################

sub get_masking_positions{

  #Genbank file to process
  my ($genbank_file) = @_;

  #Regular expression matching coding positions
  my $regexp_pos = "";

  #Array of positions that will be returned
  my @positions;

  #Build beginning of regular expression according to the mode wanted :
  #0 -> only CDS, 1 -> CDS + RNAs 
  if($mode == 0){
    $regexp_pos .= "(\\s+CDS";
  }
  else{
    $regexp_pos .= "(\\s+(CDS|[tr]RNA|misc_RNA)";
  }
  $regexp_pos .= "\\s+(complement\\()?(join\\()?(<?[0-9]+\\.\\.>?";
  $regexp_pos .= "[0-9]+,?(\\s+)?)+\\)?)";

  #Parse genbank file
  my $content_genbank = &read_file($genbank_file);
  while($content_genbank =~ /$regexp_pos/g){
    my $line = $1;
    while($line =~ /([0-9]+)\.\.>?([0-9]+)/g){
      push(@positions,$1."..".$2);
    }

  }
  return @positions;

}



###############################################################################
#									      #
#	Function create_directory       				      #
#									      #
#	Goal : Create a directory                                             #
#									      #
#	Param :	                                                              #
#               - $dir : the directory to create                              #
#									      #
#	Return :                                                              #
#               Nothing but the directory is created                          #
#									      #
###############################################################################

sub create_directory{

  (my $dir) = @_;

  if(! -d $dir){
    mkdir $dir or
      die "\nCould not create directory \"$dir\"\n\n";
  }

}


###############################################################################
#									      #
#	Function read_file						      #
#									      #
#	Goal : read a file and return its content                	      #
#									      #
#	Param :	                                                              #
#               - $file : file to read           			      #
#									      #
#	Return :                                                              #
#               - $content : the content of the file                          #
#									      #
###############################################################################

sub read_file{

  #Content that will be returned
  my $content = "";

  (my $file) = @_;

  open(FILE,$file) or die ("\nCould not open file \"$file\"\n\n");
  while(<FILE>){
    $content .= $_;
  }
  close(FILE);

  return $content;



}


###############################################################################
#									      #
#	Function read_fasta						      #
#									      #
#	Goal : read a fasta file and get the id and the sequence	      #
#									      #
#	Param :	                                                              #
#               - $fasta : fasta file to read    			      #
#									      #
#	Return :                                                              #
#               - $id : the id of the fasta file                              #
#               - $seq : the sequence                                         #
#               If the file is not in fasta format returns ("","")            #
#									      #
###############################################################################

sub read_fasta{

  #Location of the fasta file
  my $fasta;

  #Content of the fasta file
  my $content = "";

  #Id of the fasta file
  my $id = "";

  #Sequence
  my $seq = "";

  #Regular expression matching id and sequence
  my $regexp_fasta = "^>(.+)[\\n\\r]";
  $regexp_fasta .= "([ATCGNURYKMSWBDHVatcgnurykmswbdhv\\n\\r]+)\$";

  ($fasta) = @_;

  $content = &read_file($fasta);

  #Get id and sequence
  if($content =~ /$regexp_fasta/){
    $id = $1;
    $seq = $2;
    #Deleting end line characters in sequence
    $seq =~ s/[\n|\r]//g;
  }

  return ($id,$seq);


}


###############################################################################
#									      #
#	Function write_fasta						      #
#									      #
#	Goal : write a fasta file                                             #
#									      #
#	Param :	                                                              #
#               - $file : the location of the file where to write             #
#               - $id : the id of the fasta file (without > at the beginning) #
#               - $seq : the sequence                                         #
#									      #
#	Return : 							      #
#		- nothing but the file is created		              #
#									      #
###############################################################################

sub write_fasta{

  #Length of the sequence
  my $lg;

  (my $file, my $id, my $seq) = @_;

  $lg = length($seq);
  $seq = uc($seq);

  chomp($id);
  $id = ">".$id;

  #Writing of the new file in fasta format
  open(OUT, ">$file") or die ("\nCould not write in file \"$file\"\n\n");
  print OUT $id;
  for(my $i = 0; $i < $lg; $i++){
    if(($i%70) == 0){
      print OUT "\n";
    }
    print OUT substr($seq,$i,1);
  }
  close(OUT);

}


###############################################################################
###############################################################################
##									     ##
##									     ##
##			   USAGE AND POD DOCUMENTATION			     ##
##									     ##
##									     ##
###############################################################################
###############################################################################


=head1 NAME

C<generate_mask_genomes.pl> - Mask coding script of all genomes for the \
comparative analysis of the RNAspace platform

=head1 SYNOPSIS

generate_mask_genomes.pl --indir=INDIR [options]

=head1 OPTIONS

=over 8

=item B<--indir>=INDIR

Location of the root directory containing all genomes to mask.

=item B<--outdir>=OUTDIR

Location of the output directory. If not set : each masked genome will be
located aside the original fasta file with a "_masked" suffix. If an output
directory is provided, the root directory path will be reconstruct under that
output directory and masked fasta files will have the same name than the orginal
files.

=item B<--except>=DOMAIN

Name of domain not to mask. It can be : "Bacteria", "Archaea" or "Eukaryotes".

=item B<--mode>=[0,1]

Masking mode : 0 -> CDS (Default); 1 -> CDS + RNAs (tRNA, rRNA, misc_RNA).

=item B<-v, --verbose>

Verbose mode.

=item B<-h, --help>

Prints this help message and exits.

=back

=head1 AUTHORS

Benjamin Grenier-Boley <benjamin.grenier-boley@inria.fr>

=cut
