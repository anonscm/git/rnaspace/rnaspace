#!/usr/bin/env python

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The script look for rfam ids for which the family name changed between 2 
versions of rfam. The file `ids_different.log` is generated and contains 
the difference between the two versions.

example:

./compare_rfam_ids.py Rfam_9.1.fasta Rfam_8.1.fasta
"""

import sys

def get_ids_family(rfam):

    """
    Read `rfam` and return a dictionary containing the family name for each id
    """
    ids = {}
    frfam = open(rfam, "r")
    
    for line in frfam:
        if line.startswith(">"):
            family = line.split()[1].split(';')[1]
            fid = line.split()[1].split(';')[0]
            ids[fid] = family 
    frfam.close()
    
    return ids

def compare_ids(ids1, ids2):
    """
    Compare two dictionaries generated with get_ids_family function.

    Return True if the dictionaries are identical, False otherwise.
    """

    different_ids = {}

    for fid in ids1:
        if fid in ids2 and ids1[fid] != ids2[fid]:
            different_ids[fid] = (ids1[fid], ids2[fid])

    if len(different_ids) > 0:
        flog = open("ids_different.log", "w")
        for fid in different_ids:
            flog.write("%s: [%s, %s]\n" % (fid, different_ids[fid][0],
                                           different_ids[fid][1]))
        flog.close()
        return False
    else:
        return True

def main(argv):

    if len(argv) != 2:
        print "usage: ./compare_rfam_ids.py rfam1 rfam2"
        exit(1)

    rfam1 = argv[0]
    rfam2 = argv[1]

    rfam1_ids = get_ids_family(rfam1)
    rfam2_ids = get_ids_family(rfam2)

    result = compare_ids(rfam1_ids, rfam2_ids)
    if result:
        print "OK: ids are the same in both versions"
    else:
        print "KO: ids are different"
        print "differences are written in ids_different.log"

if __name__ == "__main__":
    main(sys.argv[1:])
