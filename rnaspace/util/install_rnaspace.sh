#!/bin/sh
#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Remove temporary files and directories that are :
#   - not accessed since a given number of days (specified by project_expiration
#                                                identifier in rnaspace.cfg file)
#     the directory is not removed)
#   - not in the standard temporary file /tmp
#   - not egal to .svn at the first level
# Now there is a global temporary directory defined in rnaspace.cfg
# and an another where dynamically generated images are stored

# Partial installation of RNAspace last development version
# Takes as argument the path in which to install RNAspace
 
if [ -z "$1" ]
  then
    echo "No argument supplied"
	echo "Usage: `basename $0` <PATH>"
	exit 1
fi
ROOT_PATH=$(readlink -f $1)
echo "Installing RNAspace in $ROOT_PATH"


# 1/ INSTALL REQUIRED GENOMES
echo "Manual installation of genomes required"


# 2/ DEFINE WORKSPACE
rnaspace_workspace="$ROOT_PATH/rnaspace_workspace/"
echo "Creating Workspace directory in $rnaspace_workspace"
mkdir -p $rnaspace_workspace


# 3/ LOAD SOURCES FROM SVN
cd $ROOT_PATH
echo "Creating sources directory"
mkdir -p rnaspace_sources
echo "Checking out latest version from Subversion..."
cd rnaspace_sources && svn checkout svn://svn.code.sf.net/p/rnaspace/code/rnaspace rnaspace
CONVERT_SCRIPT="$ROOT_PATH/rnaspace_sources/rnaspace/util/convert_database.py"


# 4/ LOAD DATABASE
cd $ROOT_PATH
echo "Creating database directory"
mkdir -p rnaspace_db
mkdir -p sequences
cd sequences

## Install Rfam
echo "Downloading sequence databases"
wget ftp://ftp.sanger.ac.uk/pub/databases/Rfam/9.1/Rfam.fasta.gz
gunzip Rfam.fasta.gz
mv Rfam.fasta Rfam_9.1.fasta
$CONVERT_SCRIPT -d Rfam_9.1 Rfam_9.1.fasta ../rnaspace_db

wget ftp://ftp.sanger.ac.uk/pub/databases/Rfam/10.0/Rfam.fasta.gz
gunzip Rfam.fasta.gz
mv Rfam.fasta Rfam_10.0.fasta
$CONVERT_SCRIPT -d Rfam_10.0 Rfam_10.0.fasta ../rnaspace_db

## Install frnadb
wget http://www.ncrna.org/frnadb/files/sequence.zip
unzip sequence.zip
mv sequence.fasta fRNAdb_3.0.fasta
$CONVERT_SCRIPT -d fRNAdb_3.0 fRNAdb_3.0.fasta ../rnaspace_db
#### fails : infile is not a frnadb database

## Install miRBase
wget ftp://mirbase.org/pub/mirbase/CURRENT/hairpin.fa.gz
gunzip hairpin.fa.gz
mv hairpin.fa miRBase_13.0.fasta
$CONVERT_SCRIPT -d miRBase_13.0 miRBase_13.0.fasta ../rnaspace_db



# 5/ INSTALL RNA GENE FINDERS
cd $ROOT_PATH
mkdir -p rnaspace_softwares
echo "Manual installation of RNA gene finders required"


# 6/ INSTALL PROGRAMS TO PREDICT RNA SECONDARY STRUCTURE
echo "Manual installation of RNA secondary structure prediction programs required"


# 7/ INSTALL VISULIZATION TOOLS
echo "Manual installation of visualization tools required"


# 8/ INSTALL GENOME MASKING  TOOLS
echo "Manual installation of genome masking tools required"


# 9/ CONFIGURE 
sed -ie "s#workspace_dir = /STOCK#workspace_dir = $rnaspace_workspace#g" rnaspace_sources/rnaspace/cfg/rnaspace.cfg 
echo "Manual configuration required for files in rnaspace_sources/rnaspace/cfg and rnaspace_sources/rnaspace/cfg/predictors directories"


# Finished
echo "RNAspace installation finished."
echo "To run RNAspace, please execute:"
echo "cd $ROOT_PATH/rnaspace_sources/rnaspace/"
echo "./rnaspace_on_web"
