# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
infernal_utils.py
This script provide multiples output, using merged Rfam covariance models.
In input :  It takes 2 parameters, the first one is the merged Rfam file.
            The second is the output dir for the simple convariance models.
In output : It will create all the simple covariance models, into the output dir given in parameter.
            It will give the rfam families, it's a table which give the RFxxxxx.cm file to a family name.
            It will give the rfam list, it's a list which give us a string containing 
            all the family names separated by comma. Used in the conf file of infernal.
"""

import sys
import os.path

if __name__ == '__main__':
    
    rfam_families = "rfam_families.txt"
    rfam_list = "rfam_list.txt"
    
    '''
        Main function, we test if we gave one parameter and if this file exist.
    '''
    if len(sys.argv) == 3:
        if os.path.exists(sys.argv[2]):
            f = open(sys.argv[1], 'r')
            txt = f.readline()
            list1 = []
            list2 = []
            file = ''
            
            while not txt == '':
                file += txt
                if txt.count('NAME') == 1:
                    part = txt.rstrip().split()
                    name = part[1]
                    
                elif txt.count('ACCESSION') == 1:
                    part = txt.rstrip().split()
                    access = part[1]
                    list1.append(name+' ['+access+'],')
                    list2.append(access+'='+name)
                    
                elif txt.count('//') == 1:
                    # It will put one covariance model into one file
                    ffile = open(sys.argv[2]+'/'+access+'.cm','w')
                    ffile.write(file)
                    ffile.close()
                    file = ''
                txt = f.readline()
            list1.sort()
            list2.sort()
            
            # It be will used for informations
            rfamfamilies = open(rfam_families,'w')
            for i in list2:
                rfamfamilies.write(i+'\n')
            rfamfamilies.close()
                
                
            # It will be used for build the conf file of infernal
            rfamlist = open(rfam_list,'w')
            for i in list1:
                rfamlist.write(i)
            rfamfamilies.close()
        else:
            print "ERROR: The output directory doesn't exist!"
    
    else:
        print '''ERROR: not enough parameters, you must specify 2 parameters.\n 
        cmd $ python infernal_utils.py <path Rfam.cm> <output dir>'''
