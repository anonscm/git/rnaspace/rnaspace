#!/usr/bin/perl

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

##
## Convert Rfam stockholm seed file to fasta
##   - Input  : Rfam stockholm file
##   - Output : stdout
##

use strict;
use warnings;

if(@ARGV != 1) {
  print "\nUsage: rfam_stockholm2tfa.pl <input.stockholm>\n";
  print "Error: Stockholm file required\n\n";
  exit();
}

open(STOCK, "$ARGV[0]") || die "Error enable to open $ARGV[0]";

my %h_seq;
my $ac;
my $id;
while (my $line = <STOCK>) {
  chomp $line;
  next if($line eq '');
 
  if($line !~ /^#/ && $line !~ /^\/\/$/) {
    my @a_line = split('\s+', $line);
    $a_line[1] =~ s/\.//g;
    $h_seq{">$ac;$id;$a_line[0]"} .= $a_line[1];
  }

  $ac = $line if($line =~ s/#=GF\s+AC\s+//);
  $id = $line if($line =~ s/#=GF\s+ID\s+//);
}

foreach my $k (keys(%h_seq)) { print "$k\n$h_seq{$k}\n"; }

close STOCK;