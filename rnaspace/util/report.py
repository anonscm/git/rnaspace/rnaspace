#!/usr/bin/env python

#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
This script will provide a graphical display of statistics about the analysis of
the log file of RNAspace. Using Cherrypy, Cheetah & matplotlib to give to the 
user an web interface where he can choose the metrics that he wants to display 
and update.
Syntax: python report.py <logfile>
"""

import sys
import os
import datetime 
import matplotlib.pyplot
import cherrypy
import tempfile
from Cheetah.Template import Template
from cherrypy.lib import static

DATE_STOP = datetime.datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)
DATE_START =  DATE_STOP - datetime.timedelta(days=364)

class ListMap(object) :
    '''
        This class define a list where we can access the index of a defined value
    '''
    def __init__(self, initList=[], **kw) :
        self.list = []
        self.hash = {}
        if kw.has_key('key') :
            get_elem_key = kw['key']
        else :
            get_elem_key = lambda x: x
        self.gek = get_elem_key
        self += initList
    def append(self, x) :
        self.hash[self.gek(x)] = len(self)
        self.list.append(x)
    def __iadd__(self, lx) :
        for x in lx :
            self.append(x)
        return self
    def __len__(self) :
        return len(self.list)
    def __getitem__(self, i) :
        return self.list[i]
    def __iter__(self) :
        return self.list.__iter__()
    def __index__(self, x) :
        return self.hash[x]
    def index(self, x) :
        return self.hash[x]

class report:
    '''
        Main class, will grant a webpage where the user can visualize the statistics
        of the website. And select parameters in order to change the visualisation
        of the metrics.
    '''
    
    # list of list, that will be used in methods for the tests
    
    # this list give us the list of event corresponding to a determinated action
    list_event = {"ALL" : [1,2,3,4,5,6,7],
                  "ADD_SEQ" : [1],
                  "EXPORT" : [2],
                  "ERROR" : [3],
                  "ALIGN" : [4],
                  "PREDICT_ATYPICAL" : [5],
                  "PREDICT_COMPARATIVE" : [6],
                  "PREDICT_ALL" : [5,6,7]
                  }
                
    # this list contains the events ignored
    list_event_ignored = ["USER_COMBINE"]
    
    # this list is used to define the display, here choose to display 2 graphics
    # one for the number of events one for the the number of cpu
    list_event_cpu = ["ALIGN",
                      "PREDICT_ALL",
                      "PREDICT_ATYPICAL",
                      "PREDICT_COMPARATIVE"]
                      
                      
    # this is the list of errors
    list_event_error = ["ERROR",
                        "PREDICTION ERROR", 
                        "UNKNOWN ERROR", 
                        "DISK ERROR", 
                        "ALIGNMENT ERROR"]
                         
    # this list is used to define the events with only one graphic, number of events
    list_event_no_cpu = ["ADD_SEQ", 
                         "ERROR", 
                         "EXPORT"
                        ]
                         
    # this list will be used to generate the html form, the list of event
    list_event_option = ["ALL",
                         "ADD_SEQ",
                         "ERROR",
                         "EXPORT",
                         "PREDICT_ALL",
                         "PREDICT_ATYPICAL",
                         "PREDICT_COMPARATIVE"]
                         
    # These lists will be used to store the data from the 
    log_date = []
    log_event = []
    log_project = []
    log_cpu = []
    
    # Define the template of the page
    templateDef = '''
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            \t  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
            <head>
                <title>$title</title>
                <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-15" />
                <meta http-equiv="Content-Style-Type" content="text/css" />
                <meta http-equiv="Content-Language" content="fr" />
                <link href="img/favicon.ico" rel="SHORTCUT ICON" />
                <link href="css/rnaspace.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
            <div id="container">  
                <div id="logo">
                    <a href="/"></a>
                </div>
                <div id="header">
                    <p> Predict </p>
                </div>
                <div id="content">
                    <div class="content_header">
                        This page provide us statistics based on the log file of 
                        RNAspace.org, for the last month or the last year with 
                        differents events.
                    </div>
                    <div class="cbb_header">
                        <div class="cbb_title">RNAspace site statistics ($date_of_now)</div>
                            <div class="cbb_content"><br /><br />
                                <form action="index" method="GET" id="predict_form">
                                    <table style="width:99%; align=center;">
                                    <tr>
                                        <td width=200>Date format</td>
                                        <td>$date_option
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=200>Events</td>
                                        <td>
                                        <select name="event" class="select">
                                        $event_option
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=200>Projects</td>
                                        <td>
                                        <select name="project" class="select">
                                        $project_option
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=200></td>
                                        <td>
                                        <input type="submit" value="Update display" class="button">
                                        </td>
                                    </tr>
                                    </table>
                                </form>
                                $image_option
                            </div>
                        </div>
                    </div>
                <div id="big_footer">
                	<div id="footer">
                	  <p>Comments and remarks: 
                	    <a href="mailto:contact@rnaspace.org">contact@rnaspace.org</a>.
                	  </p>
                	</div>
                </div>
            </div>
            </body>
            </html>'''
    
    def read_log_file(self, filename):
        '''
            Will read the log file given in parameter, and store the data into
            the 4 arrays log_date, log_event, log_project, log_cpu
            The data are filtered by years, to avoid store 10 years of log in memory
            using LAST_YEAR array
            
            NB : There is a problem on RNAz event, the record on log can't split in
            14 but in 15, so I test the splitted string in order to save the data
            expected
        '''
        f = open(filename, 'r')
        txt = f.readline()
        logfile = []
        while not txt == '':
            if DATE_START<= datetime.datetime.strptime(txt[0:10], "%m %d %Y") <= DATE_STOP:
                list = txt.split('|')
                
                self.log_date.append(datetime.datetime.strptime(list[0][0:10], "%m %d %Y"))
                self.log_project.append(list[2])
                
                if list[4] == "PREDICT":
                    if list[7] == 'atypicalGC':
                        self.log_event.append(5)
                        self.log_cpu.append(float(list[12]))
                    elif list[7] == 'comparative':
                        self.log_event.append(6)
                        self.log_cpu.append(float(list[12]))
                    elif list[7] == 'CG-seq':
                        self.log_event.append(6)
                        self.log_cpu.append(float(list[12]))
                    elif list[7] == 'RNAz':
                        if len(list) == 14:
                            self.log_cpu.append(float(list[12]))
                        elif len(list) == 15:
                            self.log_cpu.append(float(list[13]))
                        self.log_event.append(6)
                    elif list[7] == 'BLAST':
                        self.log_event.append(6)
                        self.log_cpu.append(float(list[12]))
                    elif list[7] == 'caRNAc':
                        self.log_event.append(6)
                        self.log_cpu.append(float(list[12]))
                    elif list[7] == 'YASS':
                        self.log_event.append(6)
                        self.log_cpu.append(float(list[12]))
                    else:
                        self.log_event.append(7)
                        self.log_cpu.append(float(list[12]))
                elif list[4] ==  "ADD_SEQ":
                    self.log_event.append(1)
                    self.log_cpu.append(0)
                elif list[4] == "EXPORT_RNA":
                    self.log_event.append(2)
                    self.log_cpu.append(0)
                elif list[4] in self.list_event_error:
                    self.log_event.append(3)
                    self.log_cpu.append(0)
                elif list[4] == "ALIGN":
                    self.log_event.append(4)
                    self.log_cpu.append(float(list[7]))
                elif list[4] == "PREDICT_COMBINE":
                    self.log_event.append(7)
                    self.log_cpu.append(float(list[8]))
                elif list[4] in self.list_event_ignored:
                    pass
                
                else:
                    print "ERROR "+list[4]+" not defined!"
                    break
            txt = f.readline()


    def generate_records(self, date, event, project):
        '''
            Generate the records using the data from log file stored in memory
            and the parameters given in argument.
            The method create the blanks days, in order to display the days 
            without data.
        '''
        records = {'date': [], 'cpu_time':[], 'nb_events':[] }
        
        delta = datetime.timedelta(days=1)
        stop = DATE_STOP + delta
        
        # If the date is month, we create a list of 30 days
        if date == 'month':
            start = DATE_STOP - datetime.timedelta(days=29)
            tmp_start = start
            while tmp_start < stop:
                records['date'].append(tmp_start)
                records['cpu_time'].append(0)
                records['nb_events'].append(0)
                tmp_start += delta
        
        # If the date is year, we create a list of 365 days
        if date == 'year':
            start = DATE_START
            tmp_start = start
            while tmp_start < stop:
                records['date'].append(tmp_start)
                records['cpu_time'].append(0)
                records['nb_events'].append(0)
                tmp_start += delta
        
        # ListMap object grant us access to the index of the list of day by giving
        # a object list type in parameters, here its datetime
        l = ListMap(records['date'])
        
        # This loop will count the number of events and the cpu time
        for i in range(0,len(self.log_event)):
            if project == 'ALL' or self.log_project[i] == project:
                if self.log_date[i] >= start:
                    if self.log_event[i] in self.list_event[event]:
                        records['nb_events'][l.index(self.log_date[i])] += 1 
                        records['cpu_time'][l.index(self.log_date[i])] += self.log_cpu[i]
        
        
        
        return records
        
        
    def create_histograms(self, records, date, event, project, temp_img1, temp_img2):
        '''
            Create the histograms using the records generated by generate_records()
            the parameters given by the user, and 2 tempfile that will be used 
            to create the image graphics, in /tmp directory.
        '''
        
        # In these cases, we create two graphics, one for the number of events, one for cpu time
        if event in self.list_event_cpu or event == 'ALL':
            # Call the creation of a figure
            fig = matplotlib.pyplot.figure()
            ax = fig.add_subplot(111)
            
            # Define the type of plot with the x and y values, with time and values
            ax.bar(records['date'], records['nb_events'])
            
            # The plot options
            matplotlib.pyplot.title(event.upper()+' events for ' + project.upper() + ' project(s) last '+date)
            ax.set_ylabel('number of '+event.upper()+' events')
            
            # If the argument is month, we need show the time per week...
            if date == 'month':
               
                ax.set_xlabel('Events per week')
                
                # Set the format of the values of X
                ax.xaxis.set_major_locator(matplotlib.dates.WeekdayLocator(1,1))
                ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("Week '%W"))
                
                # Limit the display for 30 days
                ax.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=29), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            # If the argument is year, we need show the time per month...
            if date == 'year':
                
                ax.set_xlabel('Events per month')
                
                # Set the format of the values of X
                ax.xaxis.set_major_locator(matplotlib.dates.MonthLocator(range(1,13), bymonthday=1, interval=1))
                ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%b '%y"))
                
                # Limit the display for 365 days
                ax.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=364), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            matplotlib.pyplot.grid(True)
            
            # Define the grid options
            matplotlib.pyplot.grid(alpha=0.5, color='black', linestyle='-', linewidth=0.3)
            
            # Let's format the x values, to avoid overlaps
            fig.autofmt_xdate()
            
            # Save the picture into a tempory file, in /tmp
            matplotlib.pyplot.savefig(temp_img1.name)
            
            # Call the creation of a figure
            fig2 = matplotlib.pyplot.figure()
            ax2 = fig2.add_subplot(111)
            
            # Define the type of plot with the x and y values, with time and values
            ax2.bar(records['date'], records['cpu_time'])
            
            # The plot options
            matplotlib.pyplot.title('CPU Time for ' + project.upper() + ' project(s) last '+date)
            ax2.set_ylabel('CPU Time (second)')
            
            # If the argument is month, we need show the time per week...
            if date == 'month':
               
                ax2.set_xlabel('Events per week')
                
                # Set the format of the values of X
                ax2.xaxis.set_major_locator(matplotlib.dates.WeekdayLocator(1,1))
                ax2.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("Week '%W"))
                
                # Limit the display for 30 days
                ax2.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=29), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            # If the argument is year, we need show the time per month...
            if date == 'year':
                
                ax2.set_xlabel('Events per month')
                
                # Set the format of the values of X
                ax2.xaxis.set_major_locator(matplotlib.dates.MonthLocator(range(1,13), bymonthday=1, interval=1))
                ax2.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%b '%y"))
                
                # Limit the display for 365 days
                ax2.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=364), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            matplotlib.pyplot.grid(True)
            
            # Define the grid options
            matplotlib.pyplot.grid(alpha=0.5, color='black', linestyle='-', linewidth=0.3)
            
            # Let's format the x values, to avoid overlaps
            fig2.autofmt_xdate()
            
            # Save the picture into a tempory file, in /tmp
            matplotlib.pyplot.savefig(temp_img2.name)
            
        # In this case just one graphic, for the number of events
        if event in self.list_event_no_cpu:
            # Call the creation of a figure
            fig = matplotlib.pyplot.figure()
            ax = fig.add_subplot(111)
            
            # Define the type of plot with the x and y values, with time and values
            ax.bar(records['date'], records['nb_events'])
            
            # The plot options
            matplotlib.pyplot.title(event.upper()+' events for ' + project.upper() + ' project(s) last '+date)
            ax.set_ylabel('number of '+event.upper()+' events')
            
            # If the argument is month, we need show the time per week...
            if date == 'month':
               
                ax.set_xlabel('Events per week')
                
                # Set the format of the values of X
                ax.xaxis.set_major_locator(matplotlib.dates.WeekdayLocator(1,1))
                ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("Week '%W"))
                
                # Limit the display for 30 days
                ax.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=29), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            # If the argument is year, we need show the time per month...
            if date == 'year':
                
                ax.set_xlabel('Events per month')
                
                # Set the format of the values of X
                ax.xaxis.set_major_locator(matplotlib.dates.MonthLocator(range(1,13), bymonthday=1, interval=1))
                ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%b '%y"))
                
                # Limit the display for 365 days
                ax.set_xlim(records['date'][len(records['date']) - 1] - datetime.timedelta(days=364), records['date'][len(records['date']) - 1] + datetime.timedelta(days=1))
            
            matplotlib.pyplot.grid(True)
            
            # Define the grid options
            matplotlib.pyplot.grid(alpha=0.5, color='black', linestyle='-', linewidth=0.3)
            
            # Let's format the x values, to avoid overlaps
            fig.autofmt_xdate()
            
            # Save the picture into a tempory file, in /tmp
            matplotlib.pyplot.savefig(temp_img1.name)
        
        
    
    
    def get_project_option(self, current_project):
        '''
            Use the list of all projects  and return a list with unique project id 
            for the html form, using the log_project[] and the selected project 
            given in argument.
        '''
        
        unique_project = ['ALL']
        # Filter the projects located in log_project[]
        for i in self.log_project:
            if unique_project.count(i) == 0 and not i == '-':
                unique_project.append(i)
        project_option = ''
        
        # ... then Generate the html code for it.
        for i in unique_project:
            if not i == current_project:
                project_option += "\t<option value='"+i+"'>"+i+"</option>\n"
            else:
                project_option += "\t<option value='"+i+"' selected>"+i+"</option>\n"
            
        return project_option
    
    def get_event_option(self, current_event):
        '''
            Return the list of events for the html form with the selected event 
            given in parameter.
        '''
        
        event_option = ''
        for i in self.list_event_option:
            if not i == current_event:
                event_option += "\t<option value='"+i+"'>"+i+"</option>\n"
            else:
                event_option += "\t<option value='"+i+"' selected>"+i+"</option>\n"
            
        return event_option 
        
    def get_date_option(self, current_date):
        '''
            Return the date form, with the selected option.
        '''
        if current_date == 'month':
            return "Month : <input type=radio name='date' value='month' checked/>   Year : <input type=radio name='date' value='year' />"
        if current_date == 'year':
            return "Month : <input type=radio name='date' value='month' />   Year : <input type=radio name='date' value='year' checked/>"
    
    def get_image_option(self, current_event, temp_img1, temp_img2):
        '''
            Return the picture block, with the good number of pictures, using event type.
        '''
        if current_event in self.list_event_cpu or current_event == 'ALL':
            return '<img src="get_graph?img='+temp_img1.name+'.png" width=50%><img src="get_graph?img='+temp_img2.name+'.png" width=50%>'
        elif current_event in self.list_event_no_cpu:
            return '<img src="get_graph?img='+temp_img1.name+'.png" width=50%>'
            
    def index(self, date='month', event='ALL', project='ALL'):
        '''
            This function correspond to the index page of the website.
            It creates graphical statistics by default, per months, all events,
            and all projects. And initialize the temporaly files.
        '''
        
        temp_img1 = tempfile.NamedTemporaryFile()
        temp_img2 = tempfile.NamedTemporaryFile()
        
        records =  r.generate_records(date, event, project)
        r.create_histograms(records, date, event, project, temp_img1, temp_img2)
        
        # Define the template options for Cheetah
        nameSpace = {'title': 'RNAspace - 0.Statistics', 
                     'date_option' : self.get_date_option(date),
                     'event_option': self.get_event_option(event), 
                     'project_option': self.get_project_option(project), 
                     'image_option': self.get_image_option(event, temp_img1, temp_img2),
                     'date_of_now' : datetime.date.today().strftime("%d %B, %Y")}
                   
        return str(Template(self.__class__.templateDef, searchList=[nameSpace]))
    index.exposed = True
    def get_graph(self, **params):
        '''
            Use this function in order to link pictures outside the staticdir.
            Here it will be used from /tmp
        '''
        return static.serve_file(params["img"], "application/x-download", "attachment", os.path.basename(params["img"]))
    get_graph.exposed = True
    
    
# Define the configuration and tell where to find the pictures of the plots
# And the the absolute path for the webserver
app_conf = {'/': {'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))},
            '/img': {'tools.staticdir.on'  : True,
                     'tools.staticdir.dir' : '../rnaspace/ui/ressource/img'},
            '/css': {'tools.staticdir.on'  : True,
                     'tools.staticdir.dir' : '../rnaspace/ui/ressource/css'},
                }

if __name__ == '__main__':
    '''
        Main function, we test if we gave one parameter and if this file exist.
    '''
    if len(sys.argv) == 2:
        if os.path.isfile(sys.argv[1]):
            r = report()
            r.read_log_file(sys.argv[1])
            # Call the main function as index page
            cherrypy.quickstart(r, config=app_conf)
        else:
            print "ERROR : The file doesn't exist"
    else:
        print "ERROR : not enough parameters, you must specify a log file in input."
