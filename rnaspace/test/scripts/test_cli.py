#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import unittest
import subprocess
import os
import tempfile

from rnaspace_testcase import rnaspace_testcase

from common import RNASPACE_DIR, TEST_DIR_NAME, OUTPUT_DIR_NAME, \
                   OUTPUT_FILE_SUFFIX, REFERENCE_DIR_NAME, \
                   REFERENCE_FILE_SUFFIX, RNASPACE_DIR, WORKSPACE_DIR, \
                   TEST_USER_DIR_NAME, TEST_PROJECT_NAME, SEQ_DIR_NAME, \
                   SEQ_FASTA_SUFFIX

from rnaspace.core.data_manager import data_manager

class test_rnaspace_cli (rnaspace_testcase):

    def test(self):
        self.dm = data_manager()
        self.testcase_name = 'rnaspace_cli'
        self.sequences_id = ['NC_000913']
        self.script = os.path.join(RNASPACE_DIR, 'rnaspace_cli.py')
        self.conf = os.path.join(os.path.dirname(__file__), 'test_cli.cfg')
        self.rnaspace_cfg = os.path.join(os.path.dirname(__file__), '..', '..',
                                         'cfg', 'rnaspace.cfg')
        self.predictors_cfg = os.path.join(os.path.dirname(__file__), '..', '..',
                                           'cfg', 'predictors')
        self.init_seq(self.sequences_id)
        self.launch()
        self.convert_compare_result('GFF')
        
    def launch(self):

        fconf = open(self.conf, 'r')
        conf = fconf.read()
        fconf.close()
        seq = []
        for seq_id in self.sequences_id:
            seq.append(os.path.join(WORKSPACE_DIR, TEST_USER_DIR_NAME,
                                    TEST_PROJECT_NAME, SEQ_DIR_NAME,
                                    seq_id + SEQ_FASTA_SUFFIX))

        output = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, OUTPUT_DIR_NAME,
                              self.testcase_name + OUTPUT_FILE_SUFFIX)

        if os.path.exists(output):
            os.remove(output)

        conf_temp = tempfile.NamedTemporaryFile(dir=self.dm.config.get("storage",
                                                                       "tmp_dir"),
                                                delete=False)

        conf_temp.write(conf % {"output_path": output,
                                "sequences_path": ','.join(s for s in seq)})
        conf_temp.close()
        
        cmd = "python %s -r %s -p %s %s" % (self.script,
                                            self.rnaspace_cfg,
                                            self.predictors_cfg,
                                            conf_temp.name)

        stdout = tempfile.NamedTemporaryFile(dir=self.dm.config.get("storage",
                                                                    "tmp_dir"))

        retcode = subprocess.call(cmd, shell=True, stdout=stdout,
                                  stderr=subprocess.STDOUT)

        stdout.close()
        os.remove(conf_temp.name)


    def convert_compare_result(self, type):
        """Export predictions in a file then compare it with a reference file."""
        
        # export predictions
        out_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, OUTPUT_DIR_NAME,
                                self.testcase_name + OUTPUT_FILE_SUFFIX)       
        # compare them
        ref_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, 
                                REFERENCE_DIR_NAME,
                                self.testcase_name + REFERENCE_FILE_SUFFIX)
        result = os.system("diff " + out_file + " " + ref_file + " > /dev/null")
        assert result == 0, 'incorrect predictions of ' + self.testcase_name
        # if OK then delete output file
        if result == 0:
            os.remove(out_file)


def suite_cli():
    test_cli_suite = unittest.TestSuite()
    test_cli_suite.addTest(test_rnaspace_cli('test'))
    return test_cli_suite


if __name__ == '__main__':
    unittest.main()
