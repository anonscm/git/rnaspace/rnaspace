#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import unittest

from rnaspace_testcase import rnaspace_testcase


class test_atypicalgc (rnaspace_testcase):
        
    def test(self):
        self.testcase_name = 'atypicalgc'
        self.sequences_id = ['NC_000913']
        predictor_name = 'atypicalgc'
        opts = {'F':'True','T':'all','W':101,'L':200}

        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')
     

class test_rnammer (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'rnammer'
        self.sequences_id = ['NC_000913']
        predictor_name = 'rnammer'
        opts = {'tsu':'True','ssu':'False','lsu':'False'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')


class test_trnascanse (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'trnascanse'
        self.sequences_id = ['NC_000913']
        predictor_name = 'trnascanse'
        opts = {'C':'False','D':'False','X':20,'L':116}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')


class test_darn (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'darn'
        self.sequences_id = ['NC_000913']
        predictor_name = 'darn'
        opts={'d' : 'RNaseP [B/A/E]'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')


class test_erpin (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'erpin'
        self.sequences_id = ['NC_000913']
        predictor_name = 'erpin'
        opts = {'training_set': 'Bacterial 5S rRNA [B]'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')


class test_infernal (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'infernal'
        self.sequences_id = ['NC_000913']
        predictor_name = 'infernal'
        opts = {'descriptor_set': 'TPP [RF00059]'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts)
        self.convert_compare_result('GFF')


class test_blast (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'blast'
        self.sequences_id = ['NC_000913']
        predictor_name = 'blast'
        opts = {'alignments':1, 'S': 'both', 'score': '2; -3; 5; 2', 'e': '0.001', 'W': '11', 'F': 'no'}
        p = {'db': 'testdb_1.0.fasta'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts, p=p)
        self.convert_compare_result('GFF')


class test_yass (rnaspace_testcase):
    
    def test(self):
        self.testcase_name = 'yass'
        self.sequences_id = ['NC_000913']
        predictor_name = 'yass'
        opts = {'alignments':1, 'c': 'double', 'e': 'no', 'C': '5,-4,-3,-4', 'p': '###-#@-##@##,###--#-#--#-###', 'ge': '-4', 'r': 'both', 'go': '-16', 'E': '0.001'}
        p = {'db': 'testdb_1.0.fasta'}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts, p=p)
        self.convert_compare_result('GFF')


class test_blast_rnaz (rnaspace_testcase):
    
    def test(self):

        self.testcase_name = 'blast_rnaz'
        self.sequences_id = [ 'NC_000913']
        predictor_name = 'comparative_analysis'
        opts={'conservation_soft': 'blast', 'inference_soft': 'RNAz', 'conservation_soft_opts': None, 'inference_soft_opts': None, 'aggregation_soft': 'CG-seq', 'aggregation_soft_opts': None}
        p={'species': ['Acholeplasma_laidlawii_PG_8A [1 species]']}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts, p)
        self.convert_compare_result('GFF')
        

class test_yass_carnac (rnaspace_testcase):
    
    def test(self):

        self.testcase_name = 'yass_carnac'
        self.sequences_id = [ 'NC_000913']
        predictor_name = 'comparative_analysis'
        opts={'conservation_soft': 'yass', 'inference_soft': 'caRNAc', 'conservation_soft_opts': None, 'inference_soft_opts': None, 'aggregation_soft': 'CG-seq', 'aggregation_soft_opts': None}
        p={'species': ['Anaplasma_marginale_St_Maries [1 species]']}
        self.init_seq(self.sequences_id)
        self.rnas = self.launch_predictor(predictor_name, opts, p)
        self.convert_compare_result('GFF')
        


def suite_predictors():
    test_predictors_suite = unittest.TestSuite()
    test_predictors_suite.addTest(test_atypicalgc('test'))
    test_predictors_suite.addTest(test_rnammer('test'))
    test_predictors_suite.addTest(test_trnascanse('test'))
    test_predictors_suite.addTest(test_darn('test'))
    test_predictors_suite.addTest(test_erpin('test'))
    test_predictors_suite.addTest(test_infernal('test'))
    test_predictors_suite.addTest(test_blast('test'))
    test_predictors_suite.addTest(test_yass('test'))
    test_predictors_suite.addTest(test_blast_rnaz('test'))
    test_predictors_suite.addTest(test_yass_carnac('test'))
    
    return test_predictors_suite
        
if __name__ == '__main__':
    unittest.main()



        
