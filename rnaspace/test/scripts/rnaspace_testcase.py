#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import unittest
from ConfigParser import ConfigParser


from common import RNASPACE_DIR, TEST_DIR_NAME, DATA_DIR_NAME, \
                   TEST_USER_DIR_NAME, WORKSPACE_DIR, SEQ_FASTA_SUFFIX, \
                   TEST_PROJECT_NAME, SEQ_DIR_NAME, SEQ_TXT_SUFFIX, \
                   PREDICTORS_CFG_DIR, PREDICTOR_CFG_SUFFIX, OUTPUT_DIR_NAME, \
                   TEST_RUN_NAME, OUTPUT_FILE_SUFFIX, REFERENCE_DIR_NAME, \
                   REFERENCE_FILE_SUFFIX
from rnaspace.core.sequence import sequence
from rnaspace.core.conversion.gff_converter import gff_converter
from rnaspace.core.conversion.csv_converter import csv_converter
from rnaspace.core.conversion.fasta_converter import fasta_converter


# Generic rnaspace test case
class rnaspace_testcase (unittest.TestCase):

    testcase_name = ''
    rnas = []       # list of predicted putative_rna objects
    sequences = []  # list of project sequence objects

        
    def setUp(self):
        """Copy test_user tree in workspace."""
        
        os.system(os.path.join("cp -r " + RNASPACE_DIR , 
                               TEST_DIR_NAME, DATA_DIR_NAME,
                               TEST_USER_DIR_NAME + " " + WORKSPACE_DIR))


    def tearDown(self):
        """Remove test_user tree in workspace."""
        
        os.system("rm -rf " +  os.path.join(WORKSPACE_DIR, 
                                            TEST_USER_DIR_NAME))
        

    def init_seq(self, seq_id_list):
        """Initialize sequences for the project."""

        self.sequences = []
        for seq_id in seq_id_list:
            # copy sequence information in workspace
            os.system(os.path.join("cp " + RNASPACE_DIR, TEST_DIR_NAME, 
                                   DATA_DIR_NAME, 
                                   seq_id + SEQ_FASTA_SUFFIX + " " + WORKSPACE_DIR,
                                   TEST_USER_DIR_NAME, TEST_PROJECT_NAME, 
                                   SEQ_DIR_NAME))
            os.system(os.path.join("cp " + RNASPACE_DIR , TEST_DIR_NAME, 
                                   DATA_DIR_NAME,
                                   seq_id + SEQ_TXT_SUFFIX + " " + WORKSPACE_DIR,
                                   TEST_USER_DIR_NAME, TEST_PROJECT_NAME, 
                                   SEQ_DIR_NAME))

            # load fasta file as object
            fasta_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, 
                                      DATA_DIR_NAME,
                                      seq_id + SEQ_FASTA_SUFFIX)
            fasta = open(fasta_file, 'r')
            data = fasta.read()
            fasta.close()        
            seq_data = ''
            for line in data.split('\n'):
                if not(line.startswith('>') or line ==''):
                    temp = line.upper()
                    temp = temp.replace(' ', '')
                    temp = temp.replace('\r', '')
                    seq_data += temp
            self.sequences.append(sequence(seq_id, seq_data, 
                                           domain=self.get_domain()))


    def launch_predictor(self,predictor_name, opts, p={}):
        """Launch a predictor on each sequence of the project."""
        
        if predictor_name != 'comparative_analysis':
            parser_predictor_cfg = ConfigParser()
            parser_predictor_cfg.read(
                os.path.join(PREDICTORS_CFG_DIR, 
                             predictor_name + PREDICTOR_CFG_SUFFIX))
            name = parser_predictor_cfg.get('General', 'name')[1:-1]
            type = parser_predictor_cfg.get('General', 'type')[1:-1]
            version = parser_predictor_cfg.get('General', 'version')[1:-1]
            exe = parser_predictor_cfg.get('General', 'exe_path')[1:-1]
        else:
            name =  opts['conservation_soft'] + "/" + opts['aggregation_soft'] \
                    + "/" + opts['inference_soft']
            type = 'comparative'
            version = ''
            exe = ''

        # call predictor on each sequence
        for s in self.sequences:
            stderr_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, 
                                       OUTPUT_DIR_NAME,
                                       self.testcase_name + ".stderr")
            stdout_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME,
                                       OUTPUT_DIR_NAME,
                                       self.testcase_name + ".stdout")
            mod = __import__(predictor_name)
            predictor = getattr(mod, predictor_name) (opts, s, TEST_USER_DIR_NAME,
                                                      TEST_PROJECT_NAME, TEST_RUN_NAME, p,
                                                      stderr_file, stdout_file,
                                                      name, type, '', version, exe)
            predictor.run()
            if os.path.exists(stderr_file): os.system ("rm " + stderr_file)
            if os.path.exists(stdout_file): os.system ("rm " + stdout_file)
            
        return predictor.dm.get_putative_rnas(TEST_USER_DIR_NAME,
                                              TEST_PROJECT_NAME,
                                              run_id=TEST_RUN_NAME)
    

    def convert_compare_result(self, type):
        """Export predictions in a file then compare it with a reference file."""
        
        # export predictions
        out_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, OUTPUT_DIR_NAME,
                                self.testcase_name + OUTPUT_FILE_SUFFIX)
        # rm out_file if exists
        if os.path.exists(out_file): os.system("rm " + out_file)
        if type=='GFF':
            converter = gff_converter()
        elif type=='CSV':
            converter = csv_converter()
        else:
            converter = fasta_converter()
        converter.write(self.rnas, out_file)
       
        # compare them
        ref_file = os.path.join(RNASPACE_DIR, TEST_DIR_NAME, 
                                REFERENCE_DIR_NAME,
                                self.testcase_name + REFERENCE_FILE_SUFFIX)
        result = os.system("diff " + out_file + " " + ref_file + " > /dev/null")
        assert result == 0, 'incorrect predictions of ' + self.testcase_name
        # if OK then delete output file
        if result == 0:
            os.system ("rm " + out_file)


    def get_domain(seq_id):
        """Return associated domain of a test seq_id."""

        # seq_id file is not readable with ConfigParser
        # so instead of a heavy code do this
        if seq_id == 'NC_003076':
            domain = 'eukaryote'
        else:
            domain = 'bacteria'
        return domain 
