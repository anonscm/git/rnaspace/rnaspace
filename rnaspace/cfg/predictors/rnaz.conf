[General]
name = "RNAz"
path = "rnaz.py"
exe_path = "RNAz-1.0/rnaz/RNAz"
speed = ""
version = "2.0.9"
type = "inference"
description: "<b>RNAz</b> predicts structurally conserved and thermodynamically
	      stable RNA structures in multiple sequence alignments."

[Tools]
rnazwindow = "/media/Stock/rnaspace_softwares/RNAz-1.0/perl/rnazWindow.pl"
clustalw = "clustalw"

[Options]
simple = ""
long = "p,slice,window,step"
basic_options = ""

[Options_Params]
p = "<float>"
slice = "<int>"
window = "<int>"
step = "<int>"

[Options_default_params]
default_simple = ""
p = "0.7"
slice = "300"
window = "200"
step = "50"

[Options_view]
p = "text area"
slice = "text area"
window = "text area"
step = "text area"

[Options_desc]
p = "Probability cutoff"
slice = "Slice alignments longer than"
window = "Window size"
step = "Step Size"


[Help]
text: "<div class="help_soft_title">Approach</div>

  RNAz combines comparative sequence analysis and structure
  prediction. It consists of two basic components: (i) a measure for
  RNA secondary structure conservation based on computing a consensus
  secondary structure, and (ii) a measure for thermodynamic stability,
  which, in the spirit of a Z-score, is normalized with respect to
  both sequence length and base composition. The two independent
  diagnostic features of structural ncRNAs are
  finally used to classify an alignment as "structural RNA" or
  "other". For this purpose, RNAz uses a support vector machine (SVM)
  learning algorithm which is trained on a large test set of well
  known ncRNAs.
  <br />
  <br />
  RNAz predicts a consensus secondary structure for an alignment by
  using the RNAalifold approach.  RNAalifold works almost exactly as
  single sequence folding algorithms (e.g. RNAfold), with the main
  difference that the energy model is augmented by covariance
  information. Compensatory mutations (e.g. a CG pair mutates to a UA
  pair) and consistent mutations (e.g. AU mutates to GU) give a
  "bonus" energy while inconsistent mutations (e.g. CG mutates to CA)
  yield a penalty.
  <br />
  <br />
  RNAz requires that the data  to analyse is given in a multiple sequence alignment. 
  In RNAspace, the multiple alignment is build with <b>ClustalW</b>, using
  default parameter values for nucleic acids sequences. <br />
  <br />
  RNAspace currently uses version 2.0.9. <br />
  <br />

  <div class="help_soft_title">Parameters</div>

  <b>Probability cut off:</b> This is the probability of the sequence
  of being a ncRNA, as computed by the SVM. Results are
   displayed only for sequences having a classification probability
  higher than this value.  Sequences with high probablity cut off
  (e.g.>0.9) show strong evidence for a structural RNA.  Default
  value is 0.7.<br />
  <br />

  <b>Slice alignments longer than</b>, <b>Window size</b> and <b>Step size</b> options:
    The RNAz algorithm works globally, i.e. the given alignment is
    scored as a whole.  For long alignments (e.g alignment of a whole
    chromosome), this is neither computationally tractable nor
    biologically meaningful. Therefore, long alignments are scanned in
    overlapping windows. Alignments longer than the slice size will be
    analyzed in sliding overlapping windows of the given window
    size. The step size value specifies the number of shifted
    positions. In RNAspace, The default values are 300, 200 and 50
    respectively.<br />
  <br />


   <div class="help_soft_title">Reference</div>

   Washietl S., Hofacker I.L., Stadler P.F.
   Fast and reliable prediction of noncoding RNAs
   Proc. Natl. Acad. Sci. U.S.A. 102, 2454-2459,  2005. <br />
   <br />
   <a href="http://www.tbi.univie.ac.at/~wash/RNAz/">RNAz web site</a>
   " 
