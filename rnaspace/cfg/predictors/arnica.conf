[General]
name = "caRNAc"
path = "arnica.py"
exe_path = "arnica-0.31/src/arnica"
speed = ""
version = "0.31"
type = "inference"
description: "<b>caRNAc</b> predicts structurally conserved and 
	     thermodynamically stable RNA structures. It can successfully 
	     handle low similarity sequences."

[Tools]
gardenia = "gardenia/src/gardenia"

[Options]
simple = ""
long = "A"
basic_options = ""

[Options_Params]
A = "yes,no"

[Options_default_params]
default_simple = ""
A = "yes"

[Options_view]
A="radio"

[Options_desc]
A = "Take GC percent into account"

[Help]
text: "<div class="help_soft_title">Approach</div>
       caRNAc combines three features: energy minimization,
       phylogenetic comparison and sequence conservation.  It 
       relies on a Sankoff-like folding algorithm that simultaneously infers
       a potential consensus structure and align the sequences. As
       a consequence, the method does not require
       any prior multiple sequence alignment, and appears to be robust
       to noisy datasets. <br />
       <br />
       RNAspace currently uses version 0.31.<br />
       <br />
      
      <div class="help_soft_title">Parameters</div>
       
      <b>Take GC percent into account:</b>
      When this option is selected, caRNAc uses variable energy
      thresholds for helices selection according to the average GC percent 
      of the involved sequence.<br />
       <br />

      <div class="help_soft_title">Reference</div>
      caRNAc: folding families of non coding RNAs
      Touzet H. and Perriquet O.
      Nucleic Acids Research 142, 2004. <br />
      <br />
      <a href="http://bioinfo.lifl.fr/RNA/carnac/index.php">caRNAc web site</a> 
      "
