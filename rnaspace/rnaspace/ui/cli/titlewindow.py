#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses

from rnaspace.ui.cli.window import window

class titleWindow(window):
    """ Window object for content that is divided into sections.

    This window also provides a scrolling facility.
    """

    
    def __init__(self, stdscr, y, x, h, w, border=False, paddingX=1,
                 paddingY=1, content=""):
        """
        y, x: positions of the window
        h, w: height and width
        paddingX, paddingY: number of spaces added before the content of the
        window.

        If border is True, the window border will be displayed
        """
        window.__init__(self, stdscr, y, x, h, w, border, paddingX, paddingY,
                        content)
        self.formatted_content = None
        self.beginY = 0
        self.selected_line = 0


    def scroll_down(self, step=1):
        """Scroll the page down by one line.

        Step is ignored here.
        """
        if self.selected_line < len(self.formatted_content) - 1:
            self.selected_line += 1
        
            if (self.selected_line >= self.beginY + self.max_height and
                self.beginY < len(self.formatted_content) - 1):
                self.beginY += 1
            
        self.update()
        
    def scroll_up(self, step=1):
        """Scroll the page up by one line.

        Step is ignored here.
        """

        if self.selected_line > 0:
            self.selected_line -= 1
        
            if self.selected_line < self.beginY and self.beginY > 0:
                self.beginY -= 1
            
        self.update()

    def scroll_top(self):
        """Move at the top of the page."""
        
        self.beginY = 0
        self.selected_line = 0
        self.update()

    def scroll_bottom(self):
        """Move at the bottom of the page."""
        
        self.beginY = len(self.formatted_content) - 1
        self.selected_line = self.beginY
        self.update()

    def set_content(self, new_content):
        
        self.content = new_content
        self.format_content()
        self.beginY = 0
        self.selected_line = 0
        self.update()
    
    def update(self):
        self.win.clear()

        if self.formatted_content is None:
            self.format_content()
        
        # get wich lines of content we have to display.
        endY = self.beginY + min(self.max_height,
                                 len(self.formatted_content[self.beginY:]))

        if self.border:
            self.win.border()

            
        for (i, item) in enumerate(self.formatted_content[self.beginY:endY]):

            line = item[0]
            color = item[1]
            if i + self.beginY == self.selected_line:
                color = curses.A_REVERSE

            y = self.paddingY + i
            x = self.paddingX

            if self.border:
                x += 1
                y += 1                    

            self.win.addstr(y, x, line, color)
                
        self.win.noutrefresh()

    def get_selection(self):
        """Return the selected line if it is selectable. Else return None."""

        if self.formatted_content[self.selected_line][3]:
            selection = self.formatted_content[self.selected_line][0]
            # remove the indentation spaces
            return selection.strip()

        return None



    def get_line(self, line, indent_level, is_selectable=True):
        """Format the line with the correct indentation. """
        
        indented_line = indent_level * 2 * " " + line
        return (indented_line, curses.A_NORMAL, indent_level, is_selectable)

    def get_title(self, line, indent_level, is_selectable=True):
        indented_line = indent_level * 2 * " " + "* " + line
        return (indented_line, curses.A_BOLD, indent_level, is_selectable)

    def get_subtitle(self, line, indent_level, is_selectable=True):
        indented_line = indent_level * 2 * " " + "- " + line
        return (indented_line, curses.A_NORMAL, indent_level, is_selectable)

    def format_content(self):
        """
        Split the content of the window in several line according to the
        width of the window. The splitted content is stored in
        self.formatted_content.

        Each item of formatted_content is a tuple:
           - the line
           - its color
           - the indent level
           - is it a selectable line ?
        """
        
            
        self.formatted_content = []

        for title in self.content:
            self.formatted_content.append(self.get_title(title, 0, False))
            try:                
                subtitles = self.content[title].keys()
            except AttributeError:
                # no subtitles
                self.formatted_content.append((' ', curses.A_NORMAL, 0,
                                                False))
                for item in self.content[title]:
                    self.formatted_content.append(self.get_line(item, 1))
            else:
                # there are subtitles
                self.formatted_content.append((' ', curses.A_NORMAL, 0,
                                                False))
                for subtitle in subtitles:
                    self.formatted_content.append(self.get_subtitle(subtitle,
                                                                      1, False))
                    self.formatted_content.append((' ', curses.A_NORMAL, 0,
                                                   False))
                    for item in self.content[title][subtitle]:
                        self.formatted_content.append(self.get_line(item, 2))
                    self.formatted_content.append((' ', curses.A_NORMAL, 0,
                                                   False))
            self.formatted_content.append((' ', curses.A_NORMAL, 0, False))
            
        del self.formatted_content[-1]
