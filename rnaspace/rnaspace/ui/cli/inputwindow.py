#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses
from rnaspace.ui.cli.window import window

class inputWindow(window):
    """A window that deals with the key events.

    This window is not displayed.
    """

    def __init__(self, stdscr, y, x, h, w):
        window.__init__(self, stdscr, y, x, h, w)
        
        # allow curses to deal with special keys.
        self.win.keypad(1)
        
    def getch(self):
        """Wait for user input and return the key."""
        
        # wait for user input => blocking call.
        key = self.win.getch()
        return key
