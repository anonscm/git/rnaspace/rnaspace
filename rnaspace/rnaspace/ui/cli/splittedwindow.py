#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import curses
from rnaspace.ui.cli.window import window

class splittedWindow(window):
    """Window splitted into two parts: left and right."""
    
    def __init__(self, stdscr, y, x, h, w, left, right, border=False, paddingX=1,
                 paddingY=1):
        window.__init__(self, stdscr, y, x, h, w, border, paddingX, paddingY)

        self.middle = int(self.max_width/2)
        self.left = left
        self.right = right
        self.__build_content()

    def set_left_content(self, leftpart):
        self.left = leftpart
        self.__build_content()

    def set_right_content(self, rightpart):
        self.right = rightpart
        self.__build_content()

    def __build_content(self):
        endleft = min(self.middle-1, len(self.left))
        endright = min(len(self.right), self.max_width - self.middle - 2)
        self.content = self.left[:endleft]
        self.content += (self.middle-1 - endleft) * ' ' + '| '
        self.content += (self.max_width - self.middle - 2 - endright) * ' '
        self.content += self.right[:endright]
