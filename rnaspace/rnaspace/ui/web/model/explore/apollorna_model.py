#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import tempfile
import zipfile
import os

from rnaspace.core.data_manager import data_manager
from rnaspace.core.conversion.apollo_gff_converter import apollo_gff_converter

class apollorna_model(object):
    
    def __init__(self):        
        self.data_manager = data_manager()

    def user_has_data(self, user_id):
        """ Return True if the user has data, false else 
            user_id(type:string)   the user id
        """
        return self.data_manager.user_has_data(user_id)
                    
    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_manager.get_ids_from_authkey(id)

    def get_authkey(self, user_id, project_id):
        """
        user_id(sting)      the user_id
        project_id(string)  the project_id
        return the id
        """
        return self.data_manager.get_authkey(user_id, project_id)

    def get_mount_point(self):
        return self.data_manager.get_mount_point()

    def get_putative_rnas(self, user_id, project_id, params):
        """ Return a list of putative_rna 
            user_id(type:string)      the user id
            project_id(type:string)   project id the user is working on
            params(type:{})         the dictionary of all the parameters
        """
        result_table = []

        if params.has_key("nb_putative_rnas"):
            rnas = self.data_manager.get_putative_rnas(user_id, project_id)
            for i in range(int(params["nb_putative_rnas"])):
                param_name = "putative_rna" + str(i)
                for rna in rnas:    
                    if rna.sys_id == params[param_name]:
                        result_table.append(rna)
        return result_table

    def get_action(self, params):
        """ Return the action
            params(type:{})     the dictionary of parameters
        """
        if params.has_key("action"):
            return params["action"]
        else:
            return None 

    def get_zippath(self, user_id, project_id, params):
        prnas = self.get_putative_rnas(user_id, project_id, params)
        gsid = ''
        if len(prnas) > 0:
            gsid = prnas[0].genomic_sequence_id
            for prna in prnas[1:]:
                if prna.genomic_sequence_id != gsid:
                    return (True, 'Predictions are not on the same sequence')
        else:
            return (True, 'No predictions selected')
        
        tmp_dir = self.data_manager.config.get("storage","tmp_dir")
        tmp_gff = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_zip = tempfile.NamedTemporaryFile(dir=tmp_dir).name + '.zip'
        seq_path = self.data_manager.get_sequence_file_path(user_id,
                                                            project_id,
                                                            gsid)
        gff_name = os.path.basename(seq_path).split('.')
        gff_name = gff_name[0] + '.gff'

        # convert prnas into gff
        converter = apollo_gff_converter()
        converter.write(prnas, tmp_gff)

        archive = zipfile.ZipFile(tmp_zip, mode='w')
        archive.write(tmp_gff, gff_name)
        archive.write(seq_path, os.path.basename(seq_path))
        archive.close()
        return (False, tmp_zip)

