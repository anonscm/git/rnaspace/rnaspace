#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import tempfile
import StringIO
import re
import pickle

from rnaspace.core.exceptions import disk_error
from rnaspace.core.data_manager import data_manager
from rnaspace.core.id_tools import id_tools
from rnaspace.core.sequence import sequence
from rnaspace.core.trace.event import add_seq_event
from rnaspace.core.trace.event import disk_error_event
import rnaspace.core.common as common_core


class manage_model(object):
    """ Class manage_model: the model of the manage page
    """

    available_domain = ["bacteria", "eukaryote", "archaea"]

    def __init__(self):
        """ Build a manage_model object defined by    
            data_manager(type:core.data_manager)   the application data manager  
        """
        self.data_manager = data_manager()
        self.mask_params_dir_name = "mask_params"
     
    def has_mask_state(self, user_id, project_id):
        """ check if a file with masking parameters exists

        user_id(string)       the id of the connected user
        project_id(string)    the id of the projec
        """
        tmp_dir = self.data_manager.get_config_value("storage","tmp_dir")
        mask_dir = os.path.join(tmp_dir, self.mask_params_dir_name)
        path = os.path.join(mask_dir,user_id+"-"+project_id+".pkl")
        
        if os.path.exists(path):
            return True
        else :
            return False
        
    def get_mask_state(self, user_id, project_id):
        """ open a file with the masking parameters 
        and check which parameters are present
        
        user_id(string)       the id of the connected user
        project_id(string)    the id of the projec
        """
        tmp_dir = self.data_manager.get_config_value("storage","tmp_dir")
        mask_dir = os.path.join(tmp_dir, self.mask_params_dir_name)
        path = os.path.join(mask_dir,user_id+"-"+project_id+".pkl")        
        mparams_file = open(path,'rb')        
        mparams = []
        for i in range(len(self.get_project_sequences(user_id,project_id))):
            data = pickle.load(mparams_file)
            annot = 0
            trans = 0
            if not data[1] == "":
                annot = 1
            if not data[3] == "":
                trans = 1
            mparams.append({ "annotations" : annot,
                             "transcripts" : trans,
                             "mdust" : int(data[8]),
                             "repeatmasker" : int(data[9]) })
        mparams_file.close()
                             
        return mparams
            
    def add_mask_params(self, user_id, project_id, params):
        """ create a file with the masking parameters

        user_id(string)       the id of the connected user
        project_id(string)    the id of the projec
        params(dictionnary)   the masking options chosen by the user
        """
        fseq = self.__get_sequences(params)
        
        if not self.valid_fasta_sequence(fseq):
            raise ImportError, 'Require sequence in FASTA format (a comment line beginning with >, followed by the genomic sequence) with limitation on nucleotides: ONLY A, U, T, G, C, N letters in either upper or lower case.'
        
        fseq.seek(0)
        
        count = 0
        for line in fseq:
            if line.startswith('>'):
                count += 1
        if count >= self.data_manager.get_nb_sequences_limitation() - 1:
            raise ImportError, 'Too many sequences into your fasta file ! Maximum allowed is '  + str(self.data_manager.get_nb_sequences_limitation()) + ' per fasta file.'
        
        annot_path = self.check_file_annotation(params["annotation_file"], params["domain"])        
        transcript_path = self.check_file_transcript(params["transcript_file"])        
        mdust_run = 0
        if params.has_key("mdust_run"):
            mdust_run = 1
        rm_run = 0
        if params.has_key("repeatmasker_run"):
            rm_run = 1
                   
        tmp_dir = self.data_manager.get_config_value("storage","tmp_dir")        
        mask_dir = os.path.join(tmp_dir, self.mask_params_dir_name)
        if not os.path.isdir(mask_dir) :
            os.mkdir(mask_dir)            
        path = os.path.join(mask_dir,user_id+"-"+project_id+".pkl")       
         
        mparams_file = open(path, 'ab')        

        if count == 1 :
            mparams = [params["id"],
                       annot_path,
                       params["annotation_x"],
                       transcript_path[0],
                       transcript_path[1],
                       params["transcript_x"],
                       params["transcript_p"],
                       params["transcript_w"],
                       str(mdust_run),
                       str(rm_run)]
            pickle.dump(mparams, mparams_file) 
        else :
            for i in range(count):
                mparams = [params["id"]+"."+str(i+1),
                           annot_path,
                           params["annotation_x"],
                           transcript_path[0],
                           transcript_path[1],
                           params["transcript_x"],
                           params["transcript_p"],
                           params["transcript_w"],
                           str(mdust_run),
                           str(rm_run)]
                pickle.dump(mparams, mparams_file)        
        
        mparams_file.close()
        
        fseq.seek(0)
        
        return fseq
        
            
    def check_file_annotation(self, annotation_file, domain):
        """ check if the annotation file is usable
        
        return: the path to the file 
        """
        if not annotation_file.filename == "":
            abs_path = ''
            tmp_dir = self.data_manager.get_config_value("storage","tmp_dir")
            os_temp, abs_path = tempfile.mkstemp(dir=tmp_dir)
            temp = open(abs_path, 'a')
            data = annotation_file.file.read()
            max_len = self.data_manager.get_config_value("global", "max_size_mask_file")
            if max_len[-2:] != 'Mb':
                raise ImportError, "The parameter max_size_mask_file in cherrypy.cfg file must be expressed in Mb"
            if len(data) > int(max_len[:-2])*1024*1024:
                temp.close()
                os.remove(abs_path)
                raise ImportError,"The annotation file is too long. Must be less than "+max_len
            temp.write(data)            
            temp.close()
            
           # type = self.get_format_annotation(abs_path)
            (valid, mess) = self.validate_annotation(abs_path, domain)
            if not valid:
                raise ImportError, mess
           
            return abs_path
        else :
            return ""

    def get_format_annotation(self, annotation_file):
        """ detect the format of the annotation file
        
        return: the format detected (string)
        """
        fannot = open(annotation_file, 'r')
        line = fannot.readline()
        if line.startswith("##gff-version 3"):
            type = "gff3"
        else:
            type = "gff2"
           
            fannot.close()
        return type

    def validate_annotation(self, abs_path, domain):
        """ validate the format of the annotation file
        
        return a tuple (bool, mess). bool is false if the file is 
        not valid.
        """
        type = self.get_format_annotation(abs_path)
        fannot = open(abs_path, 'r')
        annos = []
        data_absent = True
        line = fannot.readline()
                
        if type == "gff2":
            if (line.count("\t") > 6):
                if domain != "eukaryote":
                    if line.split("\t")[2] == "CDS":
                        if self.__check_valid_annotations(line):
                            data_absent = False
                        else :
                            os.remove(abs_path)
                            return (False, "Please check the strand value and if start is lower than stop in the given GFF file.")
                else:
                    if line.split("\t")[2] == "exon":
                        if self.__check_valid_annotations(line):
                            data_absent = False
                        else :
                            os.remove(abs_path)
                            return(False, "Please check the strand value and if start is lower than stop in the given GFF file.")
            else:
                os.remove(abs_path)
                return (False, 'GFF2 file must have at least 8 columns separated by tabs.')
                                
        for line in fannot :
            if type == "gff3":
                if line.startswith("##FASTA"):
                    break
                if not line.startswith("#"):
                    if (line.count("\t") > 7):
                        temp = line
                        temp = temp.replace("\r","")
                        annos.append(temp)
                        if domain != "eukaryote":
                            if temp.split("\t")[2] == "CDS":
                                if self.__check_valid_annotations(temp):
                                    data_absent = False
                                else :
                                    os.remove(abs_path)
                                    return (False, "Please check the strand value and if start is lower than stop in the given GFF file.")
                        else:
                            if temp.split("\t")[2] == "exon":
                                if self.__check_valid_annotations(temp):
                                    data_absent = False
                                else :
                                    os.remove(abs_path)
                                    return (False, "Please check the strand value and if start is lower than stop in the given GFF file.")
                    elif (line != "\n"):
                        os.remove(abs_path)
                        return (False, 'GFF3 file must have 9 columns separated by tabs.')
                            
            else :
                if not line.startswith("#"):
                    if (line.count("\t") > 6):
                        temp = line
                        temp = temp.replace("\r","")
                        annos.append(temp)
                        if domain != "eukaryote":
                            if temp.split("\t")[2] == "CDS":
                                if self.__check_valid_annotations(temp):
                                    data_absent = False
                                else :
                                    os.remove(abs_path)
                                    return (False, "Please check the strand value and if the start < stop")
                        else:
                            if temp.split("\t")[2] == "exon":
                                if self.__check_valid_annotations(temp):
                                    data_absent = False
                                else :
                                    os.remove(abs_path)
                                    return (False, "The data are wrong, please check if start is lower than stop in the given GFF file.")
                    elif (line != "\n"):
                        os.remove(abs_path)
                        return (False, 'GFF2 file must have at least 8 columns separated by tabs.')
            
        fannot.close()
        if data_absent:
            os.remove(abs_path)
            return (False, 'The annotation file do not contain any CDS for bacteria/archeae or exon for eukaryote.')

        return (True, "")
        
    def check_file_transcript(self, transcript_file):
        """ check if the transcript file is usable

        return: the path to the file and its format
        """
        if not transcript_file.filename == "":
            abs_path = ''
            tmp_dir = self.data_manager.get_config_value("storage","tmp_dir")
            os_temp, abs_path = tempfile.mkstemp(dir=tmp_dir)
            temp = open(abs_path, 'a')
            data = transcript_file.file.read()
            max_len = self.data_manager.get_config_value("global", "max_size_mask_file")
            if max_len[-2:] != 'Mb':
                raise ImportError, "The parameter max_size_mask_file in cherrypy.cfg file must be expressed in Mb"
            if len(data) > int(max_len[:-2])*1024*1024:
                temp.close()
                os.remove(abs_path)
                raise ImportError,"The transcript file is too long. Must be less than "+max_len
            temp.write(data)            
            temp.close()
            
            type = self.get_format_transcript(abs_path)
            (valid, mess) = self.validate_transcript(abs_path, type)
            if not valid:
                raise ImportError, mess

            if type == "fastq":
                fasta = self.convert_fastq_fasta(abs_path)
                fasta_file = open(abs_path, 'w')
                fasta_file.write(fasta)
                fasta_file.close()
                return [abs_path,"fasta"]
            elif type == "fasta":
                 return [abs_path,"fasta"]
            elif type == "gff2" or type == "gff3":
                return [abs_path,"gff"]
        else:
            return ["",""]

    def get_format_transcript(self, transcript_file):
        """ detect the format of the transcript file
        """
        ftrans = open(transcript_file, 'r')
        line = ftrans.readline()

        if line.startswith("@"): # It's a FASTQ file
            type = "fastq"
        elif line.startswith(">"): # It's a FASTA file
            type = "fasta"
        elif line.startswith("##gff-version 3"): # It's a GFF3 file
            type = "gff3"
        else: #It's a GFF2 file
            type = "gff2"

        return type
        ftrans.close()
    
    def check_fastq_format(self, abs_path):
        """ check the format of a fastq file
        
        return a tuple (bool, mess). bool is false if the file 
        is not valid
        """
        ftrans = open(abs_path, 'r')
        line = ftrans.readline()

        line.replace("\r","")
        rnaseq = []
        rnaseq.append(line)
        rnaseq.append(ftrans.readline().replace("\r",""))
        rnaseq.append(ftrans.readline().replace("\r",""))
        rnaseq.append(ftrans.readline().replace("\r",""))
        if not self.__check_valid_rnaseq(rnaseq):
            ftrans.close()
            return (False, "The format of the RNAseq file is wrong")
                    
        for line in ftrans :
            line = line.replace("\r","")
                    
            rnaseq.append(line)
            if len(rnaseq) == 4:
                if not self.__check_valid_rnaseq(rnaseq):
                    ftrans.close()
                    return (False, "The format of the RNAseq file is not satisfied.")
        
        ftrans.close()
        return (True, "")
    
    def convert_fastq_fasta(self, abs_path):
        """ convert a fastq file into a fasta file

        return: the content of the modified fasta file 
        as a string.
        """
        ftrans = open(abs_path, 'r')

        line = ftrans.readline()

        line.replace("\r","")
        fasta = ""
        rnaseq = []
        rnaseq.append(line)
        rnaseq.append(ftrans.readline().replace("\r",""))
        rnaseq.append(ftrans.readline().replace("\r",""))
        rnaseq.append(ftrans.readline().replace("\r",""))
        fasta += rnaseq[0].replace("@", ">") + rnaseq[1]
        rnaseq = []
        
                    
        for line in ftrans :
            line = line.replace("\r","")
                    
            rnaseq.append(line)
            if len(rnaseq) == 4:
                fasta += rnaseq[0].replace("@", ">") + rnaseq[1]
                rnaseq = []
                
        ftrans.close()
        return fasta

    def validate_transcript(self, abs_path, type):
        """ validate the format of the transcript file

        return a tuple (bool, mess). bool is false if the file is 
        not valid.  
        """
    
        if type == "fastq":
            (valid, mess) = self.check_fastq_format(abs_path)
            if not valid:
                ftrans.close()
                return (False, mess)

        elif type == "fasta":
            ftrans = open(abs_path, 'r')
            line = ftrans.readline()
            line.replace("\r","")
            fasta_txt = ""
            fasta = []
            fasta.append(line)
            fasta.append(ftrans.readline().replace("\r",""))
            if self.valid_fasta_sequence(fasta):
                fasta_txt += fasta[0] + fasta[1]
                fasta = []
            else:
                os.remove(abs_path)
                return (False, "The format of the RNAseq file is not satisfied")
                    
            for line in ftrans:
                line = line.replace("\r","")
                fasta.append(line)
                    
                if len(fasta) == 2:
                    if self.valid_fasta_sequence(fasta):
                        fasta_txt += fasta[0] + fasta[1]
                        fasta = []
                    else :
                        os.remove(abs_path)
                        return (False, "The format of the RNAseq file is not satisfied")
                            
            ftrans.close()
            
        elif type == "gff3":
            ftrans = open(abs_path, 'r')
            line = ftrans.readline()
            for line in ftrans :
                if line.startswith("##FASTA"):
                    break
                if not line.startswith("#"):
                    if (line.count("\t") > 7):
                        line = line.replace("\r","")
                        if self.__check_valid_annotations(line):
                            pass
                        else :
                            os.remove(abs_path)
                            return (False, "Please check the strand value and if start is lower than stop in the given GFF file.")
                    elif (line != "\n"):
                        os.remove(abs_path)
                        return (False, 'GFF3 file must have 9 columns separated by tabs.')
                 
        elif type == "gff2":
            ftrans = open(abs_path, 'r')
            line = ftrans.readline()
            if not line.startswith("#"):
                if (line.count("\t") > 6):
                    line = line.replace("\r","")
                    if self.__check_valid_annotations(line):
                        pass
                    else :
                        os.remove(abs_path)
                        return (False, "please check the strand value and if start is lower than stop in the given GFF file.")
                elif (line != "\n"):
                    os.remove(abs_path)
                    return (False, 'GFF2 file must have at least 8 columns separated by tabs.')
                            
            for line in ftrans :
                if not line.startswith("#"):
                    if (line.count("\t") > 6):
                        line = line.replace("\r","")
                        if self.__check_valid_annotations(line):
                            pass
                        else :
                            os.remove(abs_path)
                            return (False, "Please check the strand value and if start is lower than stop in the given GFF file.")
                    elif (line != "\n"):
                        os.remove(abs_path)
                        return (False, 'GFF2 file must have at least 8 columns separated by tabs.')
            
        return (True, "")



    def add_sequences(self, user_id, project_id, fseq, params):
        """
        Save the user sequences return 1 if everything ok, 0 otherwise.

        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        fseq(file object)     the sequence we want to save
        params({})            the infos of the sequence
        """
        
        if not self.valid_fasta_sequence(fseq):
            raise ImportError, 'Require sequence in FASTA format (a comment line beginning with >, followed by the genomic sequence) with limitation on nucleotides: ONLY A, U, T, G, C, N letters in either upper or lower case.'
        
        fseq.seek(0)
        
        id = self.__get_seq_id(params)
        id_t = id_tools()
        if id == id_t.get_current_sequence_id(user_id, project_id):
            id = id_t.get_new_sequence_id(user_id, project_id)
        domain = self.__get_domain(params)
        species = self.__get_species(params)
        strain = self.__get_strain(params)
        replicon = self.__get_replicon(params)
        
        seq_data = ''
        i = 1
        seqs = []
        for line in fseq:
            if line.startswith('>'):
                if len(seqs) >= self.data_manager.get_nb_sequences_limitation() - 1:
                    raise ImportError, 'Too many sequences into your fasta file ! Maximum allowed is '  + str(self.data_manager.get_nb_sequences_limitation()) + ' per fasta file.'
                else:
                    if seq_data != '':
                        tmp_id = id + "." + str(i)
                        seq = sequence(tmp_id, seq_data, domain, species, replicon, strain)
                        seqs.append(seq)
                        i = i + 1
                    seq_data = line.replace('\r', '')
            else:
                temp = line.upper()
                temp = temp.replace('\r', '')
                seq_data += temp
        if i != 1:
            tmp_id = id + "." + str(i)
        else:
            tmp_id = id
        seq = sequence(tmp_id, seq_data, domain, species, replicon, strain)
        seqs.append(seq)
        events = []
        for seq in seqs:
            try:
                ev = self.add_sequence(user_id, project_id, seq)
                if ev is not None:
                    events.append(ev)
            except disk_error, e:
                project_size = self.data_manager.get_project_size(user_id,
                                                                        project_id)
                ev = disk_error_event(user_id, project_id, "-", e.message,
                                      project_size)
                self.data_manager.update_project_trace(user_id, project_id, [ev])
                raise
        
        fseq.close()
        self.data_manager.update_project_trace(user_id,project_id, events)
        
    def add_sequence(self, user_id, project_id, seq):
        
        max_length = self.data_manager.get_sequence_size_limitation()
        current_size = self.data_manager.get_user_sequences_used_space(user_id, project_id)
        if len(seq.data) + current_size < max_length:
            self.data_manager.add_sequence(user_id, project_id, seq)

            e = add_seq_event(user_id, project_id,
                              "-",
                              seq.id,
                              len(seq.data),
                              self.data_manager.get_sequence_header(user_id, project_id, seq.id),
                              0)
            return e
            #self.data_manager.update_project_trace(user_id,project_id, [e])

        else:
            space_left = max_length - current_size
            if space_left < 0:
                space_left = 0
            raise ImportError, 'Sequence length is way too long! You have '  + common_core.get_octet_string_representation(space_left) + ' left to import sequence(s)!'

    def get_sequence_size_limitation(self):
        return common_core.get_octet_string_representation(self.data_manager.get_sequence_size_limitation())

    def get_nb_sequences_limitation(self):
        return self.data_manager.get_nb_sequences_limitation()

    def create_new_project(self, user_id):
        """
        Return                the user projects' id
        user_id(string)       the id of the connected user
        """
        return self.data_manager.new_project(user_id)
    
    def get_sample_sequence(self, params):
        if self.__get_sample_sequence(params) == "True":
            current_dir = os.path.dirname(os.path.abspath(__file__))
            path = os.path.join(current_dir, "../../ressource/sample/")
            file = os.path.join(path, "sample.fna")
            infos_path = os.path.join(path, "sample.txt")
            data = ""
            for ligne in open(file):
                data += ligne
        
            infos_file = open(infos_path, 'r')
            infos = infos_file.read().split('\n')
            infos_dict = {}
            for line in infos:
                info = line.split('=')
                infos_dict[info[0]] = info[1]

            infos_file.close()
            seq = sequence("sample", data, 
                           domain=infos_dict['domain'],
                           replicon=infos_dict['replicon'],
                           species=infos_dict['species'], 
                           strain=infos_dict['strain'])
            return seq
        else:
            return None 
    
    def get_current_sequence_id(self, user_id, project_id):
        """
        Return                the current sequence id
        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        """    
        id_t = id_tools()
        return id_t.get_current_sequence_id(user_id, project_id)
        
    def save_user_email(self, user_id, project_id, params):
        """
        Save the user email
        user_id(string)          the id of the connected user
        email(string)            the email address
        project_id(string)       the project id
        params({})               the page params
        """
        email = self.__get_user_email(params)
        self.data_manager.save_user_email(user_id, project_id, email)

    def get_project_sequences(self, user_id, project_id):
        """
        Return                the sequence ids the project
        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        """
        sequences = []        
        try:
            ids = self.data_manager.get_sequences_id(user_id, project_id)   
            for id in sorted(ids):
                sequences.append(self.data_manager.get_sequence(user_id, id, project_id))
        except:
            pass
        return sequences

    def get_putative_rnas(self, user_id, project_id):
        """
        Return(string)        table of putative rnas
        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        """        
        prnas = {}
        try:
            prnas = self.data_manager.get_putative_rnas_by_runs(user_id, project_id)
        except:
            pass
        return prnas
        
    def get_alignments(self, user_id, project_id):
        return self.data_manager.get_alignments(user_id, project_id)
    
    def delete_alignment(self, user_id, project_id, params):
        align_id = self.__get_alignment_to_delete(params)
        if align_id != None:
            self.data_manager.delete_alignment(user_id, project_id, align_id)
    
    def delete_run(self, user_id, project_id, params):
        run_id = self.__get_run_to_delete(params)
        if run_id != None:
            self.data_manager.delete_run(user_id, project_id, run_id)
    
    def get_error_msg(self, params):
        if params.has_key("error"):
            return params["error"]
        else:
            return ""
    
    def get_project_sequences_header(self, user_id, project_id):
        """
        Return(string)        the header of the project sequences
        user_id(string)       the id of the connected user
        project_id(string)    the id of the project
        """
        headers = {}
        try:
            ids = self.data_manager.get_sequences_id(user_id, project_id)
            for id in ids:
                headers[id] = self.data_manager.get_sequence_header(user_id, project_id, id)
        except:
            pass
        return headers

    def is_an_authentification_platform(self):
        return self.data_manager.is_an_authentification_platform()
    
    def get_ids_from_authkey(self, id):
        """
        id(sting)      the id containing the user_id and the project_id
        return [user_id, project_id]
        """
        return self.data_manager.get_ids_from_authkey(id)

    def get_authkey(self, user_id, project_id):
        """
        user_id(sting)      the user_id
        project_id(string)  the project_id
        return the id
        """
        return self.data_manager.get_authkey(user_id, project_id)

    def get_action(self, params):
        if params.has_key("action"):
            return params["action"]
        else:
            return None

    def get_unknown_user_name(self):
        return self.data_manager.get_unknown_user_name()

    def __get_user_email(self, params):
        if params.has_key("email"):
            return params["email"]
        else:
            return ""

    def __get_sample_sequence(self, params):
        if params.has_key("get_sample_sequence"):
            return params["get_sample_sequence"]
        else:
            return "False"

    def valid_fasta_sequence(self, fseq):
        """        
        check the validity of the fasta file
        Return(boolean)       Tue if the sequence is in fasta format,
                              False otherwise
        fseq(file object)     the sequence we want to check
        """
        pattern = "^(>.+[\r\n][NATCGUnatcgu\ \r\n]+)+$"
        oneseq_pattern = re.compile(pattern)

        seq = ''
        i = 0
        for line in fseq:
            if line.startswith('>'):
                if seq != '':
                    if not oneseq_pattern.search(seq):
                        return False
                    i = i + 1
                seq = line
            else:
                seq += line
        if not oneseq_pattern.search(seq + '\n'):
            return False
        return True

    def __get_domain(self, params):
        if params.has_key("domain"):
            return params["domain"]
        else:
            return "unknown"
        
    def __get_species(self, params):
        if params.has_key("species"):
            return params["species"]
        else:
            return "unknown"
        
    def __get_strain(self, params):
        if params.has_key("strain"):
            return params["strain"]
        else:
            return "unknown"
        
    def __get_replicon(self, params):
        if params.has_key("replicon"):
            return params["replicon"]
        else:
            return "unknown"
        
    def __get_seq_id(self, params):
        if params.has_key("id"):
            return params["id"]
        else:
            return "unknown"

    def __get_alignment_to_delete(self, params):
        if params.has_key("alignment_to_delete"):
            return params["alignment_to_delete"]
        else:
            return None

    def __get_run_to_delete(self, params):
        if params.has_key("run_to_delete"):
            return params["run_to_delete"]
        else:
            return None
    
    def __get_sequences(self, params):
        abs_path = ''
        if self.__is_file_to_upload(params):
            os_temp, abs_path = tempfile.mkstemp()
            temp = open(abs_path, 'a')
            # avoid huge use of memory
            while True:
                data = params['file'].file.read(8192)
                if not data:
                    break
                temp.write(data)            
            temp.close()
            fseq = open(abs_path, 'r')
        else:
            seq = params['sequences']
            # get a file-object from the string seq
            fseq = StringIO.StringIO(seq)
        return fseq

    def __is_file_to_upload(self, params):
        if params.has_key("sequences"):
            if params["sequences"] == "":
                return True
            else:
                return False
        else:
            return False
    
    def get_mount_point(self):
        return self.data_manager.get_mount_point()
        
    def __check_valid_annotations(self, annot):
        tab = annot.split("\t")
        pattern = "[+\-.?]"
        strand_pattern = re.compile(pattern)
        if int(tab[3]) <= int(tab[4]) and strand_pattern.search(tab[6]):
            return True
        else :
            return False
            
    def __check_valid_rnaseq(self, rnaseq):
        
        pattern = "[NATCGUnatcgu\n]"
        oneseq_pattern = re.compile(pattern)
        
        if not rnaseq[0].startswith("@"):
            return False
        if not oneseq_pattern.search(rnaseq[1]):
                return False
        if not rnaseq[2].startswith("+"):
                return False
        
        pattern = "[!-~\n]+"
        oneseq_pattern = re.compile(pattern)
        if not oneseq_pattern.search(rnaseq[3]):
            return False
        return True
