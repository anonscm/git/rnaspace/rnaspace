var mouseIsDown = false;
var mouseX, mouseY;
var zoom_factor_x, zoom_factor_y;
var svgRoot = null;
var default_vb = "";
var scpt_text = '\
function showMouseover(e, m){\
   window.parent.mouseOver(e,m);\
}\
function showMouseout(e){\
   window.parent.mouseOut(e);\
}\
';
// split the viewBox string into x, y, width, height
function split_vb_string(vbstring){
    var vbarray = vbstring.split(' ');
    return vbarray;
}

function cgview_svg_loaded(){
    var obj = document.getElementById('cgview-obj');
    var doc = obj.contentDocument;
    doc.onmousemove = cgview_svg_mousemove;
    doc.onmousedown = cgview_svg_mousedown;
    doc.onmouseup = cgview_svg_mouseup;
    doc.onmousewheel = cgview_svg_mousewheel;    
    if(doc.addEventListener)
	doc.addEventListener('DOMMouseScroll', cgview_svg_mousewheel, false);
    var scpt = doc.createElementNS(svgns, "script");
    var text = doc.createTextNode(scpt_text, true);
    scpt.setAttribute("type", "text/javascript");
    scpt.appendChild(text);
    svgRoot = doc.rootElement;
    svgRoot.appendChild(scpt);
    svgRoot.setAttribute('viewBox', "0 0 " + 
			 svgRoot.getAttribute('width') + " " + 
			 svgRoot.getAttribute('height'));
    default_vb = svgRoot.getAttribute('viewBox');
    zoom_factor_x = parseInt(parseInt(svgRoot.getAttribute('width')) / 10);
    zoom_factor_y = parseInt(parseInt(svgRoot.getAttribute('height')) / 10);
}


function cgview_move(evt){
    var movex=0, movey=0;
    var panid = $(this).attr('id');
    var vb = split_vb_string(svgRoot.getAttribute('viewBox'));

    if( panid == "panning-left"){
	movex -= 50;
    }
    else if(panid == "panning-right"){
	movex += 50;
    }
    else if(panid == "panning-up"){
	movey -= 50;
    }
    else if(panid == "panning-down"){
	movey += 50;
    }

    vb[0] = parseInt(vb[0]) + movex;
    vb[1] = parseInt(vb[1]) + movey;
    svgRoot.setAttribute('viewBox', vb[0] + " " + vb[1] + " " + vb[2] +
			 " " + vb[3]);
}

function cgview_svg_mousemove(evt){
    if(mouseIsDown){
	var pos;
	var vb = split_vb_string(svgRoot.getAttribute('viewBox'));
	pos = get_mouse_positions(evt);
	vb[0] = parseInt(vb[0]) + mouseX - pos.x;
	vb[1] = parseInt(vb[1]) + mouseY - pos.y;
	mouseX = pos.x;
	mouseY = pos.y;	
//	svgRoot.currentTranslate.setXY(shiftX, shiftY);

	svgRoot.setAttribute('viewBox', vb[0] + " " + vb[1] + " " + vb[2] +
			     " " + vb[3]);
//	console.log(svgRoot.getAttribute('viewBox'));
    }
}

function cgview_svg_mousedown(evt){
    var pos;
    mouseIsDown = true;
    pos = get_mouse_positions(evt);
    mouseX = pos.x;
    mouseY = pos.y;
}

function cgview_svg_mouseup(evt){
    mouseIsDown = false;
}


/** Event handler for mouse wheel event.
 */
function cgview_svg_mousewheel(event){
    var delta = 0;
    if (!event) /* For IE. */
        event = window.event;
    if (event.wheelDelta) { /* IE/Opera. */
        delta = event.wheelDelta/120;
        /** In Opera 9, delta differs in sign as compared to IE.
         */
        if (window.opera)
            delta = -delta;
    } else if (event.detail) { /** Mozilla case. */
        /** In Mozilla, sign of delta is different than in IE.
         * Also, delta is multiple of 3.
         */
        delta = -event.detail/3;
    }
    /* If delta is nonzero, handle it.*/
    if (delta){
	if(delta > 0)
	    zoomIn();
	else
	    zoomOut();
	// if(delta > 0){
	//     zoom += 0.1;
	// }
	// else{
	//     zoom -= 0.1;
	// }
	// svgRoot.currentScale = zoom;
    }
    
    /* Prevent default actions caused by mouse wheel. */
    if (event.preventDefault)
                event.preventDefault();
    event.returnValue = false;
}


function zoomIn(){
    var vb = split_vb_string(svgRoot.getAttribute('viewBox'));
    if( parseInt(vb[2]) - zoom_factor_x > 0 && parseInt(vb[3]) - zoom_factor_y){
	vb[2] = parseInt(vb[2]) - zoom_factor_x;
	vb[3] = parseInt(vb[3]) - zoom_factor_y;
	vb[0] = parseInt(vb[0]) + zoom_factor_x/2;
	vb[1] = parseInt(vb[1]) + zoom_factor_y/2;
	svgRoot.setAttribute('viewBox', vb[0] + " " + vb[1] + " " + vb[2] +
			     " " + vb[3]);
    }
}

function zoomOut(){
    var vb = split_vb_string(svgRoot.getAttribute('viewBox'));
    vb[2] = parseInt(vb[2]) + zoom_factor_x;
    vb[3] = parseInt(vb[3]) + zoom_factor_y;
    vb[0] = parseInt(vb[0]) - zoom_factor_x/2;
    vb[1] = parseInt(vb[1]) - zoom_factor_y/2;
    svgRoot.setAttribute('viewBox', vb[0] + " " + vb[1] + " " + vb[2] +
			 " " + vb[3]);
}

function get_mouse_positions(e) {
    var posx = 0;
    var posy = 0;
    if (!e) var e = window.event;
    if (e.pageX || e.pageY){
	posx = e.pageX;
	posy = e.pageY;
    }
    else if (e.clientX || e.clientY){
	posx = e.clientX + document.body.scrollLeft
	    + document.documentElement.scrollLeft;
	posy = e.clientY + document.body.scrollTop
	    + document.documentElement.scrollTop;
    }
    return {x: posx, y:posy};
}

function mouseOver(evt, message){
    var pos = get_mouse_positions(evt);
    $("#svg-tooltip").css({
	position: "absolute",
	top: pos.y - 10,
	left: pos.x + 10
    });
    $("#svg-tooltip").html(message).show();    
}
function mouseOut(evt){
    $("#svg-tooltip").hide();
}