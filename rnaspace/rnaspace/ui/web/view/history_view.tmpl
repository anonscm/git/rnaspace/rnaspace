##
## RNAspace: non-coding RNA annotation platform
## Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##


#def page_title: History

#def info_boxes
#if $authentification_platform
active#slurp
#else
inactive#slurp
#end if
#end def

#block import_ressources
<script type="text/javascript" 
	src="${mount_point}ressource/js/history.js">
</script>
<style type="text/css">
  .odd{
    background-color: #ddecf6;
  }
  .even{
    background-color: #ecf5f9;
  }

  .history-event{
    padding: 2px;
    margin: 0px;
  }
  .history_info_section{ 
    display: block;
    background-color: #a0c7de;
    color: #fff;
    font-weight: bold;
    padding: 2px;
    padding-left: 20px;
    border-bottom: 1px dashed #56b0ea;
  }
</style>
#end block

#block jquery
#end block

#block javascript
#end block

#block page_content 
#if $info_boxes != "inactive"
<div class="infobar">
  $infobar
</div>

<div class="page">
#end if
<div align="left" style="overflow:auto; height:500px;">
#for $events_block in $events:

<div class="info_section" style="margin:5px;margin-bottom:0px;">  

  <span class="history_info_section">
    #if $events_block["run"] is None:
    User action
    #elif $events_block["run"] == 'all':
    Masking
    #else:
    ${events_block["run"]}
    #end if
  </span>
  <div class="info-content" style="margin:0px;">
  #for ($i, $event) in enumerate($events_block["events"]):
    #set $test = $i%2
    #if $test == 0:
       #set $even_class = "even"
    #else:
       #set $even_class = "odd"
    #end if
  
    ## pretty date
    #set $date_str = $event.date_str.split()
    #set $date = $date_str[0] + "-" + $date_str[1] + "-" + $date_str[2] + " "
    #set $date += $date_str[3] + ":" + $date_str[4] + ":" + $date_str[5]

    ## predict event
    #if $event.type == "PREDICT":
    <div class="history-predict-event history-event $even_class" 
	 style="margin:0px;">
      #set $splited_cmd = $event.command.split(' ')
      #set $cmd = ""
      #for $junk in $splited_cmd:
          #set $cmd += " " + $junk.split('/')[-1]
      #end for
      <table>
	<tr>
	  <td><b>$event.gene_finder_name</b> ($event.gene_finder_version)</td>
	  <td> | $event.seq_name</td>
	  ## <td>$event.parameters</td>		
	  <td> | $event.nb_prediction RNAs</td>
	  <td>$event.nb_alignment Alignments</td>
	  <td> | $event.running_time sec</td>
	  <td> | $date</td>
	  <td> | $event.run_id</td>
	</tr>
      </table>
      <div style="padding:2px;padding-left:20px;margin:0px" align="left">
        <span class="history-predict-command">
        #set cmd = cmd[1:len(cmd)-1]
        #import textwrap
        #set lignes = textwrap.wrap($cmd,90)
        #for i in lignes
    	  <pre>$i</pre>
        #end for
        </span>
      </div>
    </div>

    ##masking event
    #elif $event.type == "MASKING":
    <div class="history-event $even_class" 
	 style="margin:0px;">
      #set $splited_cmd = $event.command.split(' ')
      #set $cmd = ""
      #for $junk in $splited_cmd:
          #set $cmd += " " + $junk.split('/')[-1]
      #end for
      <table>
	<tr>
	  <td><b>$event.software_name</b></td>
	  <td> | $event.seq_name</td>
	  ## <td>$event.parameters</td>		
	  <td> | $event.running_time sec</td>
	  <td> | $date</td>
	##  <td> | $event.run_id</td>
	</tr>
      </table>
      <div style="padding:2px;padding-left:20px;margin:0px" align="left">
        <span class="history-predict-command">
        #set cmd = cmd[1:len(cmd)-1]
        #import textwrap
        #set lignes = textwrap.wrap($cmd,90)
        #for i in lignes
    	  <pre>$i</pre>
        #end for
        </span>
      </div>
    </div>

    ## combine event
    #elif $event.type == "PREDICT_COMBINE":

      #if hasattr($event, "rnas_not_combined"):
        #set $minus_rnas = len($event.rnas_to_combine) - len($event.rnas_not_combined)
        #set $plus_rnas = len($event.rnas_combined)
      #else:
        #set $minus_rnas = 0
        #set $plus_rnas = 0
      #end if
    <div class="history-predict-combine-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>COMBINE</b> ($event.combine_type)</td>
	  <td> | -${minus_rnas}+${plus_rnas} 
	    RNAs
	  </td>
	  <td> | $event.running_time sec</td>
	  <td> | $date</td>
	  <td> | $event.run_id</td>
	</tr>
      </table>      
    </div>

    ## combine event
    #elif $event.type == "USER_COMBINE":
      #if hasattr($event, "rnas_not_combined"):
        #set $minus_rnas = len($event.rnas_to_combine) - len($event.rnas_not_combined)
        #set $plus_rnas = len($event.rnas_combined)
      #else:
        #set $minus_rnas = 0
        #set $plus_rnas = 0
      #end if
    <div class="history-user-combine-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>COMBINE</b> ($event.combine_type)</td>
	  <td> | -${minus_rnas}+${plus_rnas} 
	    RNAs
	  </td>
	  <td> | $event.running_time sec</td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>


    ## add sequence event
    #elif $event.type == "ADD_SEQ":
    <div class="history-add-seq-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>ADD SEQUENCE</b></td>
	  <td> | $event.seq_name</td>
	  <td> | $event.seq_size</td>
	  <td> | $event.comment</td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>

    ## remove rna event
    #elif $event.type == "REMOVE_RNA":
    <div class="history-remove-rna-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>REMOVE PREDICTIONS</b></td>
	  <td> | 
	    #for rna in $event.rna_user_ids:
	      $rna, 
	    #end for
	  </td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>

    ## edit rna event
    #elif $event.type == "EDIT_RNA":
    <div class="history-remove-rna-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>EDIT PREDICTION</b></td>
	  <td> | $event.edited_rna_user_id</td>
	  ## <td> | $event.rna.seq_name</td>
	  <td> | $event.rna.family</td>
	  <td> | $event.rna.start</td>
	  <td> | $event.rna.stop</td>
	  <td> | $event.rna.strand</td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>

    ## add rna event
    #elif $event.type == "ADD_RNA":
    <div class="history-remove-rna-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>ADD PREDICTION</b></td>
	  <td> | $event.rna.rna_user_id</td>
	  ## <td> | $event.rna.seq_name</td>
	  <td> | $event.rna.family</td>
	  <td> | $event.rna.start</td>
	  <td> | $event.rna.stop</td>
	  <td> | $event.rna.strand</td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>

    ## remove rna event
    #elif $event.type == "ADD_ALIGNMENT":
    <div class="history-remove-rna-event history-event $even_class"
	 style="margin:0px;">
      <table>
	<tr>
	  <td><b>ADD ALIGNMENT</b></td>
	  <td> | 
	    #for rna in $event.rna_user_ids:
	      $rna, 
	    #end for
	  </td>
	  <td> | $date</td>
	</tr>
      </table>      
    </div>

    #end if	
    
  #end for
  </div>
</div>

#end for
</div>
<div style="clear:right"> </div>


#if $info_boxes != "inactive"

</div>
#end if


#end block
