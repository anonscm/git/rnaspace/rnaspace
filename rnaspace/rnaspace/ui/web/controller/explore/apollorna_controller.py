#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import cherrypy
from cherrypy.lib.static import serve_file

from rnaspace.ui.web.utils.common import common
from rnaspace.ui.web.utils.common import AUTH_ERROR
from rnaspace.ui.web.controller.error_controller import error_controller
from rnaspace.ui.web.model.explore.apollorna_model import apollorna_model

class apollorna_controller(object):

    def __init__(self):
        self.model = apollorna_model()


    @cherrypy.expose
    def index(self, **params):

        view = common.get_template('explore/apollorna_view.tmpl')
        
        if not params.has_key("authkey"):
            return "You're not authorized to access this page."

        (user_id,
         project_id) = self.model.get_ids_from_authkey(params["authkey"])
        
        if user_id is None or project_id is None:
            return self.error(AUTH_ERROR)

        if not self.model.user_has_data(user_id):
            return "You're data have been deleted. For server maintenance, " +\
                   "data are cleared after a while."

        if self.model.get_action(params) == "view":
            return str(view)
        
        elif self.model.get_action(params) == "zip":
            (error, result) = self.model.get_zippath(user_id, project_id, params)
            if error:
                return self.error(result)
            else:
                return serve_file(result, "application/x-download", "attachment")


    @cherrypy.expose
    def error(self, msg, authkey=None):
        error_page = error_controller()
        return error_page.get_page(msg, authkey)
