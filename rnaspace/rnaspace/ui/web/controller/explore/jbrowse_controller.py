#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import os
import cherrypy
from cherrypy.lib import static

from rnaspace.ui.web.utils.common import AUTH_ERROR
from rnaspace.ui.web.model.explore.jbrowse_model import jbrowse_model
from rnaspace.ui.web.controller.error_controller import error_controller

class jbrowse_controller(object):

    def __init__(self):
        self.model = jbrowse_model()

    @cherrypy.expose
    def index(self, **params):
        (user_id, project_id) = (None, None)

        if params.has_key("authkey"):
            (user_id,
             project_id) = self.model.get_ids_from_authkey(params["authkey"])
            
        if user_id is None or project_id is None:
            return self.__error("You're not authorized to access this page")
        
        if not params.has_key("name"):
            return ""

        path = self.model.file_path(user_id, project_id, params['name'])

        request_type = "application/x-download"
        disposition = "attachment"
        
        if params.has_key("type"):
            request_type = params["type"]
        if params.has_key("disposition"):
            disposition = params["disposition"]

        return static.serve_file(path, request_type, disposition,
                                 os.path.basename(path))

    @cherrypy.expose
    def prepare_jbrowse (self, **params):
        (user_id, project_id) = (None, None)

        if params.has_key("authkey"):
            (user_id,
             project_id) = self.model.get_ids_from_authkey(params["authkey"])
            
        if user_id is None or project_id is None:
            return self.__error("You're not authorized to access this page")
        
        self.model.prepare_jbrowse_tracks(params)
        
        return "Done"


    def __error(self, msg):   
        """ Build an error page defined by a message error
            msg(type:string)   the message to display 
        """  
        error_page = error_controller()
        return error_page.get_page(msg)
