#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from multiprocessing import Pool
import os

from rnaspace.core.data_manager import data_manager
from rnaspace.core.manage.mask_annot import mask_annot
from rnaspace.core.manage.mask_nontranscrit import mask_nontranscrit
from rnaspace.core.manage.mask_mdust import mask_mdust
from rnaspace.core.manage.mask_repeatmasker import mask_repeatmasker
from rnaspace.core.manage.mask import mask


class mask_manager(object):
    """ 
    Class mask_region_handler: the mask region handler is the class that 
    will stock and produce the GFF output.
    """
    
    def __init__(self):
        """ Init
        run_id(string):     run id
        """
        
        self.data_manager = data_manager()
        #self.run_id = run_id
        self.id = 'mask'
        
    def run(self, run_id, user_id, project_id, params):
        """ 
        user_id(string):       the id of the connected user
        project_id(string):    the id of the project
        mparams(dict):         the masking params
        """
	self.run_id = run_id
        ids = self.data_manager.get_sequences_id(user_id, project_id)
        
        mparams = {}
        for i in range(len(ids)):
            mparams[params["mask_id"+str(i)]] = {"annot_tmp_path":params["annot_tmp_path"+str(i)],
                       "annot_x":params["annot_x"+str(i)],
                       "transcript_tmp_path":params["transcript_tmp_path"+str(i)],
                       "transcript_format":params["transcript_format"+str(i)],
                       "transcript_x":params["transcript_x"+str(i)],
                       "transcript_p":params["transcript_p"+str(i)],
                       "transcript_w":params["transcript_w"+str(i)],
                       "mdust_run":params["mdust_run"+str(i)],
                       "rm_run":params["rm_run"+str(i)]}
                       
        for i in range(len(ids)):
            pool = Pool(processes=len(ids))
            pool.map(self.launch(user_id, project_id, ids[i], mparams[ids[i]]),( ))

     
        # delete tmp files after masking all sequences because when a multiFASTA 
        # is loaded, sequences have the same file for annotation and transcrits
        for i in range(len(ids)):
            if os.path.exists(params["annot_tmp_path"+str(i)]):
                os.remove(params["annot_tmp_path"+str(i)])
            if os.path.exists(params["transcript_tmp_path"+str(i)]):
                os.remove(params["transcript_tmp_path"+str(i)])

    def launch(self, user_id, project_id, seq_id, mparams):
        """ 
        user_id(string):       the id of the connected user
        project_id(string):    the id of the project
        seq_id(string):        the id of the project
        mparams(dict):         the masking params
        """
        regions = []
        seq = self.data_manager.get_sequence(user_id, seq_id, project_id)
        
        if mparams["annot_tmp_path"] != "":
            annot = mask_annot(self.run_id)
            regions += annot.find_regions(user_id, project_id, seq_id, mparams, seq.domain, len(seq.data))
        if mparams["transcript_tmp_path"] != "":
            nontranscrit = mask_nontranscrit(self.run_id)
            regions += nontranscrit.find_regions(user_id, project_id, seq_id, mparams, len(seq.data))
        if mparams["mdust_run"] != "0":
            mdust = mask_mdust(self.run_id)
            regions += mdust.find_regions(user_id, project_id, mparams, seq_id)
        if mparams["rm_run"] != "0":
            repeatmasker = mask_repeatmasker(self.run_id)
            regions += repeatmasker.find_regions(user_id, project_id, mparams, seq_id)
            
        regions = mask().sort_and_combine(regions)            

        if (len(regions)) > 0:
            # Record masked regions
            self.data_manager.add_regions(user_id, project_id, seq_id, regions, self.id)
            # Generate the masking sequence
            sequence = [">"+seq.id+"\n"]
            mask_seq = ""
            start_position = 0
            for start,stop in regions:
                if start == 1:
                    mask_seq = "N"*(stop-start+1)
                else:
                    mask_seq += seq.data[start_position:start-1] + "N"*(stop-start+1)
                start_position = stop
            if start_position <= len(seq.data):
                mask_seq += seq.data[start_position:]
            
            sequence.append(mask_seq)
            
            self.data_manager.add_masked_sequence(user_id, project_id, seq.id, sequence)
        
        
        

