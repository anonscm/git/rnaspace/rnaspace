#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import time

from rnaspace.core.manage.mask import mask
from rnaspace.core.data_manager import data_manager

class mask_annot(mask):
    """ 
    Class mask_annot: the mask annotation processor is the class that 
    will get masking regions from annotations.
    """
    
    def __init__(self, run_id):
        """ Init
        run_id(string):        run id
        """
        self.data_manager = data_manager()        
        self.id = "mask_annot"
        self.run_id = run_id
        
    def find_regions(self, user_id, project_id, seq_id, mparams, domain, seq_len):
        """ 
        Return(list(list)):             a list of list containing the 
                                        start and stop
        user_id(string):                the id of the connected user
        project_id(string):             the id of the project
        seq_id(string):                 the sequence id
        mparams(dict):                  the masking params
        domain(string)                  the domain of the sequence
        """
         
        t1 = time.time()
        gff_file = open(mparams["annot_tmp_path"], "r")
        X = int(mparams["annot_x"])
        regions = []
        
        if domain != "eukaryote":
            coding = 'CDS'
        else:
            coding = 'exon'
        
        line = gff_file.readline()
        if line.startswith("##gff-version 3"):
            type = "gff3"
        else :
            type = "gff2"
            if not line.startswith("#"):
                tab = line.split('\t')
                if tab[2] == coding:
                    x = int(tab[3])+X ; y = int(tab[4])-X
                    if x <= y and x <= seq_len:
                        regions.append([x, min(y,seq_len)])            
                        
        for line in gff_file:
            if type == "gff3" and line.startswith("##FASTA"):
                break
            if not line.startswith("#"):
                tab = line.split('\t')
                if tab[2] == coding:
                    x = int(tab[3])+X ; y = int(tab[4])-X
                    if x <= y and x <= seq_len:
                        regions.append([x, min(y,seq_len)])            

        gff_file.close()
        regions = self.sort_and_combine(regions)
        if len(regions) > 0:
            self.data_manager.add_regions(user_id, project_id, seq_id, regions, self.id)
        t2 = time.time()
        self.trace_predict_event(user_id, project_id, self.run_id, seq_id, "Annotations", "-", t2-t1)
        return regions
    
    
