#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import tempfile
import sys

import time
import subprocess

from rnaspace.core.manage.mask import mask
from rnaspace.core.data_manager import data_manager
from rnaspace.dao.storage_configuration_reader import storage_configuration_reader

class mask_nontranscrit(mask):
    """ 
    Class mask_nontranscrit: the mask annotation processor is the class that 
    will get masking regions from transcrit regions.
    """
    
    def __init__(self, run_id):
        """ Init
        run_id(string):     run_id
        """
        self.data_manager = data_manager()
        self.config = storage_configuration_reader()
        self.id = "mask_nontranscrit"
        self.MIN_MASKED_REGION_LENGTH = 100

        self.run_id = run_id

        self.grid_engine = self.data_manager.config.get("execution","grid_engine")
        self.grid_engine_cmd = self.data_manager.config.get("execution","grid_engine.cmd")
        self.grid_engine_outfile_arg = self.data_manager.config.get("execution","grid_engine.outfile_arg") 
        self.grid_engine_errorfile_arg = self.data_manager.config.get("execution","grid_engine.errorfile_arg")
        
    def find_regions(self, user_id, project_id, seq_id, mparams, seq_len):
        """ 
        Return(list(list)):     a list of tuples containing the start and stop
        user_id(string):        the id of the connected user
        project_id(string):     the id of the project
        seq_id(string):         the sequence id
        mparams(dict):          the masking params
        seq_len(int):           the sequence length
        """
        if mparams["transcript_format"] == "fasta":
            t1 = time.time()
            cmd = self.data_manager.get_config_value("software","formatdb.exe")
            cmd += " -p F -i "+mparams["transcript_tmp_path"]

            if self.grid_engine=="true":
                # put cmd in a shell file and call the grid engine batch job submiter
           
                os_temp, tmp_cmd_file = tempfile.mkstemp()
                tmp_out_file = tmp_cmd_file+".out"
                tmp_error_file = tmp_cmd_file+".error"            
                ftmp = open(tmp_cmd_file, 'w')
                ftmp.write(cmd)
                ftmp.close()
           
                CMD = self.grid_engine_cmd +" "+ self.grid_engine_outfile_arg +" "+ tmp_out_file+" " \
                      + self.grid_engine_errorfile_arg +" "+ tmp_error_file +" "+ tmp_cmd_file
            else:
                CMD = cmd
            
            # Execute formatdb
            envir = os.environ            
            try:
                stdout = self.data_manager.get_stdout_path(user_id, project_id, "01", seq_id, "formatdb", "")
                stderr = self.data_manager.get_stderr_path(user_id, project_id, "01", seq_id, "formatdb", "")
                fstderr = open(stderr,'w')
                fstdout = open(stdout,'w')
                retcode = subprocess.call(CMD, shell=True, stderr=fstderr, stdout=fstdout, env=envir)
                fstderr.close()
                fstdout.close()
                if retcode == 0: 
                    os.remove(stdout)
                    os.remove(stderr)
            except :
                mess = "Error launching subprocess in mask_nontrancrit:find_regions."
                self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)
                sys.exit(1)  # stop thread
            
            if self.grid_engine=="true":   
                os.remove(tmp_cmd_file)
                os.remove(tmp_out_file)
                os.remove(tmp_error_file)    
                
            if retcode != 0:
                fstderr = open(stderr,'r')
                mess = fstderr.read()
                fstderr.close()
                mess = mess.replace('\n', ' ')
                self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)

            trace_cmd = cmd
            os_temp, blast_results = tempfile.mkstemp()            
            fasta_path = self.config.get_sequence_directory(user_id, project_id)
            fasta_path = os.path.join(fasta_path, seq_id+".fna")
            cmd = self.data_manager.get_config_value("software","megablast.exe")
            cmd += " -d "+mparams["transcript_tmp_path"]+" -i "+str(fasta_path)+" -D 3 -p "+mparams["transcript_p"]+" -W "+mparams["transcript_w"]+" -o "+str(blast_results)
            
            if self.grid_engine=="true":
                os_temp, tmp_cmd_file = tempfile.mkstemp()
                tmp_out_file = tmp_cmd_file+".out"
                tmp_error_file = tmp_cmd_file+".error"            
                ftmp = open(tmp_cmd_file, 'w')
                ftmp.write(cmd)
                ftmp.close()
           
                CMD = self.grid_engine_cmd +" "+ self.grid_engine_outfile_arg +" "+ tmp_out_file+" " \
                      + self.grid_engine_errorfile_arg +" "+ tmp_error_file +" "+ tmp_cmd_file
            else:
                CMD = cmd
            
            # Execute megablast
            envir = os.environ
            try:                
                stdout = self.data_manager.get_stdout_path(user_id, project_id, "01", seq_id, "megablast", "")
                stderr = self.data_manager.get_stderr_path(user_id, project_id, "01", seq_id, "megablast", "")
                fstderr = open(stderr,'w')
                fstdout = open(stdout,'w')
                retcode = subprocess.call(CMD, shell=True, stderr=fstderr, stdout=fstdout, env=envir)
                fstderr.close()
                fstdout.close()
                os.remove(mparams["transcript_tmp_path"]+".nhr")
                os.remove(mparams["transcript_tmp_path"]+".nin")
                os.remove(mparams["transcript_tmp_path"]+".nsq")
                if retcode == 0:
                    os.remove(stdout)
                    os.remove(stderr)
            except :
                mess = "Error launching subprocess in mask_nontrancrit:find_regions."
                self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)
                sys.exit(1)  # stop thread
            
            if self.grid_engine=="true":   
                os.remove(tmp_cmd_file)
                os.remove(tmp_out_file)
                os.remove(tmp_error_file)    
                
            if retcode != 0:
                fstderr = open(stderr,'r')
                mess = fstderr.read()
                fstderr.close()
                mess = mess.replace('\n', ' ')
                self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)
                os.remove(stdout)
                os.remove(stderr)
            
            regions = self.__get_transcripts_regions(blast_results)
            os.remove(str(blast_results))
            trace_cmd = trace_cmd + cmd
            
        elif mparams["transcript_format"] == "gff" :
            t1 = time.time()
            gff_file = open(mparams["transcript_tmp_path"], "r")
            regions = []
            type= ""
            line = gff_file.readline()
            if line.startswith("##gff-version 3"):
                type = "gff3"
            else :
                type = "gff2"
                if not line.startswith("#"):
                    tab = line.split('\t')
                    x = int(tab[3]) ; y = int(tab[4])
                    if x <= y and x <= seq_len:
                        regions.append([x, min(y,seq_len)])            
                        
            for line in gff_file:
                if type == "gff3" and line.startswith("##FASTA"):
                    break
                if not line.startswith("#"):
                    tab = line.split('\t')
                    x = int(tab[3]) ; y = int(tab[4])
                    if x <= y and x <= seq_len:
                        regions.append([x, min(y,seq_len)])            

            trace_cmd = '-'
            gff_file.close()
        else :
            raise ImportError, "Cannot find transcript file (fasta/gff)"

        regions = self.sort_and_combine(regions)
        regions = self.__create_non_transcripts(regions, int(mparams["transcript_x"]), seq_len)
        
        # Filter regions on length
        regions_toremove = []
        for r in regions:
            if r[1]-r[0]+1 < self.MIN_MASKED_REGION_LENGTH:
                regions_toremove.append(r)
        for r in regions_toremove:
            regions.remove(r)
            
        if len(regions) > 0 :      
            self.data_manager.add_regions(user_id, project_id, seq_id, regions, self.id)
        t2 = time.time()
        self.trace_predict_event(user_id, project_id, self.run_id, seq_id, "NonTranscript", trace_cmd, t2-t1)

        return regions
    
    
    def __create_non_transcripts(self, regions, X, seq_len):
        """
        Return(list(list))    non transcripts regions coordinates to be masked
        
        regions(list(list))   non transcripts regions
        X (int)               number of nucleotide not masked on boundaries
        seq_len(int)          sequence length
        """
        # Find non transcript regions
        start = 1
        coords = []
        for ls,le in regions:
            if ls != 1:
                coords.append([start,ls-1])
            start = le+1
        if start < int(seq_len):
                coords.append([int(start), int(seq_len)])
                
        # Reduce non transcript regions on boundaries
        final_coords = []
        for ls,le in coords:
            if ls+X <= le-X:
                final_coords.append([ls+X, le-X])
            
        return final_coords

    def __get_transcripts_regions(self, blast_results):
        """
        Return(list(list))            transcripts regions coordinates.
        
        blast_results(string)         file name where megablast results are
        """
        blast_file = open(blast_results, 'r')
        coordinates = []
        for line in blast_file:
            if not line.startswith("#"):
                tab = line.split("\t")
                coordinates.append([int(tab[6]),int(tab[7])])
        blast_file.close()
        return coordinates
