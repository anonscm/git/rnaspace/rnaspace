#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import tempfile
import sys

import time
import subprocess

from rnaspace.core.manage.mask import mask
from rnaspace.core.data_manager import data_manager

class mask_mdust(mask):
    """ 
    Class mask_mdust: the mask annotation processor is the class that 
    will get masking regions from mdust.
    """
    
    def __init__(self, run_id):
        """ Init
        run_id(string):     run id
        """
        self.data_manager = data_manager()
        self.id = "mask_mdust"
        
        self.run_id = run_id
        
        self.grid_engine = self.data_manager.config.get("execution","grid_engine")
        self.grid_engine_cmd = self.data_manager.config.get("execution","grid_engine.cmd")
        self.grid_engine_outfile_arg = self.data_manager.config.get("execution","grid_engine.outfile_arg") 
        self.grid_engine_errorfile_arg = self.data_manager.config.get("execution","grid_engine.errorfile_arg")
        
        
    def find_regions(self, user_id, project_id, mparams, seq_id):
        """ 
        Return(list(list)):      a list of list containing the start and stop
        user_id(string):         the id of the connected user
        project_id(string):      the id of the project
        mparams(dict):      the masking params
        seq_id(string):          the sequence id
        """
        
        t1 = time.time()

        os_temp, temp = tempfile.mkstemp()
        path = self.data_manager.get_sequence_directory(user_id, project_id)
        file = os.path.join(path, seq_id+".fna")
        cmd = self.data_manager.get_config_value("software","mdust.exe")
        cmd += " " + file + " -c > " + temp
        
        if self.grid_engine=="true":
            # put cmd in a shell file and call the grid engine batch job submiter
            os_temp, tmp_cmd_file = tempfile.mkstemp()
            tmp_out_file = tmp_cmd_file+".out"
            tmp_error_file = tmp_cmd_file+".error"            
            ftmp = open(tmp_cmd_file, 'w')
            ftmp.write(cmd)
            ftmp.close()
       
            CMD = self.grid_engine_cmd +" "+ self.grid_engine_outfile_arg +" "+ tmp_out_file+" " \
                  + self.grid_engine_errorfile_arg +" "+ tmp_error_file +" "+ tmp_cmd_file
        else:
            CMD = cmd
        
        # Execute mdust
        envir = os.environ
        
        try:
            stdout = self.data_manager.get_stdout_path(user_id, project_id, "01", seq_id, self.id, "")
            stderr = self.data_manager.get_stderr_path(user_id, project_id, "01", seq_id, self.id, "")
            fstderr = open(stderr,'w')
            fstdout = open(stdout,'w')
            retcode = subprocess.call(CMD, shell=True, stderr=fstderr, stdout=fstdout, env=envir)
            fstderr.close()
            fstdout.close()
            if retcode == 0:
                os.remove(stdout)
                os.remove(stderr)
        except :
            mess = "Error launching subprocess in mask_dust:find_regions."
            self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)
            sys.exit(1)  # stop thread
 
        if self.grid_engine=="true":   
            os.remove(tmp_cmd_file)
            os.remove(tmp_out_file)
            os.remove(tmp_error_file)    
            
        if retcode != 0:
            fstderr = open(stderr,'r')
            mess = fstderr.read()
            fstderr.close()
            mess = mess.replace('\n', ' ')
            self.trace_error_event(user_id, project_id, mess, cmd, self.run_id, self.id)
        
        regions = []
        if os.path.getsize(temp) > 0:
            regions_file = open(temp, 'r')
            regions = []
            for i in regions_file:
                tab = i.split("\t")
                regions.append([int(tab[2]),int(tab[3])])
                
            regions = self.sort_and_combine(regions)
            
            self.data_manager.add_regions(user_id, project_id, seq_id, regions, self.id)
            
        t2 = time.time()
        os.remove(temp)
        self.trace_predict_event(user_id, project_id, self.run_id, seq_id, "mdust", cmd, t2-t1)
        return regions

