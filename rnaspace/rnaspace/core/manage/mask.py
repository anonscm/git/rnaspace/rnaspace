#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from rnaspace.core.email_sender import email_sender
from rnaspace.core.trace.event import error_event, mask_event

class mask:
    """ 
    Class mask: the mask class, will be used to inherit the sort_and_combine().
    """
    
    def find_regions():
        """ method to be surcharged """
        pass

    def sort_and_combine(self, regions):
        """
        return a list of region sorted in growing start position
        with overlapping regions combined in a single region
        
        regions(type(list))   list of region (list of start, stop position)
        """
        combined_regions=[]
        
        if len(regions)>0:
            regions.sort()
            r=regions[0]
            for (start,stop) in regions[1:]:
                if start<=r[1]:
                    if stop > r[1]:
                        r[1]=stop
                else:
                    combined_regions.append(r)
                    r=[start,stop]
            combined_regions.append(r)

        return combined_regions

    def trace_error_event(self, user_id, project_id, mess, cmd, run_id, softname):
        """
        user_id(string):       the id of the connected user
        project_id(string):    the id of the project
        mess(string):          the error message
        cmd(string):           command line
        run_id(string):        run id
        softname(string):      software name
        """
        email_s = email_sender()
        email_s.send_admin_failed_email(user_id, project_id,
                                             run_id, softname,
                                             cmd)
        message = "Error running %s %s"%(self.id, mess)
        message = message + ". See run " + run_id + " description."
        mail = self.data_manager.get_user_email(user_id, project_id)
        project_size = self.data_manager.get_project_size(user_id, project_id)
        e = error_event(user_id, project_id, mail, message, project_size)
        self.data_manager.update_project_trace(user_id, project_id, [e])
        
    def trace_predict_event(self, user_id, project_id, run_id, seq_name, softname, cmd, running_time):
        """
        user_id(string):       the id of the connected user
        project_id(string):    the id of the project
        run_id(string):        run id
        seq_name(string):      sequence name
        softname(string):      software name
        cmd(string):           command line
        running_time(float):   the consumed time from the event
        """
        e = mask_event(user_id, project_id,
                          self.data_manager.get_user_email(user_id,project_id),
                          run_id, 
                          seq_name,
                          softname, 
                          cmd,
                          running_time,
                          self.data_manager.get_project_size(user_id,project_id),
                          )
        self.data_manager.update_project_trace(user_id, project_id, [e])    
    
