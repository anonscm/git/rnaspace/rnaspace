#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import time
try:
    import cpickle as pickle
except:
    import pickle

from rnaspace.core.putative_rna import putative_rna
from rnaspace.core.id_tools import id_tools
from rnaspace.core.families_table import families
import data_manager

class sequence_combiner (object):
    """
    Class sequence_combiner: combine sequence
    """
    
    combine_type = {"basic_combine" : ""}

    def __init__(self):
        self.data_manager = data_manager.data_manager()
        
    def run (self, user_id, project_id, prnas, combine_type,
             user_choice=1, run_id=None, **params):
        """ 
        Combine all putative rnas given 
        user_id(type:string)      user id of the connected user
        project_id(type:string)   the project the user is working on
        prnas([putative_rnas])    the prnas to combine
        combine_type(string)      the combiner algorithm to use
        **params({string:string}) combiner parameters
        """
        t1 =  time.clock()
        merged = []
        not_merged = []
        not_merged_warning = []
        if combine_type in self.combine_type:
            if combine_type == "basic_combine":
                (merged,
                 not_merged,
                 not_merged_warning) = self.__run_basic_combine(user_id,
                                                                project_id,
                                                                prnas,
                                                                user_choice,
                                                                params, run_id)
        t2 = time.clock()

        return {"merged": merged, "time": t2-t1, "not-merged": not_merged,
                "not-merged-warning": not_merged_warning}
    
    def __get_complement(self, base):
        """ 
        Return the complement base
        base(string)     the base to complement
        """
        cbase_table = {
            'A': 'T',
            'a': 't',
            'T': 'A',
            't': 'a',
            'C': 'G',
            'c': 'g',
            'G': 'C',
            'g': 'c',
            'U': 'A',
            'u': 'a',
            'N': 'N',
            'n': 'n'}
        return cbase_table[base]
 
    def __get_reverse_complement(self, seq):
        """ Return the reverse complement sequence
        seq(string)     the sequence to reverse and complement
        """
        rc_seq = ""
        for base in reversed(seq):
            rc_seq += self.__get_complement(base)
        return rc_seq

    def __change_alignment_id(self, user_id, project_id, prna, new_id):
        """Change the prna id in the alignments.

        We need to change the 'rna_id' attribute of the alignments where the
        prna is involved with the new id of the prna.
        """
        
        if prna.alignment is not None:
            align_list = []
            align_ids = []
            for align_id in prna.alignment:
                align = self.data_manager.get_alignment(user_id, project_id,
                                                        align_id)
                for entry in align.rna_list:
                    if entry.rna_id == prna.sys_id:
                        if entry.rna_id == entry.user_id:
                            entry.user_id = new_id
                        entry.rna_id = new_id

                align_list.append(align)
                align_ids.append(align_id)

            if len(align_list) > 0:        
                self.data_manager.add_alignments(user_id, project_id, 
                                                 align_list)
                return align_ids
            
        return []
        
    def __merge_prnas(self, user_id, project_id, run_id, prnas, start, stop,
                      family, structure=[]):
        """Merge all prna that are in 'prnas' list and return the new one.

        A nice program name is given to new prna:
            combine:soft1+soft2+soft3...

        """
                
        sequence_id = prnas[0].genomic_sequence_id
        
        # get a new id
        id_gen = id_tools()
        prna_id = id_gen.get_new_putativerna_id(user_id, project_id,
                                                sequence_id)

        # get the sequence according to the strand
        user_seq = self.data_manager.get_sequence(user_id, sequence_id,
                                                  project_id)                
        strand = prnas[0].strand
        if strand == "-":
            sequence = self.__get_reverse_complement(user_seq.data[start-1:stop])
        else:
            sequence = user_seq.data[start-1:stop]

        # nice program_name + update id in alignments
        softs = []
        alignments = []
        for prna in prnas:
            if prna.program_name not in softs:
                softs.append(prna.program_name)

            align_list = self.__change_alignment_id(user_id, project_id,
                                                    prna, prna_id)
            alignments.extend(align_list)

        softs.sort()
        if len(softs) > 1:
            program_name = 'combine:' + '+'.join(soft for soft in softs)
        else:
            program_name = 'combine:' + softs[0]

        score = 0.0
        # if len(prnas) == 1:
        #     score = prnas[0].score
        
        prna_run_id = run_id
        if run_id is None:
            prna_run_id = prnas[0].run
        
        # create the new prna
        prna = putative_rna(prna_id, sequence_id, start, stop,
                            prna_run_id, prna_id, sequence, family, strand, 
                            prnas[0].domain, prnas[0].species, prnas[0].strain, 
                            prnas[0].replicon, structure, score, program_name,
                            '-', str(time.localtime()[2]),
                            str(time.localtime()[1]), str(time.localtime()[0]),
                            alignments)
        return prna


    def __overlap(self, startA, stopA, startB, stopB, combine_options):
        """ Test if two putative rnas overlap.
        
        prnas has to be sorted by start position, ie: startA < startB
        """
        
        shift = 10
        if combine_options.has_key("shift"):
            shift = combine_options["shift"]       

        if stopA >= startB + shift:
            return True
        return False

    def __perform_merging(self, user_id, project_id, run_id,
                          prnas, generic_family, combine_options):
        """Merge prnas that overlap.

        WARNING: this function works only if the prnas are sorted by their
        start position.
        """
        
        if len(prnas) <= 1:
            return ([], prnas, [])

        # look for prnas that have to be merged together
        to_merge = [{'prnas': [prnas[0]],
                     'start': prnas[0].start_position,
                     'stop': prnas[0].stop_position}]
        k = 0
        for prna in prnas[1:]:
            
            # add the prna to the currant list of overlapping prnas
            if self.__overlap(to_merge[k]['start'], to_merge[k]['stop'],
                              prna.start_position, prna.stop_position,
                              combine_options):
                to_merge[k]['prnas'].append(prna)
                to_merge[k]['stop'] = max(to_merge[k]['stop'],
                                          prna.stop_position)
            # new group of overlapping prnas
            else:
                to_merge.append({'prnas': [prna],
                                 'start': prna.start_position,
                                 'stop': prna.stop_position})
                k += 1

        # merge the prnas inside each group
        merged = []
        # rnas that are not merged
        not_merged = []
        # rnas not being merged because of multiple tRNAscan/RNAmmer predictions
        not_merged_warning = []
        
        for group in to_merge:

            # nothing to merge
            if len(group["prnas"]) == 1:
                not_merged.append(group["prnas"][0])

            # if the generic family is tRNA, then we look for tRNAscan-SE
            # predictions. If there are more than one tRNAscan-SE predictions,
            # we don't merge the RNAs. If there is just one tRNAscan-SE
            # prediction we keep only this prediction.
            elif generic_family == "tRNA":
                family = generic_family
                start = group['start']
                stop = group['stop']
                nb = 0
                struct = []                
                for p in group['prnas']:
                    if p.program_name == 'tRNAscan-SE':
                        nb += 1
                        family = p.family
                        start = p.start_position
                        stop = p.stop_position
                        struct = p.structure

                # more than one tRNAscan-SE predictions => do not merge
                if nb > 1:
                    not_merged_warning.append(group['prnas'])
                    # merged.extend(group['prnas'])
                else:
                    merged.append(self.__merge_prnas(user_id, project_id,
                                                     run_id,
                                                     group['prnas'],
                                                     start, stop, family,
                                                     struct))
            elif( generic_family == "5s_rRNA" or
                  generic_family == "8s_rRNA" or
                  generic_family == "16s_rRNA" or
                  generic_family == "18s_rRNA" or
                  generic_family == "23s_rRNA" or
                  generic_family == "28s_rRNA"):

                family = generic_family
                start = group['start']
                stop = group['stop']
                nb = 0
                struct = []
                for p in group['prnas']:
                    if p.program_name == 'RNAmmer':
                        nb += 1
                        family = p.family
                        start = p.start_position
                        stop = p.stop_position
                        struct = p.structure
                        
                # more than one RNAmmer predictions => do not merge
                if nb > 1:
                    not_merged_warning.append(group['prnas'])
                    # merged.extend(group['prnas'])
                else:
                    merged.append(self.__merge_prnas(user_id, project_id,
                                                     run_id,
                                                     group['prnas'],
                                                     start, stop, family,
                                                     struct))
                
            # default combine
            else:
                merged.append(self.__merge_prnas(user_id, project_id, run_id,
                                                 group['prnas'],
                                                 group['start'],
                                                 group['stop'],
                                                 generic_family))
        return (merged, not_merged, not_merged_warning)
                          
    def __combine(self, prnas, user_id, project_id, run_id, generic_family,
                  user_choice, combine_options):

        merged = []
        not_merged = []
        not_merged_warning = []
        
        # sort prnas by their start position
        prnas.sort(cmp=lambda x,y: cmp(x.start_position, y.start_position))
        
        # if unknown family, then regroup prnas by software type
        if generic_family.lower() == 'unknown':
            comp = []
            user = []
            abinitio = []
            for prna in prnas:
                # user prediction
                if prna.program_name == "user":
                    user.append(prna)
                # comparative analysis
                elif prna.program_name.find("/") != -1:
                    comp.append(prna)
                # abinitio
                else:
                    abinitio.append(prna)
                
            (merged,
             not_merged,
             not_merged_warning) = self.__perform_merging(user_id, project_id,
                                                          run_id, comp,
                                                          generic_family,
                                                          combine_options)
            
            res = self.__perform_merging(user_id, project_id, run_id,
                                         abinitio, generic_family,
                                         combine_options)
            merged.extend(res[0])
            not_merged.extend(res[1])
            not_merged_warning.extend(res[2])
            
            # no merging for user prediction
            if user_choice == 1:
                not_merged.extend(user)
            # separate merging for user prediction
            if user_choice == 2:
                res = self.__perform_merging(user_id, project_id,
                                             run_id, user,
                                             generic_family,
                                             combine_options)
                merged.extend(res[0])
                not_merged.extend(res[1])
                not_merged_warning.extend(res[2])
        else:
            if user_choice == 1 or user_choice == 2:
                user = []
                other = []
                for prna in prnas:
                    # user prediction
                    if prna.program_name == "user":
                        user.append(prna)
                    else:
                        other.append(prna)
                
                (merged,
                 not_merged,
                 not_merged_warning) = self.__perform_merging(user_id,
                                                              project_id,
                                                              run_id, other,
                                                              generic_family,
                                                              combine_options)
                # no merging for user prediction
                if user_choice == 1:
                    not_merged.extend(user)
                # separate merging for user prediction
                else:
                    res = self.__perform_merging(user_id,
                                                 project_id,
                                                 run_id,
                                                 user,
                                                 generic_family,
                                                 combine_options)
                    merged.extend(res[0])
                    not_merged.extend(res[1])
                    not_merged_warning.extend(res[2])                    
            # merge everithing
            else:
                (merged,
                 not_merged,
                 not_merged_warning) = self.__perform_merging(user_id,
                                                              project_id,
                                                              run_id, prnas,
                                                              generic_family,
                                                              combine_options)
        return (merged, not_merged, not_merged_warning)


    def __group_predictions(self, prnas):
        """Groups prnas per input sequence, family and strand.

        Return a 3-levels dictionary:
        {
         genomic_sequence_id: {
                                family: {
                                           strand: [prnas]
                                        }
                              }
        }
        """
        
        rnas_per_family = {}

        # if no generic family found, log the family
        log = self.data_manager.get_not_combined_log()
        if os.path.isfile(log):
            flog = open(log, 'r')
            not_combined = pickle.load(flog)
            flog.close()
        else:
            not_combined = []
        
        for prna in prnas:                        
            gs_id = prna.genomic_sequence_id            
            strand = prna.strand

            # get the generic family name
            # if we can not find a genric name in the table,
            # we keep the original one.            
            family = prna.family
            test = False
            for f in families:
                if prna.family in families[f]:
                    family = f
                    test = True
                    break

            if not test:
                if family not in not_combined:
                    not_combined.append(family)                
                
            rnas_per_family.setdefault(gs_id, {})
            rnas_per_family[gs_id].setdefault(family, {})
            rnas_per_family[gs_id][family].setdefault(strand, [])
            rnas_per_family[gs_id][family][strand].append(prna)
                    
        flog = open(log, 'w')
        pickle.dump(not_combined, flog)
        flog.close()
        
        return rnas_per_family

    def __run_basic_combine(self, user_id, project_id, prnas, user_choice,
                            combine_options, run_id=None):
        """
        combine prnas and return the new ones

        Return([putative_rna])

        user_id(string):       the current user id
        project_id(string):    the current project id
        prnas([putative_rna]): the prnas to merge
        combine_options({})    options for the combine process
        """

        new_prnas = []
        not_merged = []
        not_merged_warning = []

        # make the groups
        rnas_per_family = self.__group_predictions(prnas)
        
        # combine prnas group by group
        for gs_id in rnas_per_family:
            for family in rnas_per_family[gs_id]:
                for strand in rnas_per_family[gs_id][family]:
                    rnas = rnas_per_family[gs_id][family][strand]
                    combined = self.__combine(rnas, user_id, project_id,
                                              run_id, family, user_choice,
                                              combine_options)
                    new_prnas.extend(combined[0])
                    not_merged.extend(combined[1])
                    not_merged_warning.extend(combined[2])

        return (new_prnas, not_merged, not_merged_warning)
