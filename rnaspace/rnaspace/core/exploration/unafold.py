#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import subprocess
import tempfile
import logging

from rnaspace.core.secondary_structure import secondary_structure
from rnaspace.core.data_manager import data_manager
from rnaspace.core.email_sender import email_sender
from rnaspace.core.trace.event import error_event

class unafold(object):
    """ Class unafold: in charge to execute UNAFold and parse its result file
    """

    def run(self, sequence, user_id, project_id):
        dm = data_manager()
        unafold_exe = dm.get_config_value("software","unafold.exe")
        ct2b_exe = dm.get_config_value("software","ct2b.exe")
        tmp_dir = dm.config.get("storage","tmp_dir")
        tmp_file_name = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_output_name_ct = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_output_name_db = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_file = open(tmp_file_name, 'a')
        tmp_file.write(sequence)
        tmp_file.close()
        length = len(sequence)
        unafold_cmd = unafold_exe + " --suffix DAT --mfold=5,-1,"+dm.get_config_value("software","unafold.max_result")+" " + tmp_file_name + ' -o ' + tmp_output_name_ct + " 1> /dev/null" 
        retcode = subprocess.call(unafold_cmd, shell=True)    
        ct2b_cmd = ct2b_exe + " " + tmp_output_name_ct + ".ct > " + tmp_output_name_db
        retcode = subprocess.call(ct2b_cmd, shell=True)    
        if retcode == 0:             
            structure = self.__parse_unafold_result(tmp_output_name_db,length)
        else:
            structure = ''
            message = "Problem running UNAFold: " + unafold_cmd
            email_sender().send_admin_failed_email(user_id, project_id, \
                                                   "explore", "UNAFold", unafold_cmd) 
            mail = self.dm.get_user_email(user_id, project_id)
            ev = error_event(user_id, project_id, mail, 
                             message, self.data_m.get_project_size(user_id, project_id))
            logging.getLogger("rnaspace").error(ev.get_display())

        return structure
    
    def __parse_unafold_result(self, tmp_output_name, length):
        f = open(tmp_output_name, 'r')
        sec_struct = []
        for line in f.readlines():
            part = line.split(" ")
            if len(part) == 2:
                str = part[0]
                free_energy = part[1][1:len(part[1])-2]
                sec_struct.append(secondary_structure("brackets", str, "UNAFold", free_energy))
                
        if sec_struct[0].structure == '':
            seq = ''
            for i in range(length):
                seq+="."
            return [secondary_structure("brackets", seq, "UNAFold", '')]
        return sec_struct
