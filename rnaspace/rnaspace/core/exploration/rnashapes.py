#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import tempfile
import logging

from rnaspace.core.secondary_structure import secondary_structure
from rnaspace.core.data_manager import data_manager
from rnaspace.core.email_sender import email_sender
from rnaspace.core.trace.event import error_event


class rnashapes(object):
    """ Class rnashapes: in charge to execute RNAshapes and parse its result file
    """

    def run(self, sequence, user_id, project_id):
        dm = data_manager()
        rnashapes_exe = dm.get_config_value("software","rnashapes.exe")
        tmp_dir = dm.config.get("storage","tmp_dir")
        tmp_file_name = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_output_name = tempfile.NamedTemporaryFile(dir=tmp_dir).name
        tmp_file = open(tmp_file_name, 'a')
        tmp_file.write(sequence)
        tmp_file.close()
        rnashapes_cmd = rnashapes_exe + " -f" + tmp_file_name + " -o1 -#"+dm.get_config_value("software","rnashapes.max_result")+" > " + tmp_output_name
        retcode = subprocess.call(rnashapes_cmd, shell=True)    
        if retcode == 0:             
            structure = self.__parse_rnashapes_result(tmp_output_name)
        else:
            structure = ''
            message = "Problem running RNAshapes: " + rnashapes_cmd
            email_sender().send_admin_failed_email(user_id, project_id, \
                                                   "explore", "RNAshapes", rnashapes_cmd) 
            mail = dm.get_user_email(user_id, project_id)
            ev = error_event(user_id, project_id, mail, 
                             message, dm.get_project_size(user_id, project_id))
            logging.getLogger("rnaspace").error(ev.get_display())

        return structure
    
    def __parse_rnashapes_result(self, tmp_output_name):
        f = open(tmp_output_name, 'r')
        structure = []
        for line in f.readlines():
            part = line.split(" ")
            if len(part) == 5:
                str = part[0]
                free_energy = part[2][1:-1]
                structure.append(secondary_structure("brackets", str, "RNAshapes", free_energy))
        return structure
