#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import time
import os

from wrapper import wrapper
from rnaspace.core.putative_rna import putative_rna
from rnaspace.core.prediction.software_manager import software_manager

class infernal(wrapper):

    def __init__(self, opts, seq, user_id, project_id, run_id, p, stderr, stdout, 
                 program_name, type, thread_name, version, exe):
        wrapper.__init__(self, opts, seq, user_id, project_id, run_id, p,
                         stderr, stdout, program_name, type, thread_name,
                         version, exe)
        self.infernal_dir = os.path.abspath(os.path.dirname(self.exe_path))

    # Naming in configuration file
    descriptors_conf_name = 'descriptor_set'
    # Text for choosing all descriptors (first elt of the list)
    All_descriptors_text = 'All_(LONG_RUNTIME!)'

    def run(self):
        try:
            self.__run()
        except Exception :
            import sys
            import traceback
            from rnaspace.core.trace.event import unknown_error_event
            (type, value, trace) = sys.exc_info()
            tb = "\n".join(traceback.format_exception(type, value, trace))
            error = "\n".join(traceback.format_exception_only(type, value))
            self.email_s.send_admin_tb(tb)
            ev = unknown_error_event(self.user_id, self.project_id, error,
                                     self.run_id, self.program_name)
            self.dm.update_project_trace(self.user_id, self.project_id, [ev])
            return


    def __run(self):
        '''
            Run INFERNAL gene finder for selected RNA family descriptor(s).
        '''
        defined_descriptors = []
        choosen_descriptor = []
        
        if self.opts is not None:
            for opt in self.opts:
                if opt == self.descriptors_conf_name:
                    choosen_descriptor = self.opts[opt].split()[0]
                    if  choosen_descriptor == self.All_descriptors_text:
                        sm=software_manager()
                        defined_descriptors = sm.get_options_params(self.program_name,
                                                                    self.type)[self.descriptors_conf_name]
                        defined_descriptors = defined_descriptors[1:len(defined_descriptors)] # remove 'All'
                        chosen_descriptors = []
                        # look for all descriptors 
                        for descriptor in defined_descriptors:
                            d = descriptor.split()[1]
                            chosen_descriptors.append(d[1:len(d)-1])
                    else:                    
                        # take the Rfam name, of the selected family, without the []
                        d = self.opts[opt].split()[1]
                        chosen_descriptors = [d[1:len(d)-1]]

        sequence_file = self.get_sequence_file_path()
        for descriptor in chosen_descriptors:
            t1 = time.time()
            options = self.exe_path[0:len(self.exe_path)-len(os.path.basename(self.exe_path))-4] \
                      + 'Rfam/' + descriptor  + '.cm'
                      
            result = self.get_temporary_file()
            cmd = self.exe_path + ' --ga --tabfile ' + result + ' ' + options  + ' ' + sequence_file

            self.launch(cmd)
            if choosen_descriptor == self.All_descriptors_text:
                for d in defined_descriptors:
                    if '['+descriptor+']' == d.split()[1]:
                        family =  d.split()[0]
            else:
                family = choosen_descriptor
            [nb_prediction, nb_alignment] = self.memorize_results(result,family)
            os.remove(result)
            t2 = time.time()
            self.trace_predict_event(cmd,nb_prediction,nb_alignment,t2 - t1,"infernal")

        
    def memorize_results(self, result_file, family):
        """
        Convert the RNA predictions text file in RNA objects and store them.
        """
        prnas_list = []
        nb_prediction = 0
        line1,line2 = None,None
        for line in open(result_file):
            if not line.startswith('#'):
                fields = line.split()
                start_pos = fields[2]
                stop_pos = fields[3]
                id = self.id_gen.get_new_putativerna_id(self.user_id,
                                                        self.project_id,
                                                        self.seq.id)
                if start_pos < stop_pos:
                    strand = '+'
                    rna_seq = self.seq.data[int(fields[2])-1:int(fields[3])]
                else:
                    strand = '-'
                    rna_seq = self.seq.data[int(fields[3])-1:int(fields[2])]
                
                
                prnas_list.append(putative_rna(id, self.seq.id, 
                                               long(start_pos),
                                               long(stop_pos), self.run_id, 
                                               user_id=id, family=family,
                                               sequence=rna_seq, 
                                               strand=strand,
                                               domain=self.seq.domain, 
                                               species=self.seq.species,
                                               strain=self.seq.strain, 
                                               score=float(fields[7]),
                                               replicon=self.seq.replicon,
                                               program_name=self.program_name,
                                               program_version=self.program_version,
                                               day=self.day, month=self.month, 
                                               year=self.year)) 
                 
                 
                nb_prediction = nb_prediction + 1    
        self.add_putative_rnas(prnas_list)
        return [nb_prediction,0]

