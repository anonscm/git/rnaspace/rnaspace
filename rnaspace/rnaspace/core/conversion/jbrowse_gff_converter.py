#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from rnaspace.core.putative_rna import putative_rna

class jbrowse_gff_converter (object):
    """ Class jbrowse_gff_converter: converts objects to jbrowse gff or read gff to objects
        This object has to be used as following:
            my_var = jbrowse_gff_converter()
            my_var.read(path/to.gff.gff)
            obj = my_var.get_object()
    """   

    def write(self, obj, output):
        """ writes a table of object into an gff file
            prnas(type:[object])    the object table to write down
            output(type:string)     path to the folder to store the gff file
        """
        if len(obj) > 0:
            if type(obj[0]) == putative_rna or isinstance(obj[0], putative_rna):
                self.__write_putative_rnas(obj, output)
            else: raise TypeError( str(type(obj)) + " is not supported by our rnaml converter.")

    def __write_putative_rnas(self, prnas, output):
        """ writes a table of putative_rna into a gff file
            prna(type:[putative_rna])  the putative_rna table to write down
            output(type:string)      path to the folder to store the gff file
        """
        max_length_family_name = 8
        gff_file = open(output, 'w')
        for prna in prnas:
            gff_file.write(str(prna.genomic_sequence_id) + "\t" + str(prna.program_name)    + "\tncRNA\t")
            gff_file.write(str(prna.start_position) + "\t" + str(prna.stop_position)   + "\t" + str(prna.score))
            if len(prna.family) > max_length_family_name :
                name = prna.user_id+"/"+prna.family[0:max_length_family_name]
            else :
                name = prna.user_id+"/"+prna.family
            gff_file.write("\t" + str(prna.strand) + "\t.\tName=" + str(name) + ";Alias=" + str(prna.sys_id) + "\n")
        gff_file.close()

        
