#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
from storage_configuration_reader import storage_configuration_reader

class mask_region_handler:
    """ 
    Class mask_region_handler: the mask region handler is the class that 
    will stock and produce the GFF output.
    """

    def __init__(self):
        """ Init """
        self.config = storage_configuration_reader()
        self.suffix = ".mask.gff"

    def add_regions(self, user_id, project_id, seq_id, regions, source):
        """ 
        user_id(string):           the id of the connected user
        project_id(string):        the id of the project
        seq_id(string):            sequence id
        path(string):              where write the file
        regions(list(list)):       masked regions   
        source(string):            where do the regions came from
        """
        path = self.config.get_sequence_directory(user_id, project_id)
        path = os.path.join(path, seq_id+self.suffix)
        
        if os.path.exists(path):
            mask_regions = open(path, "a")
        else:
            mask_regions = open(path, "w")
        for i,r in enumerate(regions):
            mask_regions.write(seq_id+"\tRNAspace\t"+source+"\t"+str(r[0])+"\t"+str(r[1])+"\t.\t.\t.\tName="+str(i+1)+"\n")
        mask_regions.close()
        
