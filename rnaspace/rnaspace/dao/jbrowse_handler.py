#
# RNAspace: non-coding RNA annotation platform
# Copyright (C) 2009  CNRS, INRA, INRIA, Univ. Paris-Sud 11
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import shutil
import subprocess
import json
import operator
import shutil

import rnaspace
from data_handler import data_handler
from user_handler import user_handler
from rnaspace.dao.sequence_handler import sequence_handler
from rnaspace.core.putative_rna import putative_rna
import rnaspace.core.common as common
from rnaspace.core.exceptions import disk_error, project_space_error, user_space_error
from rnaspace.core.conversion.jbrowse_gff_converter import jbrowse_gff_converter

class jbrowse_handler (data_handler):
    """
    Class jbrowse_handler: this data handler control all the data dealing with jbrowse
    """

    def __init__(self):
        data_handler.__init__(self)
        self.user_handler = user_handler()
        # Create the symbolic link between jbrowse and user datas : created from the rnaspace_on_web script 
        if not os.path.islink(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../ui/web/ressource/jbrowse/data")):
            os.symlink(self.config.get_storage_directory(), os.path.join(os.path.dirname(os.path.abspath(__file__)), "../ui/web/ressource/jbrowse/data"))
    
    def add_sequence (self, user_id, project_id, seq):
        pass
            
    def add_putative_rnas (self, user_id, project_id, prnas):
        self.prepare_tracks(user_id, project_id, prnas)
    
    
    def __create_jb_conf(self, user_id, project_id):
        config_txt = """\
{
  "description": "RNAspace data",
  "db_adaptor": "Bio::DB::SeqFeature::Store",
  "db_args": { "-adaptor": "memory",
               "-dir": "%s" },

  "TRACK DEFAULTS": {
    "class": "feature",
    "autocomplete": "all"
  },

  "tracks": [
    {
      "track": "ncRNA",
      "key": "ncRNA",
      "feature": ["ncRNA"],
      "class": "feature5",
      "urlTemplate": "%s",
      "extraData": {"alias": "sub {shift->attributes(\\"Alias\\");}"}
    },
    {
      "track": "maskCoding",
      "key": "maskCoding",
      "feature": ["mask_annot"],
      "class": "feature4"
    },
    {
      "track": "maskTranscript",
      "key": "maskTranscript",
      "feature": ["mask_nontranscrit"],
      "class": "feature4"
    },
    {
      "track": "maskMdust",
      "key": "maskMdust",
      "feature": ["mask_mdust"],
      "class": "feature4"
    },
    {
      "track": "maskRepeatM",
      "key": "maskRepeatM",
      "feature": ["mask_repeatmasker"],
      "class": "feature4"
    },
    {
      "track": "maskAll",
      "key": "maskAll",
      "feature": ["mask"],
      "class": "feature4"
    },
    {
      "track": "user",
      "key": "user",
      "feature": ["user"],
      "class": "feature"
    }
  ]
}
""" % ((self.config.get_sequence_directory(user_id, project_id)),
       (self.config.get("global", "mount_point") + "explore/rnavisualisation/index?authkey="+user_id+"-"+project_id+"&mode=display&rna_id={alias}"))

        jb_conf = open(os.path.join(self.config.get_jbrowse_directory(user_id, project_id), "jbrowse.json"), "w")
        jb_conf.write(config_txt)
        jb_conf.close()
        
    
    def prepare_tracks (self, user_id, project_id, prnas, seq_ids):       
        seq_dir = self.config.get_sequence_directory(user_id, project_id)
        out_jb_dir = self.config.get_jbrowse_directory(user_id, project_id)      
        try:
            #os.remove    (out_jb_dir + "/refSeqs.js")
            #shutil.rmtree(out_jb_dir + "/seq")
            os.remove    (out_jb_dir + "/trackInfo.js")
            shutil.rmtree(out_jb_dir + "/tracks")
        except:
            pass
        
        size = 0
        max_user_size = common.get_nb_octet(self.config.get("storage", "user_size_limitation"))
        max_project_size = common.get_nb_octet(self.config.get("storage", "project_size_limitation"))
        user_space = self.user_handler.get_user_used_space(user_id)
        project_space = self.user_handler.get_project_used_space(user_id, project_id)
        
        self.__create_jb_conf(user_id, project_id)    
        
        for seq_id in seq_ids:       
            prepare_cmd = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../ui/web/ressource/jbrowse/bin/prepare-refseqs.pl") + " --fasta " + seq_dir + "/" + seq_id + ".fna --out " + out_jb_dir + " --webpath " + os.path.join(self.config.get("global", "mount_point"), "ressource") + "/jbrowse/data/" + user_id + "/" + project_id + "/jbrowse"
            retcode = subprocess.call(prepare_cmd, shell=True)
                    
        export = jbrowse_gff_converter()
        export.write(prnas, seq_dir + "/prnasForJbrowse.gff")
                
        tracks_cmd = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../ui/web/ressource/jbrowse/bin/biodb-to-json.pl") + " --conf " + out_jb_dir + "/jbrowse.json --out " + out_jb_dir + " --webpath " + os.path.join(self.config.get("global", "mount_point"), "ressource") + "/jbrowse/data/" + user_id + "/" + project_id + "/jbrowse 1> /dev/null"
        retcode = subprocess.call(tracks_cmd, shell=True)
        
        size += self.get_disk_size(out_jb_dir)
        
        if size + user_space > max_user_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise user_space_error(user_id, project_id,
                                       disk_error.jbrowse_message)
                
        if size + project_space > max_project_size:
                self.user_handler.update_project_used_space(user_id,
                                                            project_id, size)
                raise project_space_error(user_id, project_id,
                                          disk_error.jbrowse_message)